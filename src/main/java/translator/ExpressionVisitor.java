package translator;

import ast.*;
import ast.ExpBinOp;
import ir.*;
import ir.Exp;
import ir.Stm;
import translator.VTableLayout.Entry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by bernhard on 13.11.17.
 */
public class ExpressionVisitor implements ast.ExpVisitor<ir.Exp, Error> {

    private Env env;

    public ExpressionVisitor(Env env) {
        this.env = env;
    }
    
    @Override
    public ir.Exp visit(ExpTrue e) throws Error {
        return new ir.ExpConst(1);
    }

    @Override
    public ir.Exp visit(ExpFalse e) throws Error {
        return new ir.ExpConst(0);
    }

    @Override
    public ir.Exp visit(ExpThis e) throws Error {
        return new ir.ExpParam(0, ir.Exp.Type.OBJECT);
    }

    @Override
    public ir.Exp visit(ExpNewIntArray e) throws Error {
        final ir.Exp size = e.getSize().accept(this);
        return new ir.ExpCall(env.getLabel("int$__newarray__"), Collections.singletonList(size), ir.Exp.Type.OBJECT);
    }

    @Override
    public ir.Exp visit(ExpNew e) throws Error {
        return new ir.ExpCall(env.getLabel(e.getClassName() + "$__new__"), Collections.emptyList(), ir.Exp.Type.OBJECT);
    }

    @Override
    public ir.Exp visit(ExpNeg e) throws Error {
        final ir.Exp exp = e.getExp().accept(this);
        return new ir.ExpBinOp(ir.ExpBinOp.Op.MINUS, new ir.ExpConst(1), exp);
    }

    @Override
    public ir.Exp visit(ExpBinOp e) throws Error {
        ir.ExpBinOp.Op op = null;
        switch (e.getOp()) {
            case PLUS:
                op = ir.ExpBinOp.Op.PLUS;
                break;
            case MINUS:
                op = ir.ExpBinOp.Op.MINUS;
                break;
            case TIMES:
                op = ir.ExpBinOp.Op.MUL;
                break;
            case DIV:
                op = ir.ExpBinOp.Op.DIV;
                break;
        }

        switch (e.getOp()) {
            case PLUS:
            case MINUS:
            case TIMES: {
                    final ir.Exp left = e.getLeft().accept(this);
                    final ir.Exp right = e.getRight().accept(this);
                    return new ir.ExpBinOp(op, left, right);
                }
            case DIV: {
                    final ir.Exp left = e.getLeft().accept(this);
                    final ir.Exp right = e.getRight().accept(this);

                    final ir.Label raiseLabel = new ir.Label("_raise_dispatch");
                    final ir.Label passedLabel = new ir.Label();

                    final boolean isTrivialRight = right instanceof ExpTemp || right instanceof ExpMem;
                    final boolean isTrivialNonzeroConst = right instanceof ExpConst && ((ExpConst) right).getValue() != 0;
                    final ir.Exp rightValue = isTrivialRight ? right : new ir.ExpTemp(new ir.Temp(), ir.Exp.Type.INTEGER);

                    final ir.Stm check = new ir.StmSeq(
                            isTrivialRight ? Stm.getNOP() : new ir.StmMove(rightValue, right),
                            isTrivialNonzeroConst ? Stm.getNOP() : new ir.StmCJump(StmCJump.Rel.EQ, rightValue, new ir.ExpConst(0), raiseLabel, passedLabel),
                            isTrivialNonzeroConst ? Stm.getNOP() : new ir.StmLabel(passedLabel)
                    );
                    return new ir.ExpESeq(check, new ir.ExpBinOp(op, left, rightValue));
                }
            case AND:
            case LT: {
                    final Exp result = new ir.ExpTemp(new ir.Temp(), ir.Exp.Type.INTEGER);
                    final Label returnFalse = new ir.Label();
                    final Label returnTrue = new ir.Label();
                    final Label endLabel = new ir.Label();
                    final ConditionVisitor conditionVisitor = new ConditionVisitor(env, returnTrue, returnFalse);
                    final ir.Stm condition = e.accept(conditionVisitor);
                    return new ir.ExpESeq(
                        new ir.StmSeq(
                                condition,
                                new ir.StmLabel(returnTrue),
                                new ir.StmMove(result, new ir.ExpConst(1)),
                                new ir.StmJump(endLabel),
                                new ir.StmLabel(returnFalse),
                                new ir.StmMove(result, new ir.ExpConst(0)),
                                new ir.StmLabel(endLabel)
                        ),
                        result);
                }
            default:
                throw new Error();
        }
    }

    @Override
    public ir.Exp visit(ExpArrayGet e) throws Error {
        final Exp array = e.getArray().accept(this);
        final Exp index = e.getIndex().accept(this);

        final boolean isTrivialIndex = index instanceof ExpConst || index instanceof ExpTemp || index instanceof ExpMem;
        final boolean isTrivialArray = array instanceof ExpTemp;
        final boolean isValidConstIndex = index instanceof ExpConst && ((ExpConst) index).getValue() >= 0;

        final Exp indexValue = isTrivialIndex ? index : new ExpTemp(new Temp(), ir.Exp.Type.INTEGER);
        final Exp arrayValue = isTrivialArray ? array : new ExpTemp(new Temp(), ir.Exp.Type.OBJECT);
        final Exp arrayLength = new ir.ExpMem(arrayValue, ir.Exp.Type.INTEGER);

        final Label raiseLabel = new Label("_raise_dispatch");
        final Label boundObeyedLabel1 = new Label();
        final Label boundObeyedLabel2 = new Label();

        final Stm calcIndexValue = new ir.StmMove(indexValue, index);
        final Stm calcArrayValue = new ir.StmMove(arrayValue, array);

        final Stm boundCheck = new ir.StmSeq(
                    isTrivialArray ? Stm.getNOP() : calcArrayValue,
                    isTrivialIndex ? Stm.getNOP() : calcIndexValue,
                    new ir.StmCJump(StmCJump.Rel.LE, arrayLength, indexValue, raiseLabel, boundObeyedLabel1),
                    new ir.StmLabel(boundObeyedLabel1),
                    isValidConstIndex ? Stm.getNOP() : new ir.StmCJump(StmCJump.Rel.LT, indexValue, new ir.ExpConst(0), raiseLabel, boundObeyedLabel2),
                    isValidConstIndex ? Stm.getNOP() : new ir.StmLabel(boundObeyedLabel2)
            );

        final Exp element = new ir.ExpBinOp(ir.ExpBinOp.Op.PLUS,
                arrayValue,
                new ir.ExpBinOp(ir.ExpBinOp.Op.PLUS,
                        new ir.ExpConst(4),
                        new ir.ExpBinOp(ir.ExpBinOp.Op.MUL,
                                new ir.ExpConst(4),
                                indexValue)));

        return new ir.ExpESeq(boundCheck, new ir.ExpMem(element, ir.Exp.Type.INTEGER));
    }

    @Override
    public ir.Exp visit(ExpArrayLength e) throws Error {
        final Exp array = e.getArray().accept(this);
        return new ir.ExpMem(array, ir.Exp.Type.INTEGER);
    }

    @Override
    public ir.Exp visit(ExpInvoke e) throws Error {
        final Exp thisExp = e.getObj().accept(this);
        final List<Exp> args = new ArrayList<>();

        final Temp thisTemp = new Temp();

        args.add(new ir.ExpTemp(thisTemp, ir.Exp.Type.OBJECT));
        for (ast.Exp arg : e.getArgs()) {
            args.add(arg.accept(this));
        }

        final VTableLayout vTable = env.getVTableLayout(e.getObjClass());
        final Entry method = vTable.getMethod(e.getMethod());
        if (vTable.isBaseClass()) {
            ExpMem f = new ir.ExpMem(
                    new ir.ExpBinOp(
                            ir.ExpBinOp.Op.PLUS,
                            new ir.ExpMem(new ir.ExpTemp(thisTemp, ir.Exp.Type.OBJECT), ir.Exp.Type.INTEGER),
                            new ir.ExpConst(method.getOffset())), ir.Exp.Type.INTEGER);

            return new ir.ExpESeq(
                    new ir.StmMove(new ir.ExpTemp(thisTemp, ir.Exp.Type.OBJECT), thisExp),
                    new ir.ExpCall(f, args, method.getReturnType()));
        } else {
            return new ir.ExpESeq(
                    new ir.StmMove(new ir.ExpTemp(thisTemp, ir.Exp.Type.OBJECT), thisExp),
                    new ir.ExpCall(method.getLabel(), args, method.getReturnType()));
        }
    }

    @Override
    public ir.Exp visit(ExpRead e) throws Error {
        return new ir.ExpCall(new ir.ExpName(new ir.Label("_read")), Collections.emptyList(), ir.Exp.Type.INTEGER);
    }

    @Override
    public ir.Exp visit(ExpIntConst e) throws Error {
        return new ir.ExpConst(e.getValue());
    }

    @Override
    public ir.Exp visit(ExpId e) throws Error {
        return env.getVariable(e.getId());
    }
}
