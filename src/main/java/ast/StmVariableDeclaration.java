package ast;
import java_cup.runtime.ComplexSymbolFactory;

import java.util.Optional;

public class StmVariableDeclaration extends Stm {

  private final Type type;
  private final String name;
  private final Optional<Exp> initializer;

  private final ComplexSymbolFactory.Location leftNameLocation;
  private final ComplexSymbolFactory.Location rightNameLocation;

  public StmVariableDeclaration(Type type, String name, ComplexSymbolFactory.Location leftNameLocation, ComplexSymbolFactory.Location rightNameLocation) {
    this(type, name, Optional.empty(), leftNameLocation, rightNameLocation);
  }

  public StmVariableDeclaration(Type type, String name, Exp initializer, ComplexSymbolFactory.Location leftNameLocation, ComplexSymbolFactory.Location rightNameLocation) {
    this(type, name, Optional.ofNullable(initializer), leftNameLocation, rightNameLocation);
  }

  private StmVariableDeclaration(Type type, String name, Optional<Exp> initializer, ComplexSymbolFactory.Location leftNameLocation, ComplexSymbolFactory.Location rightNameLocation) {
    this.type = type;
    this.name = name;
    this.initializer = initializer;
    this.leftNameLocation = leftNameLocation;
    this.rightNameLocation = rightNameLocation;
  }

  public Type getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public Optional<Exp> getInitializer() {
    return initializer;
  }

  @Override
  public <A, T extends Throwable> A accept(StmVisitor<A, T> v) throws T {
    return v.visit(this);
  }

  public ComplexSymbolFactory.Location getLeftNameLocation() {
    return leftNameLocation;
  }

  public ComplexSymbolFactory.Location getRightNameLocation() {
    return rightNameLocation;
  }
}
