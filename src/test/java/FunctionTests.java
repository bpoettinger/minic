import org.junit.Test;

public class FunctionTests {
    @Test
    public void testLargeGameOfLife() {
        Helpers.compileAndExecute("Large/GameOfLife");
    }
    @Test
    public void testLargeMandelbrot() {
        Helpers.compileAndExecute("Large/Mandelbrot");
    }
    @Test
    public void testLargeRaytrace() {
        Helpers.compileAndExecute("Large/Raytrace");
    }
    @Test
    public void testLargeRaytraceAndGauss() {
        Helpers.compileAndExecute("Large/RaytraceAndGauss");
    }
    @Test
    public void testLargeGameOfLifeAnim() {
        Helpers.compileAndExecute("Large/GameOfLifeAnim");
    }
    @Test
    public void testLargeMandelbrotAnim() {
        Helpers.compileAndExecute("Large/MandelbrotAnim");
    }
    @Test
    public void testLargePi() {
        Helpers.compileAndExecute("Large/Pi");
    }
    @Test
    public void testMediumBinarySearch() {
        Helpers.compileAndExecute("Medium/BinarySearch");
    }
    @Test
    public void testMediumBinaryTree() {
        Helpers.compileAndExecute("Medium/BinaryTree");
    }
    @Test
    public void testMediumBubbleSort() {
        Helpers.compileAndExecute("Medium/BubbleSort");
    }
    @Test
    public void testMediumEuler() {
        Helpers.compileAndExecute("Medium/Euler");
    }
    @Test
    public void testMediumFib() {
        Helpers.compileAndExecute("Medium/Fib");
    }
    @Test
    public void testMediumFibL() {
        Helpers.compileAndExecute("Medium/FibL");
    }
    @Test
    public void testMediumHanoi() {
        Helpers.compileAndExecute("Medium/Hanoi");
    }
    @Test
    public void testMediumLinearSearch() {
        Helpers.compileAndExecute("Medium/LinearSearch");
    }
    @Test
    public void testMediumLinkedList() {
        Helpers.compileAndExecute("Medium/LinkedList");
    }
    @Test
    public void testMediumManyArgs() {
        Helpers.compileAndExecute("Medium/ManyArgs");
    }
    @Test
    public void testMediumNewton() {
        Helpers.compileAndExecute("Medium/Newton");
    }
    @Test
    public void testMediumPiSin() {
        Helpers.compileAndExecute("Medium/PiSin");
    }
    @Test
    public void testMediumPrimes() {
        Helpers.compileAndExecute("Medium/Primes");
    }
    @Test
    public void testMediumQuickSort() {
        Helpers.compileAndExecute("Medium/QuickSort");
    }
    @Test
    public void testMediumFannkuch() {
        Helpers.compileAndExecute("Medium/Fannkuch");
    }
    @Test
    public void testMediumGraph() {
        Helpers.compileAndExecute("Medium/Graph");
    }
    @Test
    public void testSmallArrayAccess() {
        Helpers.compileAndExecute("Small/ArrayAccess");
    }
    @Test
    public void testSmallEffects() {
        Helpers.compileAndExecute("Small/Effects");
    }
    @Test
    public void testSmallFactorial() {
        Helpers.compileAndExecute("Small/Factorial");
    }
    @Test
    public void testSmallPrecedence() {
        Helpers.compileAndExecute("Small/Precedence");
    }
    @Test
    public void testSmallScope2() {
        Helpers.compileAndExecute("Small/Scope2");
    }
    @Test
    public void testSmallShortCutAnd() {
        Helpers.compileAndExecute("Small/ShortCutAnd");
    }
    @Test
    public void testSmallTestEq() {
        Helpers.compileAndExecute("Small/TestEq");
    }
    @Test
    public void testSmallTrivialClass() {
        Helpers.compileAndExecute("Small/TrivialClass");
    }
    @Test
    public void testSmallSum() {
        Helpers.compileAndExecute("Small/Sum");
    }
    @Test
    public void testSmallStck() {
        Helpers.compileAndExecute("Small/Stck");
    }
    @Test
    public void testSmallArrSum() {
        Helpers.compileAndExecute("Small/ArrSum");
    }
    @Test
    public void testSmallShadowedAttribute() {
        Helpers.compileAndExecute("Small/ShadowedAttribute");
    }
    @Test
    public void testSmallWhile() {
        Helpers.compileAndExecute("Small/While");
    }
    @Test
    public void testSmallConstArithmetic() {
        Helpers.compileAndExecute("Small/ConstArithmetic");
    }
    @Test
    public void testGrammarExtensionDerivedMembers() {
        Helpers.compileAndExecute("GrammarExtension/DerivedMembers");
    }
    @Test
    public void testGrammarExtensionUnorderedMembers() {
        Helpers.compileAndExecute("GrammarExtension/UnorderedMembers");
    }
    @Test
    public void testInheritanceEulerOO() {
        Helpers.compileAndExecute("Inheritance/EulerOO");
    }
    @Test
    public void testInheritanceInheritance() {
        Helpers.compileAndExecute("Inheritance/Inheritance");
    }
    @Test
    public void testInheritanceNewtonOO() {
        Helpers.compileAndExecute("Inheritance/NewtonOO");
    }
    @Test
    public void testInheritanceQuickSortOO() {
        Helpers.compileAndExecute("Inheritance/QuickSortOO");
    }
    @Test
    public void testInheritanceTreeVisitor() {
        Helpers.compileAndExecute("Inheritance/TreeVisitor");
    }
    @Test
    public void testInheritancePrimesOO() {
        Helpers.compileAndExecute("Inheritance/PrimesOO");
    }
    @Test
    public void testShouldFailScope() {
        Helpers.compileFileFails("ShouldFail/Scope");
    }
    @Test
    public void testShouldFailClashedIfScope() {
        Helpers.compileFileFails("ShouldFail/ClashedIfScope");
    }
    @Test
    public void testShouldFailClashedWhileScope() {
        Helpers.compileFileFails("ShouldFail/ClashedWhileScope");
    }
    @Test
    public void testShouldFailParseErrorsBigMain() {
        Helpers.compileFileFails("ShouldFail/ParseErrors/BigMain");
    }
    @Test
    public void testShouldFailParseErrorsBrokenLex1() {
        Helpers.compileFileFails("ShouldFail/ParseErrors/BrokenLex1");
    }
    @Test
    public void testShouldFailParseErrorsBrokenLex2() {
        Helpers.compileFileFails("ShouldFail/ParseErrors/BrokenLex2");
    }
    @Test
    public void testShouldFailParseErrorsBrokenParse1() {
        Helpers.compileFileFails("ShouldFail/ParseErrors/BrokenParse1");
    }
    @Test
    public void testShouldFailParseErrorsBrokenParse2() {
        Helpers.compileFileFails("ShouldFail/ParseErrors/BrokenParse2");
    }
    @Test
    public void testShouldFailParseErrorsMissingThis() {
        Helpers.compileFileFails("ShouldFail/ParseErrors/MissingThis");
    }
    @Test
    public void testShouldFailParseErrorsStringLiteral() {
        Helpers.compileFileFails("ShouldFail/ParseErrors/StringLiteral");
    }
    @Test
    public void testShouldFailRuntimeErrorsArrSumBUG() {
        Helpers.compileAndExecuteFail("ShouldFail/RuntimeErrors/ArrSumBUG");
    }
    @Test
    public void testShouldFailRuntimeErrorsArrayBounds() {
        Helpers.compileAndExecuteFail("ShouldFail/RuntimeErrors/ArrayBounds");
    }
    @Test
    public void testShouldFailRuntimeErrorsArrayBoundsNeg() {
        Helpers.compileAndExecuteFail("ShouldFail/RuntimeErrors/ArrayBoundsNeg");
    }
    @Test
    public void testShouldFailRuntimeErrorsArrSumBUGNeg() {
        Helpers.compileAndExecuteFail("ShouldFail/RuntimeErrors/ArrSumBUGNeg");
    }
    @Test
    public void testShouldFailRuntimeErrorsDivisionByZero() {
        Helpers.compileAndExecuteFail("ShouldFail/RuntimeErrors/DivisionByZero");
    }
    @Test
    public void testShouldFailTypeErrorsInvalidIfCondition() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/InvalidIfCondition");
    }
    @Test
    public void testShouldFailTypeErrorsLinkedListBUG() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/LinkedListBUG");
    }
    @Test
    public void testShouldFailTypeErrorsMethodCallWrong() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/MethodCallWrong");
    }
    @Test
    public void testShouldFailTypeErrorsMethodWronglySpelled() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/MethodWronglySpelled");
    }
    @Test
    public void testShouldFailTypeErrorsPrintFirstArg() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/PrintFirstArg");
    }
    @Test
    public void testShouldFailTypeErrorsShadow() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/Shadow");
    }
    @Test
    public void testShouldFailTypeErrorsTooManyArguments() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/TooManyArguments");
    }
    @Test
    public void testShouldFailTypeErrorsTypeOfArgWrong() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/TypeOfArgWrong");
    }
    @Test
    public void testShouldFailTypeErrorsUndeclaredType() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/UndeclaredType");
    }
    @Test
    public void testShouldFailTypeErrorsUndeclaredVar1() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/UndeclaredVar1");
    }
    @Test
    public void testShouldFailTypeErrorsUndeclaredVar2() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/UndeclaredVar2");
    }
    @Test
    public void testShouldFailTypeErrorsWrongArgumentTypes() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/WrongArgumentTypes");
    }
    @Test
    public void testShouldFailTypeErrorsWrongArgumentTypes2() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/WrongArgumentTypes2");
    }
    @Test
    public void testShouldFailTypeErrorsWrongTypeDeclared() {
        Helpers.compileFileFails("ShouldFail/TypeErrors/WrongTypeDeclared");
    }

}