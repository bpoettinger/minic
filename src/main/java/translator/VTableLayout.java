package translator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import ir.Exp;
import ir.ExpName;

public class VTableLayout {

	private final String className;
	private final Map<String, Entry> methods = new HashMap<>();
	private final boolean isBaseClass;

	public VTableLayout(String className, boolean isBaseClass) {
		this.className = className;
		this.isBaseClass = isBaseClass;
	}

	public void addMethod(String name, int offset, ExpName methodLabel, Exp.Type returnType) {
		methods.put(name, new Entry(offset, methodLabel, returnType));
	}

	public boolean containsMethod(String name) {
		return methods.containsKey(name);
	}

	public Entry getMethod(String name) {
		if (methods.containsKey(name)) {
			return methods.get(name);
		}
		throw new IllegalStateException(name + " not in vtable");
	}

	public Collection<Entry> getAllMethods() {
		return methods.values();
	}

	public String getClassName() {
		return className;
	}

	public boolean isBaseClass() {
		return isBaseClass;
	}

	public static class Entry {
		private final int offset;
		private final ir.ExpName label;
		private Exp.Type returnType;

		public Entry(int offset, ExpName label, Exp.Type returnType) {
			this.offset = offset;
			this.label = label;
			this.returnType = returnType;
		}

		public int getOffset() {
			return offset;
		}

		public ir.ExpName getLabel() {
			return label;
		}

		public Exp.Type getReturnType() {
			return returnType;
		}
	}
}
