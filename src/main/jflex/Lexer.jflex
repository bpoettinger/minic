/* Copyright (c) 1997 Andrew W. Appel.  Licensed software: see LICENSE file */

import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;

%%

%public
%class Lexer
%implements java_cup.runtime.Scanner
%function next_token
%type java_cup.runtime.Symbol
%yylexthrow ParseException

%char
%line
%column

%{

private ComplexSymbolFactory symbolFactory;

public Lexer(java.io.Reader in, ComplexSymbolFactory symbolFactory) {
    this(in);
    this.symbolFactory = symbolFactory;
}

private void error(String s) throws ParseException {
  throw new ParseException("At line " + yyline + ", column " + yycolumn + ": " + s);
}

private Symbol symbol(String name, int kind) {
    return symbolFactory.newSymbol(name,
                                   kind,
                                   new ComplexSymbolFactory.Location(yyline+1, yycolumn+1),
                                   new ComplexSymbolFactory.Location(yyline+1, yycolumn+yylength()));
}

private Symbol symbol(String name, int kind, Object value) {
    return symbolFactory.newSymbol(name,
                                   kind,
                                   new ComplexSymbolFactory.Location(yyline+1, yycolumn+1),
                                   new ComplexSymbolFactory.Location(yyline+1, yycolumn+yylength()),
                                   value);
}

private int commentDepth = 0;

%}

%eofval{
  {
    if (commentDepth > 0) {
       error("runaway comment");
    }
    return symbol("{eof}", Parser_sym.EOF);
  }
%eofval}

%state SingleLineComment MultiLineComment
idchars=[A-Za-z_0-9]
id=[A-Za-z]{idchars}*
ws=[\r\n\t\ ]+
%%
<YYINITIAL> {
   {ws}     { }
   "class"  { return symbol("class", Parser_sym.CLASS); }
   "{"      { return symbol("{", Parser_sym.LBRACE); }
   "}"      { return symbol("}", Parser_sym.RBRACE); }
   "["      { return symbol("[", Parser_sym.LSBRACK); }
   "]"      { return symbol("]", Parser_sym.RSBRACK); }
   "("      { return symbol("(", Parser_sym.LPAREN); }
   ")"      { return symbol(")", Parser_sym.RPAREN); }
   "<"      { return symbol("<", Parser_sym.LPBRACK); }
   "public" { return symbol("public", Parser_sym.PUBLIC); }
   "static" { return symbol("static", Parser_sym.STATIC); }
   "void"   { return symbol("void", Parser_sym.VOID); }
   "String" { return symbol("String", Parser_sym.STRING); }
   "extends" { return symbol("extends", Parser_sym.EXTENDS); }
   "throws" { return symbol("throws", Parser_sym.THROWS); }
   "java.io.IOException" { return symbol("java.io.Exception", Parser_sym.EXCEPTION); }
   "return" { return symbol("return", Parser_sym.RETURN); }
   "int"    { return symbol("int", Parser_sym.INT); }
   "boolean" { return symbol("boolean", Parser_sym.BOOLEAN); }
   "if"     { return symbol("if", Parser_sym.IF); }
   "else"   { return symbol("else", Parser_sym.ELSE); }
   "while"  { return symbol("while", Parser_sym.WHILE); }
   "System.out.println" { return symbol("System.out.println", Parser_sym.PRINTLN); }
   "System.out.write" { return symbol("System.out.write", Parser_sym.WRITE); }
   "System.out.read" { return symbol("System.out.read", Parser_sym.READ); }
   "length"	{ return symbol("length", Parser_sym.LENGTH); }
   "true"   { return symbol("true", Parser_sym.TRUE); }
   "false"   { return symbol("false", Parser_sym.FALSE); }
   "this"   { return symbol("this", Parser_sym.THIS); }
   "new"   { return symbol("new", Parser_sym.NEW); }
   "main"   { return symbol("main", Parser_sym.MAIN); }
   "&&"     { return symbol("&&", Parser_sym.AND); }
   "!"      { return symbol("!", Parser_sym.BANG); }
   "."      { return symbol(".", Parser_sym.DOT); }
   ","      { return symbol(",", Parser_sym.COMMA); }
   ";"      { return symbol(";", Parser_sym.SEMICOLON); }
   "+"      { return symbol("+", Parser_sym.PLUS); }
   "-"      { return symbol("-", Parser_sym.MINUS); }
   "*"      { return symbol("*", Parser_sym.TIMES); }
   "/"      { return symbol("/", Parser_sym.DIVIDE); }
   "="      { return symbol("=", Parser_sym.EQ); }
   {id}     { return symbol("{id}", Parser_sym.IDENTIFIER, yytext()); }
   [0-9]+   { return symbol("{num}", Parser_sym.INT_VALUE, Integer.parseInt(yytext())); }
   "/*"     { commentDepth = 1; yybegin(MultiLineComment); }
   "*/"     { error("unexpected */"); }
   "//"     { yybegin(SingleLineComment); }
}
<SingleLineComment> {
   [^\n]+   { /* ignore */ }
   \n       { yybegin(YYINITIAL); }
}
<MultiLineComment> {
    "/*"    { commentDepth++; }
    "*/"    { commentDepth--; if (commentDepth==0) yybegin(YYINITIAL); }
    [^]|\n  { }
}


