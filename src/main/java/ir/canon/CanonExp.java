package ir.canon;

import java.util.LinkedList;
import java.util.List;

import ir.Temp;

/**
 * Visitor class to canonize tree expressions.
 * <p>
 *   Returns a canonized expression whose tree-exp-part
 *   that may contain a single CALL at the top. All other
 *   calls are moved to statements.
 * </p>
 */
class CanonExp implements ir.ExpVisitor<CanonizedExp> {

  /**
   * Method to canonize an expression into one that does not
   * contain any calls in the expression (only in the statements).
   */
  CanonizedExp canonNoTopCALL(ir.Exp e) {

    CanonizedExp ce = e.accept(this);
    if (ce.getExp() instanceof ir.ExpCall) {
      ir.Exp call = ce.getExp();
      ir.Exp t = new ir.ExpTemp(new Temp(), ce.getExp().getType());
      List<ir.Stm> stms = new LinkedList<>(ce.getBody());
      stms.add(new ir.StmMove(t, call));
      ce = new CanonizedExp(stms, t);
    }
    return ce;
  }

  @Override
  public CanonizedExp visit(ir.ExpConst expCONST) {
    return new CanonizedExp(expCONST);
  }

  @Override
  public CanonizedExp visit(ir.ExpName expNAME) {
    return new CanonizedExp(expNAME);
  }

  @Override
  public CanonizedExp visit(ir.ExpTemp expTEMP) {
    return new CanonizedExp(expTEMP);
  }

  @Override
  public CanonizedExp visit(ir.ExpParam expPARAM) {
    return new CanonizedExp(expPARAM);
  }

  @Override
  public CanonizedExp visit(ir.ExpMem expMEM) {
    return canonNoTopCALL(expMEM.getAddress()).mapExp(address -> new ir.ExpMem(address, expMEM.getType()));
  }

  @Override
  public CanonizedExp visit(ir.ExpBinOp expOP) {
    return CanonizedExp.combine(
            canonNoTopCALL(expOP.getLeft()),
            canonNoTopCALL(expOP.getRight()),
            (l, r) -> new ir.ExpBinOp(expOP.getOp(), l, r));
  }

  @Override
  public CanonizedExp visit(ir.ExpCall expCALL) {
    CanonizedExp cfunc = canonNoTopCALL(expCALL.getFunction());

    List<CanonizedExp> cargs = new LinkedList<>();
    for (ir.Exp arg : expCALL.getArguments()) {
      cargs.add(canonNoTopCALL(arg));
    }

    return CanonizedExp.combine(cfunc, cargs, (func, args) -> new ir.ExpCall(func, args, expCALL.getType()));
  }

  @Override
  public CanonizedExp visit(ir.ExpESeq expESEQ) {
    CanonizedExp b1 = new CanonizedExp(expESEQ.getStm().accept(new CanonStm()), new ir.ExpConst(0));
    CanonizedExp cres = canonNoTopCALL(expESEQ.getExp());
    return CanonizedExp.combine(b1, cres, (nop, e) -> e);
  }
}
