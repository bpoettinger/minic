package typechecker;

import ast.*;

/**
 * Created by bernhard on 17.11.17.
 */
public class ErrorCounter implements ErrorHandler {
    private final ErrorHandler errorHandler;
    private int numErrors = 0;

    public ErrorCounter(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    private void incrementErrors() {
        numErrors += 1;
    }

    public boolean hasErrors() {
        return numErrors > 0;
    }

    @Override
    public void onTypeError(Type actual, Type expected, String message, Exp e) {
        incrementErrors();
        errorHandler.onTypeError(actual, expected, message, e);
    }

    @Override
    public void onTypeError(Type actual, Type expected, String message, ast.Type e) {
        incrementErrors();
        errorHandler.onTypeError(actual, expected, message, e);
    }

    @Override
    public void onNameClash(Member m, String message) {
        incrementErrors();
        errorHandler.onNameClash(m, message);
    }

    @Override
    public void onNameClash(ClassDeclaration c, String message) {
        incrementErrors();
        errorHandler.onNameClash(c, message);
    }
    @Override
    public void onInvalidArgumentCount(int actual, int expected, String message, ExpInvoke e) {
        incrementErrors();
        errorHandler.onInvalidArgumentCount(actual, expected, message, e);
    }

    @Override
    public void onUnknownEntity(String message, Exp e) {
        incrementErrors();
        errorHandler.onUnknownEntity(message, e);
    }

    @Override
    public void onUnknownEntity(String message, StmAssign s) {
        incrementErrors();
        errorHandler.onUnknownEntity(message, s);
    }

    @Override
    public void onNameClash(StmVariableDeclaration s, String s1) {
        incrementErrors();
        errorHandler.onNameClash(s, s1);
    }

    @Override
    public void onNameClash(Parameter parameter, String s) {
        incrementErrors();
        errorHandler.onNameClash(parameter, s);
    }

    @Override
    public void onUnknownEntity(String s, ClassDeclaration class_) {
        incrementErrors();
        errorHandler.onUnknownEntity(s, class_);
    }

    @Override
    public void onUnknownEntity(String s, ast.Type type) {
        incrementErrors();
        errorHandler.onUnknownEntity(s, type);
    }

    @Override
    public void onUnknownEntity(String s, StmArrayAssign s1) {
        incrementErrors();
        errorHandler.onUnknownEntity(s, s1);
    }
}
