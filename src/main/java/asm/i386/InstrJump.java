package asm.i386;

import ir.Label;
import ir.Temp;
import util.Pair;

import java.util.*;
import java.util.function.Function;

final class InstrJump implements I386Instruction {

    enum Kind {
        JMP, J, CALL
    }

    enum Cond {
        E, NE, L, LE, G, GE, Z
    }

    private final Kind kind;
    private final Label label;
    private Operand dest;
    private final Cond cond;

    InstrJump(Kind kind, Label label) {
        this(kind, label, null, null);
    }

    InstrJump(Kind kind, Operand dest) {
        this(kind, null, dest, null);
    }

    InstrJump(Cond cond, Label label) {
        this(Kind.J, label, null, cond);
    }

    InstrJump(Kind kind, Label label, Operand dest, Cond cond) {
        assert (kind != Kind.J || cond != null) : "J needs condition argument";
        assert (kind == Kind.CALL || label != null) : "J and JMP need label as destination";
        //assert (dest == null || dest instanceof Operand.Reg) : "dynamic destination of CALL must be Reg";
        this.kind = kind;
        this.label = label;
        this.dest = dest;
        this.cond = cond;
    }

    @Override
    public Collection<Temp> use() {
        switch (kind) {
            case JMP:
            case J:
                return Collections.emptyList();
            case CALL:
                return dest != null ? dest.use() : Collections.emptyList();
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public Collection<Temp> def() {
        switch (kind) {
            case JMP:
            case J:
                return Collections.emptyList();
            case CALL:
                return Arrays.asList(Registers.eax.reg, Registers.ecx.reg, Registers.edx.reg);
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public Collection<Label> jumps() {
        return label != null ? Collections.singletonList(label) : Collections.emptyList();
    }

    @Override
    public boolean isFallThrough() {
        switch (kind) {
            case JMP:
                return false;
            case J:
            case CALL:
                return true;
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public Pair<Temp, Temp> isMoveBetweenTemps() {
        return null;
    }

    @Override
    public Label isLabel() {
        return null;
    }

    @Override
    public String toString() {
        String ins = (kind == Kind.J) ? (kind.toString() + cond.toString()) : kind.toString();
        return "\t" + ins + " " + (label != null ? label : dest) + "\n";
    }

    @Override
    public void rename(Function<Temp, Temp> sigma) {
        if (dest != null) {
            dest = dest.rename(sigma);
        }
    }

    @Override
    public List<Operand.Reg> getSpillableOperands() {
        return Collections.emptyList();
    }

    @Override
    public void replaceSpillableOperand(Operand original, Operand replacement) {

    }
}
