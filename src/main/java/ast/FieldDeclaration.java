package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Created by bernhard on 06.11.17.
 */
public class FieldDeclaration extends Member {

    private final Type type;
    private final String name;

    public FieldDeclaration(Type type, String name, ComplexSymbolFactory.Location from, ComplexSymbolFactory.Location to) {
        super(from, to);
        this.type = type;
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
