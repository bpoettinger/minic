package ast;

import java_cup.runtime.ComplexSymbolFactory;

import java.util.Collections;
import java.util.List;

public class MethodDeclaration extends Member {

  private final Type returnType;
  private final Boolean throwsIOException;
  private final String methodName;
  private final List<Parameter> parameters;
  private final Stm body;
  private final Exp returnExp;

  public MethodDeclaration(Type returnType,
                           boolean throwsIOException,
                           String methodName,
                           List<Parameter> parameters,
                           Stm body,
                           Exp returnExp,
                           ComplexSymbolFactory.Location left,
                           ComplexSymbolFactory.Location right) {
    super(left, right);
    this.returnType = returnType;
    this.throwsIOException = throwsIOException;
    this.methodName = methodName;
    this.parameters = parameters;
    this.body = body;
    this.returnExp = returnExp;
  }

  public Type getReturnType() {
    return returnType;
  }

  public Boolean throwsIOException() {
    return throwsIOException;
  }

  public String getMethodName() {
    return methodName;
  }

  public List<Parameter> getParameters() {
    return Collections.unmodifiableList(parameters);
  }

  public Stm getBody() {
    return body;
  }

  public Exp getReturnExp() {
    return returnExp;
  }
}

