package ir.eval;

import ir.*;

import java.util.List;
import java.util.stream.Collectors;

public class StatementVisitor implements StmVisitor<Stm> {
    @Override
    public Stm visit(StmMove stmMOVE) {
        final ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        return new StmMove(stmMOVE.getDst().accept(expressionVisitor), stmMOVE.getSrc().accept(expressionVisitor));
    }

    @Override
    public Stm visit(StmJump stmJUMP) {
        return stmJUMP;
    }

    @Override
    public Stm visit(StmCJump stmCJUMP) {
        final ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        return new StmCJump(stmCJUMP.getRel(),
                stmCJUMP.getLeft().accept(expressionVisitor),
                stmCJUMP.getRight().accept(expressionVisitor),
                stmCJUMP.getLabelTrue(),
                stmCJUMP.getLabelFalse());
    }

    @Override
    public Stm visit(StmSeq stmSEQ) {
        final List<Stm> stms = stmSEQ.getStms().stream().map(s -> s.accept(this)).collect(Collectors.toList());
        return new StmSeq(stms);
    }

    @Override
    public Stm visit(StmLabel stmLABEL) {
        return stmLABEL;
    }
}
