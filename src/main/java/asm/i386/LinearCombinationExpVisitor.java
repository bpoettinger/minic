package asm.i386;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.Exp;
import ir.ExpBinOp;
import ir.ExpCall;
import ir.ExpConst;
import ir.ExpESeq;
import ir.ExpMem;
import ir.ExpName;
import ir.ExpParam;
import ir.ExpTemp;
import ir.Temp;

public class LinearCombinationExpVisitor implements ir.ExpVisitor<Void> {

	private static final List<Integer> allowedCoefficients = Arrays.asList(1, 2, 4, 8);
	private final LinearCombination linearComb = new LinearCombination();
	private final Map<Temp, Exp> delayedExpressions = new HashMap<>();

	private boolean isLinear = true;

	@Override
	public Void visit(ExpConst expCONST) {
		linearComb.putUnit(expCONST.getValue());
		return null;
	}

	@Override
	public Void visit(ExpName expNAME) {
		assert (false);
		return null;
	}

	@Override
	public Void visit(ExpTemp expTEMP) {
		linearComb.putReg(expTEMP.getTemp(), 1);
		return null;
	}

	@Override
	public Void visit(ExpParam expPARAM) {
		Temp temp = new Temp();
		delayedExpressions.put(temp, expPARAM);
		linearComb.putReg(temp, 1);
		return null;
	}

	@Override
	public Void visit(ExpMem expMEM) {
		Temp temp = new Temp();
		delayedExpressions.put(temp, expMEM);
		linearComb.putReg(temp, 1);
		return null;
	}

	@Override
	public Void visit(ExpBinOp expOP) {
		switch (expOP.getOp()) {
		case PLUS:
			expOP.getLeft().accept(this);
			expOP.getRight().accept(this);
			break;
		case MUL:
			if(!linearizeMultiplication(expOP.getLeft(), expOP.getRight())) {
				if(!linearizeMultiplication(expOP.getRight(), expOP.getLeft())) {
					isLinear = false;
				}
			}
			break;
		default:
			isLinear = false;
		}
		return null;
	}

	@Override
	public Void visit(ExpCall expCALL) {
		isLinear = false;
		return null;
	}

	@Override
	public Void visit(ExpESeq expESEQ) {
		assert (false);
		return null;
	}

	public LinearCombination getLinearComb() {
		return linearComb;
	}

	public Map<Temp, Exp> getDelayedExpressions() {
		return delayedExpressions;
	}

	public boolean isLinear() {
		for(int coefficient : linearComb.getCoefficients().values()) {
			if(!allowedCoefficients.contains(coefficient)) {
				isLinear = false;
			}
		}
		return isLinear;
	}

	private boolean linearizeMultiplication(Exp first, Exp second) {
		if(first instanceof ExpConst) {
			if(second instanceof ExpTemp) {
				linearComb.putReg(((ExpTemp) second).getTemp(), ((ExpConst) first).getValue());
			} else if(second instanceof ExpConst) {
				linearComb.putUnit(((ExpConst) second).getValue() * ((ExpConst) first).getValue());
			} else {
				Temp temp = new Temp();
				delayedExpressions.put(temp, second);
				linearComb.putReg(temp, ((ExpConst) first).getValue());
			}
			return true;
		}
		return false;
	}
}
