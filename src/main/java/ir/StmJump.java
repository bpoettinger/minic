package ir;

import java.util.Collections;
import java.util.List;

public class StmJump extends Stm {
  private final Exp dest;
  private final List<Label> possibleTargets;

  public StmJump(Exp dest, List<Label> possibleTargets) {
    if (dest == null || possibleTargets == null) {
      throw new NullPointerException();
    }
    this.dest = dest;
    this.possibleTargets = possibleTargets;
  }

  public Exp getDst() {
    return dest;
  }

  public List<Label> getPossibleTargets() {
    return possibleTargets;
  }

  public StmJump(Label l) {
    this(new ExpName(l), Collections.singletonList(l));
  }

  @Override
  public <A> A accept(StmVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder("JUMP(" + getDst() + ", ");
    String sep = "";
    for (Label l : getPossibleTargets()) {
      s.append(sep).append(l);
      sep = ", ";
    }
    s.append(")");
    return s.toString();
  }

}
