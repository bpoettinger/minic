package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents {@code id = exp;}.
 */
public class StmAssign extends Stm {

  private final String id;
  private final Exp exp;

  private ComplexSymbolFactory.Location leftNameLocation;
  private ComplexSymbolFactory.Location rightNameLocation;

  public StmAssign(String id, Exp exp, ComplexSymbolFactory.Location leftNameLocation, ComplexSymbolFactory.Location rightNameLocation) {
    this.id = id;
    this.exp = exp;
    this.leftNameLocation = leftNameLocation;
    this.rightNameLocation = rightNameLocation;
  }

  @Override
  public <A, T extends Throwable> A accept(StmVisitor<A, T> v) throws T {
    return v.visit(this);
  }

  public String getId() {
    return id;
  }

  public Exp getExp() {
    return exp;
  }

  public ComplexSymbolFactory.Location getLeftNameLocation() {
    return leftNameLocation;
  }

  public ComplexSymbolFactory.Location getRightNameLocation() {
    return rightNameLocation;
  }
}
