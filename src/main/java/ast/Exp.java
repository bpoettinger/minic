package ast;

import java_cup.runtime.ComplexSymbolFactory;
import prettyprinter.PrettyPrinter;

public abstract class Exp extends Node {

  public abstract <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T;

  public Exp(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
  }

  public String prettyPrint() {
    return accept(new PrettyPrinter.PrettyPrintVisitorExp());
  }
}



