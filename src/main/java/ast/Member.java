package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Created by bernhard on 06.11.17.
 */
public class Member extends Node {
    public Member(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
        super(left, right);
    }
}
