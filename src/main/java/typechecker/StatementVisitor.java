package typechecker;

import ast.StmVariableDeclaration;

/**
 * Created by bernhard on 06.11.17.
 */
class StatementVisitor implements ast.StmVisitor<Void, TypeError> {

    private final ExecutionScope scope;
    private boolean throwsException = false;
    private final ErrorHandler errorHandler;

    public StatementVisitor(ExecutionScope scope, ErrorHandler errorHandler) {
        this.scope = scope;
        this.errorHandler = errorHandler;
    }

    public boolean throwsException() {
        return throwsException;
    }

    @Override
    public Void visit(ast.StmSeq s) throws TypeError {
        for (ast.Stm statement : s.getStmList()) {
            statement.accept(this);
        }
        return null;
    }

    private void checkSubExpression(ast.Exp e, Type expectedType) {
        final ExpressionVisitor visitor = new ExpressionVisitor(scope, errorHandler);
        final Type actualType = e.accept(visitor);
        if (!actualType.equals(expectedType)) {
            errorHandler.onTypeError(actualType, expectedType, "Invalid type", e);
        }
        throwsException |= visitor.throwsException();
    }

    private void checkSubStatementInNewScope(ast.Stm s) {
        final ExecutionScope innerScope = new ExecutionScope(scope);
        final StatementVisitor visitor = new StatementVisitor(innerScope, errorHandler);
        s.accept(visitor);
        throwsException |= visitor.throwsException();
    }

    @Override
    public Void visit(ast.StmIf s) throws TypeError {
        checkSubExpression(s.getCond(), Type.BOOLEAN);
        checkSubStatementInNewScope(s.getTrueBranch());
        checkSubStatementInNewScope(s.getFalseBranch());

        return null;
    }

    @Override
    public Void visit(ast.StmWhile s) throws TypeError {
        checkSubExpression(s.getCond(), Type.BOOLEAN);
        checkSubStatementInNewScope(s.getBody());

        return null;
    }

    @Override
    public Void visit(ast.StmPrintln s) throws TypeError {
        checkSubExpression(s.getArg(), Type.INT);
        throwsException = true;

        return null;
    }

    @Override
    public Void visit(ast.StmWrite s) throws TypeError {
        checkSubExpression(s.getExp(), Type.INT);
        throwsException = true;

        return null;
    }

    @Override
    public Void visit(ast.StmAssign s) throws TypeError {
        if (scope.hasVariableGlobally(s.getId())) {
            final Variable destVariable = scope.getVariable(s.getId());
            checkSubExpression(s.getExp(), destVariable.getType());
        } else {
            errorHandler.onUnknownEntity("Unknown variable", s);
        }

        return null;
    }

    @Override
    public Void visit(ast.StmArrayAssign s) throws TypeError {
        if (scope.hasVariableGlobally(s.getId())) {
            final Variable destVariable = scope.getVariable(s.getId());
            final Type.ArrayType arrayType = (Type.ArrayType) destVariable.getType();
            checkSubExpression(s.getExp(), arrayType.getInnerType());
        } else {
            errorHandler.onUnknownEntity("Unknown variable", s);
        }

        checkSubExpression(s.getIndexExp(), Type.INT);

        return null;
    }

    @Override
    public Void visit(StmVariableDeclaration s) throws TypeError {
        final TypeVisitor typeVisitor = new TypeVisitor(scope, errorHandler);
        final Type type = s.getType().accept(typeVisitor);

        if (s.getInitializer().isPresent()) {
            checkSubExpression(s.getInitializer().get(), type);
        }

        if (!scope.hasClassGlobally(s.getType().toString())) {
            errorHandler.onUnknownEntity("Unknown class", s.getType());
        }

        if (!scope.hasVariableInExecutionScope(s.getName())) {
            scope.addVariable(s.getName(), new Variable(type));
        } else {
            errorHandler.onNameClash(s, "Variable already exists");
        }
        return null;
    }
}
