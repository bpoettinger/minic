package translator;
import ast.*;
import ast.StmSeq;
import ir.*;
import ir.Exp;
import ir.Stm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by bernhard on 13.11.17.
 */
public class StatementVisitor implements ast.StmVisitor<ir.Stm, Error> {
    
    private Env env;
    
    public StatementVisitor(Env env) {
        this.env = env;
    }
    
    @Override
    public ir.Stm visit(StmSeq s) throws Error {
        final List<ir.Stm> stms = new ArrayList<>();
        for (ast.Stm stm : s.getStmList()) {
            stms.add(stm.accept(this));
        }

        final ir.StmSeq seq = new ir.StmSeq(stms);
        return new ir.StmSeq(Collections.singletonList(seq));
    }

    @Override
    public ir.Stm visit(StmIf s) throws Error {
        final ir.Label trueBranch = new ir.Label();
        final ir.Label falseBranch = new ir.Label();
        final ir.Label endLabel = new ir.Label();

        final ir.Stm trueStatement = s.getTrueBranch().accept(this);
        final ir.Stm falseStatement = s.getFalseBranch().accept(this);

        final ConditionVisitor conditionVisitor = new ConditionVisitor(env, trueBranch, falseBranch);
        final ir.Stm condition = s.getCond().accept(conditionVisitor);
        
        return new ir.StmSeq(
                condition,
                new ir.StmLabel(trueBranch),
                trueStatement,
                new ir.StmJump(endLabel),
                new ir.StmLabel(falseBranch),
                falseStatement,
                new ir.StmLabel(endLabel)
        );
    }

    @Override
    public ir.Stm visit(StmWhile s) throws Error {
        final ir.Label startLabel = new ir.Label();
        final ir.Label trueLabel = new ir.Label();
        final ir.Label falseLabel = new ir.Label();

        final ConditionVisitor conditionVisitor = new ConditionVisitor(env, trueLabel, falseLabel);
        final ir.Stm condition = s.getCond().accept(conditionVisitor);

        final ir.Stm body = s.getBody().accept(this);

        return new ir.StmSeq(
                new ir.StmLabel(startLabel),
                condition,
                new ir.StmLabel(trueLabel),
                body,
                new ir.StmJump(startLabel),
                new ir.StmLabel(falseLabel)
        );
    }

    @Override
    public ir.Stm visit(StmPrintln s) throws Error {
        final ExpressionVisitor argumentVisitor = new ExpressionVisitor(env);
        final ir.Exp arg = s.getArg().accept(argumentVisitor);

        final ir.Exp tmp = new ir.ExpTemp(new ir.Temp(), ir.Exp.Type.INTEGER);

        return new ir.StmMove(tmp, new ir.ExpCall(new ir.ExpName(new ir.Label("_println_int")), Collections.singletonList(arg), ir.Exp.Type.INTEGER));
    }

    @Override
    public ir.Stm visit(StmWrite s) throws Error {
        final ExpressionVisitor argumentVisitor = new ExpressionVisitor(env);
        final ir.Exp arg = s.getExp().accept(argumentVisitor);

        final ir.Exp tmp = new ir.ExpTemp(new ir.Temp(), ir.Exp.Type.INTEGER);

        return new ir.StmMove(tmp, new ir.ExpCall(new ir.ExpName(new ir.Label("_write")), Collections.singletonList(arg), ir.Exp.Type.INTEGER));
    }

    @Override
    public ir.Stm visit(StmAssign s) throws Error {
        final ExpressionVisitor visitor = new ExpressionVisitor(env);
        final ir.Exp value = s.getExp().accept(visitor);

        return new ir.StmMove(env.getVariable(s.getId()), value);
    }

    @Override
    public ir.Stm visit(StmArrayAssign s) throws Error {
        final ExpressionVisitor visitor = new ExpressionVisitor(env);
        final ir.Exp value = s.getExp().accept(visitor);

        final ir.Exp array = env.getVariable(s.getId());
        final ir.Exp index = s.getIndexExp().accept(visitor);

        final boolean isTrivialIndex = index instanceof ExpConst || index instanceof ExpMem || index instanceof ExpTemp;
        final boolean isTrivialArray = array instanceof ExpTemp;
        final boolean isValidConstIndex = index instanceof ExpConst && ((ExpConst) index).getValue() >= 0;

        final Exp indexValue = isTrivialIndex ? index : new ExpTemp(new Temp(), ir.Exp.Type.INTEGER);
        final Exp arrayValue = isTrivialArray ? array : new ExpTemp(new Temp(), ir.Exp.Type.OBJECT);
        final Exp arrayLength = new ir.ExpMem(arrayValue, ir.Exp.Type.INTEGER);

        final Label raiseLabel = new Label("_raise_dispatch");
        final Label boundObeyedLabel1 = new Label();
        final Label boundObeyedLabel2 = new Label();

        final Stm calcIndexValue = new ir.StmMove(indexValue, index);
        final Stm calcArrayValue = new ir.StmMove(arrayValue, array);

        final Exp element = new ir.ExpBinOp(ir.ExpBinOp.Op.PLUS,
                arrayValue,
                new ir.ExpBinOp(ir.ExpBinOp.Op.PLUS,
                        new ir.ExpConst(4),
                        new ir.ExpBinOp(ir.ExpBinOp.Op.MUL,
                                new ir.ExpConst(4),
                                indexValue)));

        return new ir.StmSeq(
                isTrivialArray ? Stm.getNOP() : calcArrayValue,
                isTrivialIndex ? Stm.getNOP() : calcIndexValue,
                new ir.StmCJump(StmCJump.Rel.LE, arrayLength, indexValue, raiseLabel, boundObeyedLabel1),
                new ir.StmLabel(boundObeyedLabel1),
                isValidConstIndex ? Stm.getNOP() : new ir.StmCJump(StmCJump.Rel.LT, indexValue, new ir.ExpConst(0), raiseLabel, boundObeyedLabel2),
                isValidConstIndex ? Stm.getNOP() : new ir.StmLabel(boundObeyedLabel2),
                new ir.StmMove(new ir.ExpMem(element, ir.Exp.Type.INTEGER), value)
        );
    }

    @Override
    public Stm visit(StmVariableDeclaration s) throws Error {
        env.addLocalVariable(s.getName(),
                s.getType() instanceof ast.TypeArray || s.getType() instanceof ast.TypeClass
                        ? ir.Exp.Type.OBJECT : ir.Exp.Type.INTEGER);

        if (s.getInitializer().isPresent()) {
            final ExpressionVisitor initializerVisitor = new ExpressionVisitor(env);
            final ir.Exp value = s.getInitializer().get().accept(initializerVisitor);

            return new ir.StmMove(env.getVariable(s.getName()), value);
        } else {
            return Stm.getNOP();
        }
    }
}
