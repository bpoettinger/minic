package ir;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class CmmPrinter {

  private StringBuilder declarations;
  private StringBuilder code;
  private int codeIndent;

  public String prgToCmm(Prg prg) {
    declarations = new StringBuilder();
    code = new StringBuilder();
    codeIndent = 0;

    declarations.append("#include <stdint.h>\n");
    declarations.append("#define MEM(x) *((int32_t*)(x))\n\n");
    declarations.append("int32_t L_halloc(int32_t size);\n");
    declarations.append("int32_t L_println_int(int32_t n);\n");
    declarations.append("int32_t L_write(int32_t n);\n");
    declarations.append("int32_t L_read();\n");
    declarations.append("int32_t L_raise(int32_t rc);\n");

    for(VTable vTable : prg.getVTables()) {
    	vTableToCmm(vTable);
    }
    
    for (Function m : prg) {
      methodToCmm(m);
    }

    return declarations.toString() + "\n\n" + code.toString();
  }

  private void vTableToCmm(VTable vTable) {
		code.append("int32_t LvTable_" + vTable.getClassName() + " [] = {\n");
		code.append("0");
		vTable.getValues().stream().forEach(label -> code.append(",\n(int32_t) " + label.getLabel().toString()));
		code.append("\n};\n");
  }

private void methodToCmm(Function method) {

    // function prototype
    StringBuilder prototype = new StringBuilder();
    prototype.append("int32_t ").append(method.getName()).append("(");
    String sep = "";
    for (int i = 0; i < method.getNumberOfParameters(); i++) {
      prototype.append(sep).append("int32_t param").append(i);
      sep = ", ";
    }
    prototype.append(")");

    declarations.append(prototype);
    declarations.append(";\n");

    code.append(prototype);
    code.append(" {\n");
    codeIndent++;

    // declare locals
    Set<Temp> temps = new TreeSet<>();
    TempsInTreeStm tempsv = new TempsInTreeStm();
    for (Stm stm : method) {
      temps.addAll(stm.accept(tempsv));
    }
    if (temps.size() > 0) {
      StringBuilder decls = new StringBuilder();
      sep = "";
      for (Temp t : temps) {
        decls.append(sep);
        decls.append(t);
        sep = ", ";
      }
      emit("int32_t " + decls + ";");
    }

    // function body
    TreeStmToCmm tv = new TreeStmToCmm();
    for (Stm stm : method) {
      emit("/* " + stm.toString() + " */");
      stm.accept(tv);
    }

    // return
    emit("return " + method.getReturnTemp() + ";");
    codeIndent--;
    emit("}");
    code.append("\n");
  }

  private void emit(String s) {
    for (int i = 0; i < codeIndent; i++) {
      code.append("  ");
    }
    code.append(s);
    code.append("\n");
  }

  private Temp emitVarDecl(String v) {
    Temp t = new Temp();
    emit("int32_t " + t + " = " + v + ";");
    return t;
  }

  private class TreeExpToCmm implements ExpVisitor<String> {

    private String gen(Exp e) {
      return e.accept(this);
    }

    private String genVarDecl(Exp e) {
      String v = e.accept(this);
      if (e instanceof ExpConst || e instanceof ExpName) {
        return v;
      } else {
        return emitVarDecl(v).toString();
      }
    }

    private void gen(Stm s) {
      s.accept(new TreeStmToCmm());
    }

    @Override
    public String visit(ExpConst expCONST) {
      return "" + expCONST.getValue();
    }

    @Override
    public String visit(ExpName expNAME) {
      return "(int32_t)" + expNAME.getLabel().toString();
    }

    @Override
    public String visit(ExpTemp expTEMP) {
      return expTEMP.getTemp().toString();
    }

    @Override

    public String visit(ExpParam expPARAM) {
      return "param" + expPARAM.getNumber();
    }

    @Override
    public String visit(ExpMem expMEM) {
      return "MEM(" + gen(expMEM.getAddress()) + ")";
    }

    private String printOperator(ExpBinOp.Op op) {
      switch (op) {
        case PLUS:
          return "+";
        case MINUS:
          return "-";
        case MUL:
          return "*";
        case DIV:
          return "/";
        case AND:
          return "&";
        case OR:
          return "|";
        case LSHIFT:
          return "<<";
        case RSHIFT:
          return ">>";
        case XOR:
          return "^";
      }
      throw new UnsupportedOperationException("Operator " + op + " not supported in C--");
    }

    @Override
    public String visit(ExpBinOp expOP) {
      // C doesn't specify the evaluation ordering within expressions,
      // so we have to sequence possibly effectful expressions explicitly.
      String left = genVarDecl(expOP.getLeft());
      String right = genVarDecl(expOP.getRight());
      return "(" + left + " " + printOperator(expOP.getOp()) + " " + right + ")";
    }

    @Override
    public String visit(ExpCall expCALL) {
      StringBuilder call = new StringBuilder();
      StringBuilder argTypes = new StringBuilder();
      String sep = "";
      for (Exp arg : expCALL.getArguments()) {
        call.append(sep);
        call.append(genVarDecl(arg));
        argTypes.append(sep);
        argTypes.append("int32_t");
        sep = ", ";
      }
      Temp r;
      if (expCALL.getFunction() instanceof ExpName) {
        r = emitVarDecl(((ExpName) expCALL.getFunction()).getLabel() + "(" + call + ")");
      } else {
        r = emitVarDecl("((int32_t (*) (" + argTypes + "))(" + gen(expCALL.getFunction()) + "))(" + call + ")");
      }
      return r.toString();
    }

    @Override
    public String visit(ExpESeq expESEQ) {
      gen(expESEQ.getStm());
      return gen(expESEQ.getExp());
    }
  }

  private class TreeStmToCmm implements StmVisitor<Void> {

    private String gen(Exp e) {
      return e.accept(new TreeExpToCmm());
    }

    private String genVarDecl(Exp e) {
      String v = e.accept(new TreeExpToCmm());
      if (e instanceof ExpConst || e instanceof ExpName) {
        return v;
      } else {
        return emitVarDecl(v).toString();
      }
    }

    private void gen(Stm s) {
      s.accept(this);
    }

    @Override
    public Void visit(StmMove stmMOVE) {
      if (stmMOVE.getDst() instanceof ExpTemp) {
        ExpTemp dst = (ExpTemp) stmMOVE.getDst();
        String src = gen(stmMOVE.getSrc());
        emit(dst.getTemp() + " = " + src + ";");
      } else if (stmMOVE.getDst() instanceof ExpParam) {
        ExpParam dst = (ExpParam) stmMOVE.getDst();
        String src = gen(stmMOVE.getSrc());
        emit("param" + dst.getNumber() + " = " + src + ";");
      } else if (stmMOVE.getDst() instanceof ExpMem) {
        ExpMem dst = (ExpMem) stmMOVE.getDst();
        String a = genVarDecl(dst.getAddress());
        String src = gen(stmMOVE.getSrc());
        emit("MEM(" + a + ")" + " = " + src + ";");
      } else if (stmMOVE.getDst() instanceof ExpESeq) {
        ExpESeq dst = (ExpESeq) stmMOVE.getDst();
        new StmSeq(dst.getStm(), new StmMove(dst.getExp(), stmMOVE.getSrc())).accept(this);
      } else {
        throw new UnsupportedOperationException("Left-hand side of MOVE must be TEMP, PARAM, MEM or ESEQ!");
      }
      return null;
    }

    @Override
    public Void visit(StmJump stmJUMP) {
      if (stmJUMP.getDst() instanceof ExpName) {
        emit("goto " + ((ExpName) stmJUMP.getDst()).getLabel() + ";");
      } else {
        throw new UnsupportedOperationException("Only calls to labels are implemented!");
      }
      return null;
    }

    private String printRelation(StmCJump.Rel rel) {
      switch (rel) {
        case EQ:
          return "==";
        case NE:
          return "!=";
        case LT:
          return "<";
        case GT:
          return ">";
        case LE:
          return "<=";
        case GE:
          return ">=";
      }
      throw new UnsupportedOperationException("CJUMP relation " + rel + " not supported in C--");
    }

    @Override
    public Void visit(StmCJump stmCJUMP) {
      // C doesn't specify the evaluation ordering within expressions,
      // so we have to sequence possibly effectful expressions explicitly.
      String left = genVarDecl(stmCJUMP.getLeft());
      String right = genVarDecl(stmCJUMP.getRight());

      emit("if ("
              + left + " "
              + printRelation(stmCJUMP.getRel()) + " "
              + right + ") "
              + "goto " + stmCJUMP.getLabelTrue()
              + "; else goto " + stmCJUMP.getLabelFalse() + ";");
      return null;
    }

    @Override
    public Void visit(StmSeq stmSEQ) {
      for (Stm s: stmSEQ.getStms()) {
        gen(s);
      }
      return null;
    }

    @Override
    public Void visit(StmLabel stmLABEL) {
      codeIndent--;
      emit(stmLABEL.getLabel().toString() + ": ;");
      codeIndent++;
      return null;
    }
  }

  static class TempsInTreeExp implements ExpVisitor<Set<Temp>> {

    private Set<Temp> temps(Exp e) {
      return e.accept(this);
    }

    private Set<Temp> temps(Stm s) {
      return s.accept(new TempsInTreeStm());
    }

    @Override
    public Set<Temp> visit(ExpConst expCONST) {
      return new TreeSet<>();
    }

    @Override
    public Set<Temp> visit(ExpName expNAME) {
      return new TreeSet<>();
    }

    @Override
    public Set<Temp> visit(ExpTemp expTEMP) {
      Set<Temp> s = new TreeSet<>();
      s.add(expTEMP.getTemp());
      return s;
    }

    @Override
    public Set<Temp> visit(ExpParam expPARAM) {
      return new TreeSet<>();
    }

    @Override
    public Set<Temp> visit(ExpMem expMEM) {
      return temps(expMEM.getAddress());
    }

    @Override
    public Set<Temp> visit(ExpBinOp expOP) {
      Set<Temp> s = temps(expOP.getLeft());
      s.addAll(temps(expOP.getRight()));
      return s;
    }

    @Override
    public Set<Temp> visit(ExpCall expCALL) {
      Set<Temp> s = new TreeSet<>();
      for (Exp arg : expCALL.getArguments()) {
        s.addAll(temps(arg));
      }
      return s;
    }

    @Override
    public Set<Temp> visit(ExpESeq expESEQ) {
      Set<Temp> s = temps(expESEQ.getStm());
      s.addAll(temps(expESEQ.getExp()));
      return s;
    }
  }

  static class TempsInTreeStm implements StmVisitor<Set<Temp>> {

    private Set<Temp> temps(Exp e) {
      return e.accept(new TempsInTreeExp());
    }

    private Set<Temp> temps(Stm s) {
      return s.accept(this);
    }

    @Override
    public Set<Temp> visit(StmMove stmMOVE) {
      Set<Temp> s = temps(stmMOVE.getDst());
      s.addAll(temps(stmMOVE.getSrc()));
      return s;
    }

    @Override
    public Set<Temp> visit(StmJump stmJUMP) {
      return temps(stmJUMP.getDst());
    }

    @Override
    public Set<Temp> visit(StmCJump stmCJUMP) {
      Set<Temp> s = temps(stmCJUMP.getLeft());
      s.addAll(temps(stmCJUMP.getRight()));
      return s;
    }

    @Override
    public Set<Temp> visit(StmSeq stmSEQ) {
      Set<Temp> ts = new HashSet<>();
      for (Stm s: stmSEQ.getStms()) {
        ts.addAll(temps(s));
      }
      return ts;
    }

    @Override
    public Set<Temp> visit(StmLabel stmLABEL) {
      return new TreeSet<>();
    }
  }
}
