package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents {@code id[index] = exp;}.
 */
public class StmArrayAssign extends Stm {

  private final String id;
  private final Exp index;
  private final Exp exp;

  private ComplexSymbolFactory.Location leftNameLocation;
  private ComplexSymbolFactory.Location rightNameLocation;

  public StmArrayAssign(String id, Exp index, Exp exp, ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    this.id = id;
    this.index = index;
    this.exp = exp;
    this.leftNameLocation = left;
    this.rightNameLocation = right;
  }

  @Override
  public <A, T extends Throwable> A accept(StmVisitor<A, T> v) throws T {
    return v.visit(this);
  }

  public String getId() {
    return id;
  }

  public Exp getIndexExp() {
    return index;
  }

  public Exp getExp() {
    return exp;
  }

  public ComplexSymbolFactory.Location getLeftNameLocation() {
    return leftNameLocation;
  }

  public ComplexSymbolFactory.Location getRightNameLocation() {
    return rightNameLocation;
  }
}
