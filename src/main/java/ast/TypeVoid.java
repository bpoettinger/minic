package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents the type {@code void}.
 */
public class TypeVoid extends Type {

  public TypeVoid(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
  }

  @Override
  public String toString() {
    return "boolean";
  }

  @Override
  public <A> A accept(TypeVisitor<A> v) {
    return v.visit(this);
  }

  @Override
  public boolean equals(Object obj) {
    return (obj instanceof TypeVoid);
  }

  @Override
  public int hashCode() {
    return 7;
  }
}
