package asm.i386;

import java.util.HashMap;
import java.util.Map;

import ir.Temp;

public class LinearCombination {
	private final Map<Temp, Integer> coefficients = new HashMap<>();
	private int unit = 0;
	
	public LinearCombination() {
	}
	
	public void putReg(Temp temp, int coefficient) {
		coefficients.merge(temp, coefficient, Integer::sum);
	}
	
	public void putUnit(int unit) {
		this.unit += unit;
	}

	public int getUnit() {
		return unit;
	}

	public Map<Temp, Integer> getCoefficients() {
		return coefficients;
	}
}
