package asm.i386;

import asm.MachineInstruction;

import java.util.ArrayList;
import java.util.List;

public class InstructionEmitter {

    private List<MachineInstruction> instructions = new ArrayList<>();

    public void emit(MachineInstruction instruction) {
        instructions.add(instruction);
    }

    public List<MachineInstruction> getMachineInstructions() {
        return instructions;
    }
}
