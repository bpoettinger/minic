package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents {@code !exp}.
 */
public class ExpNeg extends Exp {

  private final Exp exp;

  public ExpNeg(Exp exp, ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
    this.exp = exp;
  }

  @Override
  public <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T {
    return v.visit(this);
  }

  public Exp getExp() {
    return exp;
  }
}
