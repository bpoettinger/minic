package translator;

import ast.*;

/**
 * Created by bernhard on 28.11.17.
 */
public class ConditionVisitor implements ExpVisitor<ir.Stm, Error> {

    private final Env env;
    private final ir.Label trueBranch;
    private final ir.Label falseBranch;
    
    public ConditionVisitor(Env env, ir.Label trueBranch, ir.Label falseBranch) {
        this.env = env;
        this.trueBranch = trueBranch;
        this.falseBranch = falseBranch;
    }
    
    @Override
    public ir.Stm visit(ExpTrue e) throws Error {
        return new ir.StmJump(trueBranch);
    }

    @Override
    public ir.Stm visit(ExpFalse e) throws Error {
        return new ir.StmJump(falseBranch);
    }

    @Override
    public ir.Stm visit(ExpThis e) throws Error {
        assert (false);
        return null;
    }

    @Override
    public ir.Stm visit(ExpNewIntArray e) throws Error {
        assert (false);
        return null;
    }

    @Override
    public ir.Stm visit(ExpNew e) throws Error {
        assert (false);
        return null;
    }

    @Override
    public ir.Stm visit(ExpNeg e) throws Error {
        final ConditionVisitor innerConditionVisitor = new ConditionVisitor(env, falseBranch, trueBranch);
        return e.getExp().accept(innerConditionVisitor);
    }

    @Override
    public ir.Stm visit(ExpBinOp e) throws Error {
        switch (e.getOp()) {
            case AND: {
                final ir.Label secondLabel = new ir.Label();
                final ConditionVisitor leftConditionVisitor = new ConditionVisitor(env, secondLabel, falseBranch);
                final ir.Stm left = e.getLeft().accept(leftConditionVisitor);
                final ConditionVisitor rightConditionVistor = new ConditionVisitor(env, trueBranch, falseBranch);
                final ir.Stm right = e.getRight().accept(rightConditionVistor);
                return new ir.StmSeq(
                        left,
                        new ir.StmLabel(secondLabel),
                        right
                );
            }
            case LT: {
                final ExpressionVisitor leftValueVisitor = new ExpressionVisitor(env);
                final ir.Exp left = e.getLeft().accept(leftValueVisitor);
                final ExpressionVisitor rightValueVisitor = new ExpressionVisitor(env);
                final ir.Exp right = e.getRight().accept(rightValueVisitor);
                return new ir.StmCJump(ir.StmCJump.Rel.LT, left, right, trueBranch, falseBranch);
            }
            default:
                assert (false);
                return null;
        }
    }

    @Override
    public ir.Stm visit(ExpArrayGet e) throws Error {
        assert (false);
        return null;
    }

    @Override
    public ir.Stm visit(ExpArrayLength e) throws Error {
        assert (false);
        return null;
    }

    @Override
    public ir.Stm visit(ExpInvoke e) throws Error {
        final ExpressionVisitor callVisitor = new ExpressionVisitor(env);
        final ir.Exp call = e.accept(callVisitor);
        return new ir.StmCJump(ir.StmCJump.Rel.EQ, new ir.ExpConst(1), call, trueBranch, falseBranch);
    }

    @Override
    public ir.Stm visit(ExpRead e) throws Error {
        assert (false);
        return null;
    }

    @Override
    public ir.Stm visit(ExpIntConst e) throws Error {
        assert (false);
        return null;
    }

    @Override
    public ir.Stm visit(ExpId e) throws Error {
        return new ir.StmCJump(ir.StmCJump.Rel.EQ, new ir.ExpConst(1), env.getVariable(e.getId()), trueBranch, falseBranch);
    }
}
