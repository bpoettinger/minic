package ir.eval;

import ir.*;

import java.util.List;
import java.util.stream.Collectors;

public class ExpressionVisitor implements ExpVisitor<Exp> {
    @Override
    public Exp visit(ExpConst expCONST) {
        return expCONST;
    }

    @Override
    public Exp visit(ExpName expNAME) {
        return expNAME;
    }

    @Override
    public Exp visit(ExpTemp expTEMP) {
        return expTEMP;
    }

    @Override
    public Exp visit(ExpParam expPARAM) {
        return expPARAM;
    }

    @Override
    public Exp visit(ExpMem expMEM) {
        return new ExpMem(expMEM.getAddress().accept(this), expMEM.getType());
    }

    @Override
    public Exp visit(ExpBinOp expOP) {
        final Exp left = expOP.getLeft().accept(this);
        final Exp right = expOP.getRight().accept(this);

        if (left instanceof ExpConst && right instanceof ExpConst) {
            final int lhs = ((ExpConst) left).getValue();
            final int rhs = ((ExpConst) right).getValue();
            int result;
            switch (expOP.getOp()) {
                case PLUS:
                    result = lhs + rhs;
                    break;
                case MINUS:
                    result = lhs - rhs;
                    break;
                case MUL:
                    result = lhs * rhs;
                    break;
                case DIV:
                    result = lhs / rhs;
                    break;
                default:
                    throw new RuntimeException();
            }

            return new ExpConst(result);
        } else {
            return new ExpBinOp(expOP.getOp(), left, right);
        }
    }

    @Override
    public Exp visit(ExpCall expCALL) {
        final List<Exp> args = expCALL.getArguments().stream().map(e -> e.accept(this)).collect(Collectors.toList());
        return new ExpCall(expCALL.getFunction(), args, expCALL.getType());
    }

    @Override
    public Exp visit(ExpESeq expESEQ) {
        final StatementVisitor statementVisitor = new StatementVisitor();
        return new ExpESeq(
                expESEQ.getStm().accept(statementVisitor),
                expESEQ.getExp().accept(this));
    }
}
