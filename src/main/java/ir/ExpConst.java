package ir;

public class ExpConst extends Exp {
  private final int value;

  public ExpConst(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  @Override
  public Type getType() {
    return Type.INTEGER;
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "CONST(" + getValue() + ")";
  }

}
