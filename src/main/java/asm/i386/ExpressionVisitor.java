package asm.i386;

import asm.MachineInstruction;
import ir.*;
import java.util.List;

public class ExpressionVisitor implements ir.ExpVisitor<Operand> {

    private final InstructionEmitter emitter;

    public ExpressionVisitor(InstructionEmitter emitter) {
        this.emitter = emitter;
    }

    private void emit(MachineInstruction instruction) {
        emitter.emit(instruction);
    }

    @Override
    public Operand visit(ExpConst expCONST) {
        throw new RuntimeException();
    }

    @Override
    public Operand visit(ExpName expNAME) {
        throw new RuntimeException();
    }

    @Override
    public Operand visit(ExpTemp expTEMP) {
        throw new RuntimeException();
    }

    @Override
    public Operand visit(ExpParam expPARAM) {
        throw new RuntimeException();
    }

    @Override
    public Operand visit(ExpMem expMEM) {
        throw new RuntimeException();
    }

    @Override
    public Operand visit(ExpBinOp expOP) {
        throw new RuntimeException();
    }

    @Override
    public Operand visit(ExpCall expCALL) {
        final Matcher matcher = new Matcher(emitter);

        if (expCALL.getFunction() instanceof ir.ExpName
                && ((ExpName) expCALL.getFunction()).getLabel().toString().equals("L_raise")) {
            emit(new InstrJump(InstrJump.Kind.CALL, new ir.Label("_raise_dispatch")));
            return new Operand.Imm(0);
        }

        final List<Exp> arguments = expCALL.getArguments();
        final int numArguments = arguments.size();
        final Operand.Imm requiredStackSpace = new Operand.Imm(((23 + 4 * numArguments) & ~0xf) - 8);
        
        if (requiredStackSpace.imm != 0) {
            emit(new InstrBinary(InstrBinary.Kind.SUB, Registers.esp, requiredStackSpace));
        }

        for (int i = 0; i < numArguments; ++i) {
            final Exp argument = arguments.get(i);
            Operand argumentValue = matcher.match(argument);
            final Operand dst = new Operand.Mem(Registers.esp.reg, null, null, 4 * i);
            if (argumentValue instanceof Operand.Mem) {
                final Operand.Reg newArgumentValue = new Operand.Reg(new Temp());
                emit(new InstrBinary(InstrBinary.Kind.MOV, newArgumentValue, argumentValue));
                argumentValue = newArgumentValue;
            }
            emit(new InstrBinary(InstrBinary.Kind.MOV, dst, argumentValue));
        }

        final Exp function = expCALL.getFunction();
        if (function instanceof ExpName) {
            emit(new InstrJump(InstrJump.Kind.CALL, ((ExpName) function).getLabel()));
        } else {
            Operand addr = matcher.match(function);
            if (!(addr instanceof Operand.Reg || addr instanceof Operand.Mem)) {
                final Operand target = new Operand.Reg(new Temp());
                emit(new InstrBinary(InstrBinary.Kind.MOV, target, addr));
                addr = target;
            }
            emit(new InstrJump(InstrJump.Kind.CALL, addr));
        }
        
        final Operand dst = new Operand.Reg(new Temp());
        emit(new InstrBinary(InstrBinary.Kind.MOV, dst, Registers.eax));

        if (requiredStackSpace.imm != 0) {
            emit(new InstrBinary(InstrBinary.Kind.ADD, Registers.esp, requiredStackSpace));
        }

        return dst;
    }

    @Override
    public Operand visit(ExpESeq expESEQ) {
        throw new RuntimeException();
    }
}
