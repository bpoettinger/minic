package asm.i386;

import ir.Temp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bernhard on ...
 */
public class Registers {
    public static Operand.Reg eax = new Operand.Reg(new Temp());
    public static Operand.Reg ebx = new Operand.Reg(new Temp());
    public static Operand.Reg ecx = new Operand.Reg(new Temp());
    public static Operand.Reg edx = new Operand.Reg(new Temp());
    public static Operand.Reg esp = new Operand.Reg(new Temp());
    public static Operand.Reg ebp = new Operand.Reg(new Temp());
    public static Operand.Reg esi = new Operand.Reg(new Temp());
    public static Operand.Reg edi = new Operand.Reg(new Temp());

    public static Map<Operand.Reg, String> names = createNames();

    private static Map<Operand.Reg, String> createNames() {
        final Map<Operand.Reg, String> names = new HashMap<>();
        names.put(eax, "eax");
        names.put(ebx, "ebx");
        names.put(ecx, "ecx");
        names.put(edx, "edx");
        names.put(esp, "esp");
        names.put(ebp, "ebp");
        names.put(esi, "esi");
        names.put(edi, "edi");
        return names;
    }
}
