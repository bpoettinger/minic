import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import static org.junit.Assert.*;

public class Helpers {

    static boolean compileFile(String basePath) {
        basePath = "src/test/resources/" + basePath;
        final String sourcePath = basePath + ".java";
        try {
            final File dest = File.createTempFile("minic-test-", "");
            dest.deleteOnExit();
            Main.compile(sourcePath, dest.getAbsolutePath());
        } catch (Error e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            throw new RuntimeException("IOException", e);
        }

        return true;
    }

    static void compileFileFails(String basePath) {
        assertFalse(compileFile(basePath));
    }

    static void compileAndExecuteFail(String basePath) {
        basePath = "src/test/resources/" + basePath;
        final String sourcePath = basePath + ".java";
        try {
            final File dest = File.createTempFile("minic-test-", "");
            dest.deleteOnExit();
            Main.compile(sourcePath, dest.getAbsolutePath());
            final Process test = new ProcessBuilder(dest.getAbsolutePath()).start();
            test.waitFor();
            assertNotEquals(0, test.exitValue());
        } catch (IOException|InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    static void compileAndExecute(String basePath) {
        basePath = "src/test/resources/" + basePath;
        final String sourcePath = basePath + ".java";
        final String expectedPath = basePath + ".out";
        try {
            final String expected = String.join("\n", Files.readAllLines(Paths.get(expectedPath)));
            final File dest = File.createTempFile("minic-test-", "");
            dest.deleteOnExit();
            Main.compile(sourcePath, dest.getAbsolutePath());
            final Process test = new ProcessBuilder(dest.getAbsolutePath()).start();
            final String output = new BufferedReader(new InputStreamReader(test.getInputStream())).lines().collect(Collectors.joining("\n"));
            //System.out.println("Output: " + output);
            assertTrue(output.equals(expected));
        } catch (IOException|NumberFormatException e) {
            throw new RuntimeException(e);
        }
    }
}
