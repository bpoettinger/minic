package ir;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class StmSeq extends Stm {

  private final List<Stm> stms;

  public StmSeq(List<Stm> stms) {
    if (stms == null) {
      throw new NullPointerException();
    }
    this.stms = stms;
  }

  public StmSeq(Stm... stms) {
    this(Arrays.asList(stms));
  }

  public List<Stm> getStms() {
    return stms;
  }

  @Override
  public <A> A accept(StmVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append("SEQ(");
    Iterator<Stm> it = getStms().iterator();
    while (it.hasNext()) {
      Stm s = it.next();
      buf.append(s);
      if (it.hasNext()) buf.append(", ");
    }
    buf.append(") ");
    return buf.toString();
  }
}
