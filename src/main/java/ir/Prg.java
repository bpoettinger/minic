package ir;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public final class Prg implements Iterable<Function> {

	private final List<Function> functions;
	private final List<VTable> vTables;

	public Prg(List<Function> functions, List<VTable> vTables) {
		this.functions = functions;
		this.vTables = vTables;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (VTable vTable : vTables) {
			s.append(vTable.toString());
			s.append('\n');
		}

		for (Function f : functions) {
			s.append(f);
			s.append("\n");
		}

		return s.toString();
	}

	@Override
	public Iterator<Function> iterator() {
		return functions.iterator();
	}

	public Stream<Function> stream() {
		return functions.stream();
	}

	public List<ir.VTable> getVTables() {
		return vTables;
	}

	public List<Function> getFunctions() {
		return functions;
	}
}
