package ir;

public class ExpParam extends Exp {
  private final int number;
  private final Type type;

  public ExpParam(int number, Type type) {
    this.number = number;
    this.type = type;
  }

  public int getNumber() {
    return number;
  }

  @Override
  public Type getType() {
    return type;
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "PARAM(" + getNumber() + ")";
  }


}
