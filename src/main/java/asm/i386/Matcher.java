/*
=====================================
GENERATED CODE, CHANGES WILL BE LOST!
===================================== 
*/

package asm.i386;
        
import ir.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import asm.i386.InstructionEmitter;
import asm.i386.I386Instruction;

public class Matcher {
    private final InstructionEmitter emitter;
    public Matcher(InstructionEmitter emitter) { this.emitter = emitter; }
    private void emit(I386Instruction instruction) { emitter.emit(instruction); }
    public Operand match(Exp exp) {
        Exp t1 = exp;
        if (t1 instanceof ExpMem) {
            ExpMem t2 = (ExpMem) t1;
            Exp t3 = t2.getAddress();
            if (t3 instanceof ExpTemp) {
                ExpTemp base = (ExpTemp) t3;
                return new Operand.Mem(base.getTemp());
            }
            else if (t3 instanceof ExpBinOp && T5.contains(((ExpBinOp)t3).getOp())) {
                ExpBinOp t4 = (ExpBinOp) t3;
                Exp t6 = t4.getLeft();
                if (t6 instanceof ExpTemp) {
                    ExpTemp base = (ExpTemp) t6;
                    Exp t7 = t4.getRight();
                    if (t7 instanceof ExpBinOp && T9.contains(((ExpBinOp)t7).getOp())) {
                        ExpBinOp t8 = (ExpBinOp) t7;
                        Exp t10 = t8.getLeft();
                        if (t10 instanceof ExpConst && T11.contains(((ExpConst)t10).getValue())) {
                            ExpConst scale = (ExpConst) t10;
                            Exp t12 = t8.getRight();
                            if (t12 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t12;
                                return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), 0);
                            }
                        }
                        else if (t10 instanceof ExpTemp) {
                            ExpTemp index = (ExpTemp) t10;
                            Exp t13 = t8.getRight();
                            if (t13 instanceof ExpConst && T11.contains(((ExpConst)t13).getValue())) {
                                ExpConst scale = (ExpConst) t13;
                                return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), 0);
                            }
                        }
                    }
                    else if (t7 instanceof ExpConst) {
                        ExpConst offset = (ExpConst) t7;
                        return new Operand.Mem(base.getTemp(), null, null, offset.getValue());
                    }
                    else if (t7 instanceof ExpBinOp && T5.contains(((ExpBinOp)t7).getOp())) {
                        ExpBinOp t14 = (ExpBinOp) t7;
                        Exp t15 = t14.getLeft();
                        if (t15 instanceof ExpBinOp && T9.contains(((ExpBinOp)t15).getOp())) {
                            ExpBinOp t16 = (ExpBinOp) t15;
                            Exp t17 = t16.getLeft();
                            if (t17 instanceof ExpConst && T11.contains(((ExpConst)t17).getValue())) {
                                ExpConst scale = (ExpConst) t17;
                                Exp t18 = t16.getRight();
                                if (t18 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t18;
                                    Exp t19 = t14.getRight();
                                    if (t19 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t19;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                            else if (t17 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t17;
                                Exp t20 = t16.getRight();
                                if (t20 instanceof ExpConst && T11.contains(((ExpConst)t20).getValue())) {
                                    ExpConst scale = (ExpConst) t20;
                                    Exp t21 = t14.getRight();
                                    if (t21 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t21;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                        else if (t15 instanceof ExpConst) {
                            ExpConst offset = (ExpConst) t15;
                            Exp t22 = t14.getRight();
                            if (t22 instanceof ExpBinOp && T9.contains(((ExpBinOp)t22).getOp())) {
                                ExpBinOp t23 = (ExpBinOp) t22;
                                Exp t24 = t23.getLeft();
                                if (t24 instanceof ExpConst && T11.contains(((ExpConst)t24).getValue())) {
                                    ExpConst scale = (ExpConst) t24;
                                    Exp t25 = t23.getRight();
                                    if (t25 instanceof ExpTemp) {
                                        ExpTemp index = (ExpTemp) t25;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t24 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t24;
                                    Exp t26 = t23.getRight();
                                    if (t26 instanceof ExpConst && T11.contains(((ExpConst)t26).getValue())) {
                                        ExpConst scale = (ExpConst) t26;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                    }
                    Operand wc = match(t7);
                    if (!(wc instanceof Operand.Reg)) {
                        Operand.Reg temp = new Operand.Reg(new Temp(), false);
                        emit(new InstrBinary(InstrBinary.Kind.MOV, temp, wc));
                        wc = temp;
                    }
                    return new Operand.Mem(base.getTemp(), 1, ((Operand.Reg)wc).reg, 0);
                }
                else if (t6 instanceof ExpBinOp && T9.contains(((ExpBinOp)t6).getOp())) {
                    ExpBinOp t27 = (ExpBinOp) t6;
                    Exp t28 = t27.getLeft();
                    if (t28 instanceof ExpConst && T11.contains(((ExpConst)t28).getValue())) {
                        ExpConst scale = (ExpConst) t28;
                        Exp t29 = t27.getRight();
                        if (t29 instanceof ExpTemp) {
                            ExpTemp index = (ExpTemp) t29;
                            Exp t30 = t4.getRight();
                            if (t30 instanceof ExpTemp) {
                                ExpTemp base = (ExpTemp) t30;
                                return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), 0);
                            }
                            else if (t30 instanceof ExpBinOp && T5.contains(((ExpBinOp)t30).getOp())) {
                                ExpBinOp t31 = (ExpBinOp) t30;
                                Exp t32 = t31.getLeft();
                                if (t32 instanceof ExpTemp) {
                                    ExpTemp base = (ExpTemp) t32;
                                    Exp t33 = t31.getRight();
                                    if (t33 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t33;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t32 instanceof ExpConst) {
                                    ExpConst offset = (ExpConst) t32;
                                    Exp t34 = t31.getRight();
                                    if (t34 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t34;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                    }
                    else if (t28 instanceof ExpTemp) {
                        ExpTemp index = (ExpTemp) t28;
                        Exp t35 = t27.getRight();
                        if (t35 instanceof ExpConst && T11.contains(((ExpConst)t35).getValue())) {
                            ExpConst scale = (ExpConst) t35;
                            Exp t36 = t4.getRight();
                            if (t36 instanceof ExpTemp) {
                                ExpTemp base = (ExpTemp) t36;
                                return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), 0);
                            }
                            else if (t36 instanceof ExpBinOp && T5.contains(((ExpBinOp)t36).getOp())) {
                                ExpBinOp t37 = (ExpBinOp) t36;
                                Exp t38 = t37.getLeft();
                                if (t38 instanceof ExpTemp) {
                                    ExpTemp base = (ExpTemp) t38;
                                    Exp t39 = t37.getRight();
                                    if (t39 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t39;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t38 instanceof ExpConst) {
                                    ExpConst offset = (ExpConst) t38;
                                    Exp t40 = t37.getRight();
                                    if (t40 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t40;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                    }
                }
                else if (t6 instanceof ExpConst) {
                    ExpConst offset = (ExpConst) t6;
                    Exp t41 = t4.getRight();
                    if (t41 instanceof ExpTemp) {
                        ExpTemp base = (ExpTemp) t41;
                        return new Operand.Mem(base.getTemp(), null, null, offset.getValue());
                    }
                    else if (t41 instanceof ExpBinOp && T5.contains(((ExpBinOp)t41).getOp())) {
                        ExpBinOp t42 = (ExpBinOp) t41;
                        Exp t43 = t42.getLeft();
                        if (t43 instanceof ExpTemp) {
                            ExpTemp base = (ExpTemp) t43;
                            Exp t44 = t42.getRight();
                            if (t44 instanceof ExpBinOp && T9.contains(((ExpBinOp)t44).getOp())) {
                                ExpBinOp t45 = (ExpBinOp) t44;
                                Exp t46 = t45.getLeft();
                                if (t46 instanceof ExpConst && T11.contains(((ExpConst)t46).getValue())) {
                                    ExpConst scale = (ExpConst) t46;
                                    Exp t47 = t45.getRight();
                                    if (t47 instanceof ExpTemp) {
                                        ExpTemp index = (ExpTemp) t47;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t46 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t46;
                                    Exp t48 = t45.getRight();
                                    if (t48 instanceof ExpConst && T11.contains(((ExpConst)t48).getValue())) {
                                        ExpConst scale = (ExpConst) t48;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                            else if (t44 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t44;
                                return new Operand.Mem(base.getTemp(), 1, index.getTemp(), offset.getValue());
                            }
                        }
                        else if (t43 instanceof ExpBinOp && T9.contains(((ExpBinOp)t43).getOp())) {
                            ExpBinOp t49 = (ExpBinOp) t43;
                            Exp t50 = t49.getLeft();
                            if (t50 instanceof ExpConst && T11.contains(((ExpConst)t50).getValue())) {
                                ExpConst scale = (ExpConst) t50;
                                Exp t51 = t49.getRight();
                                if (t51 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t51;
                                    Exp t52 = t42.getRight();
                                    if (t52 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t52;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                            else if (t50 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t50;
                                Exp t53 = t49.getRight();
                                if (t53 instanceof ExpConst && T11.contains(((ExpConst)t53).getValue())) {
                                    ExpConst scale = (ExpConst) t53;
                                    Exp t54 = t42.getRight();
                                    if (t54 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t54;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                    }
                    Operand wc = match(t41);
                    if (!(wc instanceof Operand.Reg)) {
                        Operand.Reg temp = new Operand.Reg(new Temp(), false);
                        emit(new InstrBinary(InstrBinary.Kind.MOV, temp, wc));
                        wc = temp;
                    }
                    return new Operand.Mem(((Operand.Reg)wc).reg, null, null, offset.getValue());
                }
                else if (t6 instanceof ExpBinOp && T5.contains(((ExpBinOp)t6).getOp())) {
                    ExpBinOp t55 = (ExpBinOp) t6;
                    Exp t56 = t55.getLeft();
                    if (t56 instanceof ExpTemp) {
                        ExpTemp base = (ExpTemp) t56;
                        Exp t57 = t55.getRight();
                        if (t57 instanceof ExpConst) {
                            ExpConst offset = (ExpConst) t57;
                            Exp t58 = t4.getRight();
                            if (t58 instanceof ExpBinOp && T9.contains(((ExpBinOp)t58).getOp())) {
                                ExpBinOp t59 = (ExpBinOp) t58;
                                Exp t60 = t59.getLeft();
                                if (t60 instanceof ExpConst && T11.contains(((ExpConst)t60).getValue())) {
                                    ExpConst scale = (ExpConst) t60;
                                    Exp t61 = t59.getRight();
                                    if (t61 instanceof ExpTemp) {
                                        ExpTemp index = (ExpTemp) t61;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t60 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t60;
                                    Exp t62 = t59.getRight();
                                    if (t62 instanceof ExpConst && T11.contains(((ExpConst)t62).getValue())) {
                                        ExpConst scale = (ExpConst) t62;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                            else if (t58 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t58;
                                return new Operand.Mem(base.getTemp(), 1, index.getTemp(), offset.getValue());
                            }
                        }
                        else if (t57 instanceof ExpBinOp && T9.contains(((ExpBinOp)t57).getOp())) {
                            ExpBinOp t63 = (ExpBinOp) t57;
                            Exp t64 = t63.getLeft();
                            if (t64 instanceof ExpConst && T11.contains(((ExpConst)t64).getValue())) {
                                ExpConst scale = (ExpConst) t64;
                                Exp t65 = t63.getRight();
                                if (t65 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t65;
                                    Exp t66 = t4.getRight();
                                    if (t66 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t66;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                            else if (t64 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t64;
                                Exp t67 = t63.getRight();
                                if (t67 instanceof ExpConst && T11.contains(((ExpConst)t67).getValue())) {
                                    ExpConst scale = (ExpConst) t67;
                                    Exp t68 = t4.getRight();
                                    if (t68 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t68;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                        else if (t57 instanceof ExpTemp) {
                            ExpTemp index = (ExpTemp) t57;
                            Exp t69 = t4.getRight();
                            if (t69 instanceof ExpConst) {
                                ExpConst offset = (ExpConst) t69;
                                return new Operand.Mem(base.getTemp(), 1, index.getTemp(), offset.getValue());
                            }
                        }
                    }
                    else if (t56 instanceof ExpConst) {
                        ExpConst offset = (ExpConst) t56;
                        Exp t70 = t55.getRight();
                        if (t70 instanceof ExpTemp) {
                            ExpTemp base = (ExpTemp) t70;
                            Exp t71 = t4.getRight();
                            if (t71 instanceof ExpBinOp && T9.contains(((ExpBinOp)t71).getOp())) {
                                ExpBinOp t72 = (ExpBinOp) t71;
                                Exp t73 = t72.getLeft();
                                if (t73 instanceof ExpConst && T11.contains(((ExpConst)t73).getValue())) {
                                    ExpConst scale = (ExpConst) t73;
                                    Exp t74 = t72.getRight();
                                    if (t74 instanceof ExpTemp) {
                                        ExpTemp index = (ExpTemp) t74;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t73 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t73;
                                    Exp t75 = t72.getRight();
                                    if (t75 instanceof ExpConst && T11.contains(((ExpConst)t75).getValue())) {
                                        ExpConst scale = (ExpConst) t75;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                            else if (t71 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t71;
                                return new Operand.Mem(base.getTemp(), 1, index.getTemp(), offset.getValue());
                            }
                        }
                        else if (t70 instanceof ExpBinOp && T9.contains(((ExpBinOp)t70).getOp())) {
                            ExpBinOp t76 = (ExpBinOp) t70;
                            Exp t77 = t76.getLeft();
                            if (t77 instanceof ExpConst && T11.contains(((ExpConst)t77).getValue())) {
                                ExpConst scale = (ExpConst) t77;
                                Exp t78 = t76.getRight();
                                if (t78 instanceof ExpTemp) {
                                    ExpTemp index = (ExpTemp) t78;
                                    Exp t79 = t4.getRight();
                                    if (t79 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t79;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                            else if (t77 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t77;
                                Exp t80 = t76.getRight();
                                if (t80 instanceof ExpConst && T11.contains(((ExpConst)t80).getValue())) {
                                    ExpConst scale = (ExpConst) t80;
                                    Exp t81 = t4.getRight();
                                    if (t81 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t81;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                    }
                    else if (t56 instanceof ExpBinOp && T9.contains(((ExpBinOp)t56).getOp())) {
                        ExpBinOp t82 = (ExpBinOp) t56;
                        Exp t83 = t82.getLeft();
                        if (t83 instanceof ExpConst && T11.contains(((ExpConst)t83).getValue())) {
                            ExpConst scale = (ExpConst) t83;
                            Exp t84 = t82.getRight();
                            if (t84 instanceof ExpTemp) {
                                ExpTemp index = (ExpTemp) t84;
                                Exp t85 = t55.getRight();
                                if (t85 instanceof ExpTemp) {
                                    ExpTemp base = (ExpTemp) t85;
                                    Exp t86 = t4.getRight();
                                    if (t86 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t86;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t85 instanceof ExpConst) {
                                    ExpConst offset = (ExpConst) t85;
                                    Exp t87 = t4.getRight();
                                    if (t87 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t87;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                        else if (t83 instanceof ExpTemp) {
                            ExpTemp index = (ExpTemp) t83;
                            Exp t88 = t82.getRight();
                            if (t88 instanceof ExpConst && T11.contains(((ExpConst)t88).getValue())) {
                                ExpConst scale = (ExpConst) t88;
                                Exp t89 = t55.getRight();
                                if (t89 instanceof ExpTemp) {
                                    ExpTemp base = (ExpTemp) t89;
                                    Exp t90 = t4.getRight();
                                    if (t90 instanceof ExpConst) {
                                        ExpConst offset = (ExpConst) t90;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                                else if (t89 instanceof ExpConst) {
                                    ExpConst offset = (ExpConst) t89;
                                    Exp t91 = t4.getRight();
                                    if (t91 instanceof ExpTemp) {
                                        ExpTemp base = (ExpTemp) t91;
                                        return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue());
                                    }
                                }
                            }
                        }
                    }
                }
                else if (t6 instanceof ExpTemp) {
                    ExpTemp index = (ExpTemp) t6;
                    Exp t92 = t4.getRight();
                    if (t92 instanceof ExpBinOp && T5.contains(((ExpBinOp)t92).getOp())) {
                        ExpBinOp t93 = (ExpBinOp) t92;
                        Exp t94 = t93.getLeft();
                        if (t94 instanceof ExpTemp) {
                            ExpTemp base = (ExpTemp) t94;
                            Exp t95 = t93.getRight();
                            if (t95 instanceof ExpConst) {
                                ExpConst offset = (ExpConst) t95;
                                return new Operand.Mem(base.getTemp(), 1, index.getTemp(), offset.getValue());
                            }
                        }
                        else if (t94 instanceof ExpConst) {
                            ExpConst offset = (ExpConst) t94;
                            Exp t96 = t93.getRight();
                            if (t96 instanceof ExpTemp) {
                                ExpTemp base = (ExpTemp) t96;
                                return new Operand.Mem(base.getTemp(), 1, index.getTemp(), offset.getValue());
                            }
                        }
                    }
                }
                Exp t97 = t4.getRight();
                if (t97 instanceof ExpTemp) {
                    ExpTemp base = (ExpTemp) t97;
                    Operand wc = match(t6);
                    if (!(wc instanceof Operand.Reg)) {
                        Operand.Reg temp = new Operand.Reg(new Temp(), false);
                        emit(new InstrBinary(InstrBinary.Kind.MOV, temp, wc));
                        wc = temp;
                    }
                    return new Operand.Mem(base.getTemp(), 1, ((Operand.Reg)wc).reg, 0);
                }
                else if (t97 instanceof ExpConst) {
                    ExpConst offset = (ExpConst) t97;
                    Operand wc = match(t6);
                    if (!(wc instanceof Operand.Reg)) {
                        Operand.Reg temp = new Operand.Reg(new Temp(), false);
                        emit(new InstrBinary(InstrBinary.Kind.MOV, temp, wc));
                        wc = temp;
                    }
                    return new Operand.Mem(((Operand.Reg)wc).reg, null, null, offset.getValue());
                }
            }
            Operand wc = match(t3);
            if (!(wc instanceof Operand.Reg)) {
                Operand.Reg temp = new Operand.Reg(new Temp(), false);
                emit(new InstrBinary(InstrBinary.Kind.MOV, temp, wc));
                wc = temp;
            }
            return new Operand.Mem(((Operand.Reg)wc).reg);
        }
        else if (t1 instanceof ExpParam) {
            ExpParam param = (ExpParam) t1;
            return new Operand.Mem(Registers.ebp.reg, null, null, 8+4*param.getNumber());
        }
        else if (t1 instanceof ExpBinOp && T98.contains(((ExpBinOp)t1).getOp())) {
            ExpBinOp op = (ExpBinOp) t1;
            Exp t99 = op.getLeft();
            Exp t100 = op.getRight();
            Operand w1 = match(t99);
            if (!(w1 instanceof Operand.Reg)) {
                Operand.Reg temp = new Operand.Reg(new Temp(), false);
                emit(new InstrBinary(InstrBinary.Kind.MOV, temp, w1));
                w1 = temp;
            }
            Operand w2 = match(t100);
            if (!((Operand.Reg)w1).isReusable) { Operand.Reg t = new Operand.Reg(new Temp(), false); emit(new InstrBinary(InstrBinary.Kind.MOV, t, w1)); emit(new InstrBinary(opMap.get(op.getOp()), t, w2)); return t; } else { emit(new InstrBinary(opMap.get(op.getOp()), w1, w2)); return w1; }
        }
        else if (t1 instanceof ExpBinOp && T101.contains(((ExpBinOp)t1).getOp())) {
            ExpBinOp op = (ExpBinOp) t1;
            Exp t102 = op.getLeft();
            Exp t103 = op.getRight();
            Operand w1 = match(t102);
            if (!(w1 instanceof Operand.Reg)) {
                Operand.Reg temp = new Operand.Reg(new Temp(), false);
                emit(new InstrBinary(InstrBinary.Kind.MOV, temp, w1));
                w1 = temp;
            }
            Operand w2 = match(t103);
            Operand.Reg t = new Operand.Reg(new Temp(), false);
                    emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.eax, w1));
                    emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.edx, Registers.eax));
                    emit(new InstrBinary(InstrBinary.Kind.SAR, Registers.edx, new Operand.Imm(31)));
                    emit(new InstrUnary(InstrUnary.Kind.IDIV, w2));
                    emit(new InstrBinary(InstrBinary.Kind.MOV, t, Registers.eax));
                    return t;
        }
        else if (t1 instanceof ExpConst) {
            ExpConst c = (ExpConst) t1;
            return new Operand.Imm(c.getValue());
        }
        else if (t1 instanceof ExpTemp) {
            ExpTemp t = (ExpTemp) t1;
            return new Operand.Reg(t.getTemp());
        }
        else if (t1 instanceof ExpName) {
            ExpName n = (ExpName) t1;
            Operand.Reg t = new Operand.Reg(new Temp(), false); emit(new InstrBinary(InstrBinary.Kind.LEA, t, new Operand.Name(n.getLabel()))); return t;
        }
        else if (t1 instanceof ExpCall) {
            ExpCall call = (ExpCall) t1;
            ExpressionVisitor v = new ExpressionVisitor(emitter); return call.accept(v);
        }
        throw new RuntimeException();
    }
    private static Map<ExpBinOp.Op, InstrBinary.Kind> createOpMap() {
        final Map<ExpBinOp.Op,InstrBinary.Kind> opMap = new HashMap<>();
        opMap.put(ExpBinOp.Op.PLUS, InstrBinary.Kind.ADD);
        opMap.put(ExpBinOp.Op.MINUS, InstrBinary.Kind.SUB);
        opMap.put(ExpBinOp.Op.MUL, InstrBinary.Kind.IMUL);
        opMap.put(ExpBinOp.Op.DIV, InstrBinary.Kind.IMUL);
        return opMap;
    }
    private static final Map<ExpBinOp.Op, InstrBinary.Kind> opMap = createOpMap();
    private static final List<ExpBinOp.Op> T5 = Arrays.asList(ExpBinOp.Op.PLUS);
    private static final List<ExpBinOp.Op> T9 = Arrays.asList(ExpBinOp.Op.MUL);
    private static final List<Integer> T11 = Arrays.asList(1, 2, 4, 8);
    private static final List<ExpBinOp.Op> T98 = Arrays.asList(ExpBinOp.Op.PLUS, ExpBinOp.Op.MINUS, ExpBinOp.Op.MUL);
    private static final List<ExpBinOp.Op> T101 = Arrays.asList(ExpBinOp.Op.DIV);
}
