package ir;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class VTable {

	private final String className;
	private final Map<Integer, ExpName> labels = new TreeMap<>();

	public VTable(String className) {
		this.className = className;
	}

	public void add(int index, ExpName label) {
		assert(!labels.containsKey(index));
		labels.put(index, label);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("int32_t LvTable_" + getClassName() + " [] = {");
		sb.append("0");
		labels.values().stream().forEach(label -> sb.append(",\n(int32_t) " + label.toString()));
		sb.append("\n};");

		return sb.toString();
	}

	public String getClassName() {
		return className;
	}

	public Collection<ExpName> getValues() {
		return labels.values();
	}
}
