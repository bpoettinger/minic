package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents {@code this}.
 */
public class ExpThis extends Exp {

  public ExpThis(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
  }

  @Override
  public <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T {
    return v.visit(this);
  }
}
