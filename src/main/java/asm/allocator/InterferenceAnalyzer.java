package asm.allocator;
import asm.MachineInstruction;
import ir.Temp;
import util.Pair;

import java.util.*;


public class InterferenceAnalyzer {

    private final Map<MachineInstruction, ActivitySet> livenessTable;
    private InterferenceGraph interference;
    private final Set<Temp> registers;
    private final Set<Temp> allRegisters;

    public InterferenceAnalyzer(Map<MachineInstruction, ActivitySet> livenessTable, Set<Temp> registers, Set<Temp> allRegisters) {
        this.livenessTable = livenessTable;
        this.registers = registers;
        this.allRegisters = allRegisters;
    }

    public void buildInterferenceGraph() {
        createNodes();
        createEdges();
    }

    private void createNodes() {
        final Set<Temp> temps = new HashSet<>(1024);
        temps.addAll(registers);

        for (MachineInstruction instruction : livenessTable.keySet()) {
            temps.addAll(instruction.use());
            temps.addAll(instruction.def());
        }

        this.interference = new InterferenceGraph(temps.size());

        for (Temp temp : temps) {
            final Temp register = allRegisters.contains(temp) ? temp : null;
            interference.addNode(temp, register);
        }
    }

    private void createEdges() {
        for (Temp r1 : registers) {
            for (Temp r2 : registers) {
                interference.addEdge(r1, r2);
            }
        }

        for (Map.Entry<MachineInstruction, ActivitySet> entry : livenessTable.entrySet()) {
            final MachineInstruction instruction = entry.getKey();
            final Set<Temp> out = entry.getValue().out;

            final Pair<Temp, Temp> moveBetweenTemps = instruction.isMoveBetweenTemps();
            if (moveBetweenTemps == null) {
                for (Temp temp1 : instruction.def()) {
                    for (Temp temp2 : out) {
                        interference.addEdge(temp1, temp2);
                    }
                }
            } else {
                final Temp temp1 = moveBetweenTemps.fst;
                final Temp temp2_ = moveBetweenTemps.snd;

                final InterferenceGraph.Node node1 = interference.getNode(temp1);
                final InterferenceGraph.Node node2_ = interference.getNode(temp2_);

                node1.setMoveRelated(true);
                node2_.setMoveRelated(true);

                for (Temp temp2 : out) {
                    if (temp2 != temp2_) {
                        interference.addEdge(temp1, temp2);
                    }
                }
            }
        }
    }

    public InterferenceGraph getInterferenceGraph() {
        return interference;
    }
}
