package ir.block;

import ir.Stm;
import ir.StmCJump;
import ir.StmJump;
import ir.StmLabel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Block {

    private List<Stm> statements = new ArrayList<>();

    public void addStatement(ir.Stm stm) {
        statements.add(stm);
    }

    public List<Stm> getStatements() {
        return Collections.unmodifiableList(statements);
    }

    public boolean isWell() {
        final int numStatements = statements.size();
        return numStatements >= 2
                && statements.get(0) instanceof StmLabel
                && (statements.get(numStatements - 1) instanceof StmJump || statements.get(numStatements - 1) instanceof StmCJump);
    }

    public ir.Label getLabel() {
        final Stm label = statements.get(0);
        return ((StmLabel) label).getLabel();
    }

    public ir.Stm getJump() {
        return statements.get(statements.size() - 1);
    }

    public boolean endsWithJump() {
        return statements.get(statements.size() - 1) instanceof StmJump;
    }

    public boolean endsWithCJump() {
        return statements.get(statements.size() - 1) instanceof StmCJump;
    }

    public void redirect(ir.Stm newJump) {
        statements.remove(statements.size() - 1);
        statements.add(newJump);
    }

    public void negate() {
        final ir.StmCJump jump = (ir.StmCJump) statements.remove(statements.size() - 1);
        statements.add(new ir.StmCJump(jump.getRel().neg(), jump.getLeft(), jump.getRight(), jump.getLabelFalse(), jump.getLabelTrue()));
    }
}
