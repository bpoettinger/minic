import org.apache.commons.cli.*;
import org.apache.commons.cli.ParseException;

/**
 * Created by bernhard on 31.10.17.
 */
class ArgParser {

	private CommandLineParser parser;
	private HelpFormatter formatter;
	private Options options;

	public ArgParser() {
		Option compile = Option.builder("c")
				.longOpt("compile")
				.desc("Compile the input file to output path")
				.hasArg(false)
				.build();
		Option prettify = Option.builder("p")
				.longOpt("prettify")
				.desc("Prettify the input file to stdout")
				.hasArg(false)
				.build();
		Option irTree = Option.builder("i")
				.longOpt("irtree")
				.desc("Print IR tree to stdout")
				.hasArg(false)
				.build();
		Option irCode = Option.builder("j")
				.longOpt("ircode")
				.desc("Print IR code to stdout")
				.hasArg(false)
				.build();
		Option irCodeNormalized = Option.builder("k")
				.longOpt("normircode")
				.desc("Print normalized IR code to stdout")
				.hasArg(false)
				.build();
		Option asmCode = Option.builder("l")
				.longOpt("asmcode")
				.desc("Write asm code to output path")
				.hasArg(false)
				.build();
		Option help = Option.builder("h")
				.longOpt("help")
				.desc("Print this help message")
				.hasArg(false)
				.build();
		OptionGroup commands = new OptionGroup();
		commands.setRequired(false);
		commands.addOption(compile);
		commands.addOption(prettify);
		commands.addOption(irTree);
		commands.addOption(irCode);
		commands.addOption(irCodeNormalized);
		commands.addOption(asmCode);
		commands.addOption(help);

		options = new Options();
		options.addOptionGroup(commands);
		options.addOption("o", true, "Output file. Defaults to current directory + input filename + '.out/.s'.");

		parser = new DefaultParser();
		formatter = new HelpFormatter();
	}

	public CommandLine parse(String[] args) throws ParseException {
		CommandLine command = parser.parse(options, args);
		if ((!command.hasOption("h") && command.getArgs().length != 1)
				|| (command.hasOption("h") && command.getArgs().length != 0)) {
			throw new ParseException("Invalid positional arguments");
		}
		return command;
	}

	public void printHelp() {
		formatter.printHelp("minic [-options] <input-file>", options);
	}
}
