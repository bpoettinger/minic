package asm.allocator;

import java.util.*;

import asm.MachineFunction;
import asm.MachineInstruction;
import ir.Label;
import util.Pair;

public class ControlFlowAnalyzer {
	private final MachineFunction function;
	private final ControlFlowGraph<MachineInstruction> controlFlowGraph;
	private final Map<Label, MachineInstruction> labelInstructions;

	public ControlFlowAnalyzer(MachineFunction function) {
		this.function = function;
		final int instructionCount = function.getInstructionCount();
		this.controlFlowGraph = new ControlFlowGraph<>(instructionCount);
		this.labelInstructions = new HashMap<>(instructionCount);
	}

	public void build() {
		for (MachineInstruction instruction : function) {
			controlFlowGraph.addNode(instruction);

			Label label = instruction.isLabel();
			if (label != null) {
				labelInstructions.put(label, instruction);
			}
		}

		Iterator<MachineInstruction> it = function.iterator();
		if (it.hasNext()) {
			for (MachineInstruction curInstruction = it.next(), nextInstruction; it.hasNext(); curInstruction = nextInstruction) {
				nextInstruction = it.next();

				for (Label label : curInstruction.jumps()) {
					MachineInstruction labelInstruction = labelInstructions.get(label);
					if (labelInstruction != null) {
						// == null e.g. if instructions is a call to a runtime function
						controlFlowGraph.addSuccessor(curInstruction, labelInstruction);
					}
				}

				if (curInstruction.isFallThrough()) {
					controlFlowGraph.addSuccessor(curInstruction, nextInstruction);
				}
			}
		}
	}

	public ControlFlowGraph<MachineInstruction> getControlFlowGraph() {
		return controlFlowGraph;
	}
}
