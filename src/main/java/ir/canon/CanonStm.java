package ir.canon;

import java.util.LinkedList;
import java.util.List;

/**
 * Visitor class for canonizing a single statement.
 */
class CanonStm implements ir.StmVisitor<List<ir.Stm>> {

  private List<ir.Stm> canon(ir.Stm s) {
    return s.accept(this);
  }

  // canonize expression e, without removing top-level call
  private CanonizedExp canon(ir.Exp e) {
    return e.accept(new CanonExp());
  }

  // canonize expression e, removing top-level call
  private CanonizedExp canonNoTopCALL(ir.Exp e) {
    CanonExp ce = new CanonExp();
    return ce.canonNoTopCALL(e);
  }

  @Override
  public List<ir.Stm> visit(ir.StmMove stmMOVE) {
    if (stmMOVE.getDst() instanceof ir.ExpMem) {
      ir.Exp addr = ((ir.ExpMem) stmMOVE.getDst()).getAddress();
      return CanonizedExp.toStm(
              canonNoTopCALL(addr),
              canonNoTopCALL(stmMOVE.getSrc()),
              (eaddr, esrc) -> new ir.StmMove(new ir.ExpMem(eaddr, stmMOVE.getDst().getType()), esrc));
    } else if (stmMOVE.getDst() instanceof ir.ExpTemp) {
      return canon(stmMOVE.getSrc()).toStm(esrc -> new ir.StmMove(stmMOVE.getDst(), esrc));
    } else if (stmMOVE.getDst() instanceof ir.ExpParam) {
      return canon(stmMOVE.getSrc()).toStm(esrc -> new ir.StmMove(stmMOVE.getDst(), esrc));
    } else if (stmMOVE.getDst() instanceof ir.ExpESeq) {
      ir.ExpESeq dst = (ir.ExpESeq) stmMOVE.getDst();
      return new ir.StmSeq(dst.getStm(), new ir.StmMove(dst.getExp(), stmMOVE.getSrc())).accept(this);
    } else {
      throw new Error("Left-hand side of MOVE must be TEMP, PARAM, MEM or ESEQ.");
    }
  }

  @Override
  public List<ir.Stm> visit(ir.StmJump stmJUMP) {
    return canonNoTopCALL(stmJUMP.getDst()).toStm(e -> new ir.StmJump(e, stmJUMP.getPossibleTargets()));
  }

  @Override
  public List<ir.Stm> visit(ir.StmCJump stmCJUMP) {
    return CanonizedExp.toStm(
            canonNoTopCALL(stmCJUMP.getLeft()),
            canonNoTopCALL(stmCJUMP.getRight()),
            (l, r) -> new ir.StmCJump(stmCJUMP.getRel(), l, r, stmCJUMP.getLabelTrue(), stmCJUMP.getLabelFalse())
    );
  }

  @Override
  public List<ir.Stm> visit(ir.StmSeq stmSEQ) {
    List<ir.Stm> cstms = new LinkedList<>();
    for (ir.Stm s: stmSEQ.getStms()) {
      cstms.addAll(canon(s));
    }
    return cstms;
  }

  @Override
  public List<ir.Stm> visit(ir.StmLabel stmLABEL) {
    List<ir.Stm> stms = new LinkedList<>();
    stms.add(stmLABEL);
    return stms;
  }
}
