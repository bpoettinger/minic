package asm.i386;

import ir.Label;
import ir.Temp;
import util.Pair;

import java.util.*;
import java.util.function.Function;

final class InstrUnary implements I386Instruction {

    enum Kind {

        PUSH, POP, NEG, NOT, INC, DEC, IDIV
    }

    private Operand op;
    private final Kind kind;

    InstrUnary(Kind kind, Operand op) {
        assert (!(kind == Kind.POP || kind == Kind.NEG || kind == Kind.NOT
                || kind == Kind.INC || kind == Kind.DEC || kind == Kind.IDIV) || !(op instanceof Operand.Imm));
        this.op = op;
        this.kind = kind;
    }

    @Override
    public Collection<Temp> use() {
        switch (kind) {
            case PUSH:
            case NEG:
            case NOT:
            case INC:
            case DEC:
                return op.use();
            case IDIV: {
                final List<Temp> result = new ArrayList<>();
                result.addAll(op.use());
                result.add(Registers.eax.reg);
                result.add(Registers.edx.reg);
                return result;
            }
            case POP:
                return Collections.emptyList();
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public Collection<Temp> def() {
        switch (kind) {
            case POP:
            case NEG:
            case NOT:
            case INC:
            case DEC:
                return op.use();
            case IDIV:
                return Arrays.asList(Registers.eax.reg, Registers.edx.reg);
            case PUSH:
                return Collections.emptyList();
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public Collection<Label> jumps() {
        return Collections.emptyList();
    }

    @Override
    public boolean isFallThrough() {
        return true;
    }

    @Override
    public Pair<Temp, Temp> isMoveBetweenTemps() {
        return null;
    }

    @Override
    public Label isLabel() {
        return null;
    }

    @Override
    public String toString() {
        return "\t" + kind + " " + op + "\n";
    }

    @Override
    public void rename(Function<Temp, Temp> sigma) {
        op = op.rename(sigma);
    }

    @Override
    public List<Operand.Reg> getSpillableOperands() {
        return Collections.emptyList();
    }

    @Override
    public void replaceSpillableOperand(Operand original, Operand replacement) {

    }
}
