package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents a constant number {@code value}.
 */
public class ExpIntConst extends Exp {

  private final int value;

  public ExpIntConst(int value, ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
    this.value = value;
  }

  @Override
  public <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T{
    return v.visit(this);
  }

  public int getValue() {
    return value;
  }
}
