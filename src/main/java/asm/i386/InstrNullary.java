package asm.i386;

import ir.Label;
import ir.Temp;
import util.Pair;

import java.util.*;
import java.util.function.Function;

final class InstrNullary implements I386Instruction {

    enum Kind {
        RET, LEAVE, NOP
    }

    private final Kind kind;

    InstrNullary(Kind kind) {
        this.kind = kind;
    }

    @Override
    public Collection<Temp> use() {
        switch (kind) {
            case RET:
                return Arrays.asList(Registers.esi.reg, Registers.edi.reg, Registers.ebx.reg);
            default:
                return Collections.emptyList();
        }
    }

    @Override
    public Collection<Temp> def() {
        return Collections.emptyList();
    }

    @Override
    public Collection<Label> jumps() {
        return Collections.emptyList();
    }

    @Override
    public boolean isFallThrough() {
        switch (kind) {
            case RET:
                return false;
            case LEAVE:
            case NOP:
                return true;
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public Pair<Temp, Temp> isMoveBetweenTemps() {
        return null;
    }

    @Override
    public Label isLabel() {
        return null;
    }

    @Override
    public String toString() {
        return "\t" + kind + "\n";
    }

    @Override
    public void rename(Function<Temp, Temp> sigma) {

    }

    public Kind getKind() {
        return kind;
    }

    @Override
    public List<Operand.Reg> getSpillableOperands() {
        return Collections.emptyList();
    }

    @Override
    public void replaceSpillableOperand(Operand original, Operand replacement) {

    }
}
