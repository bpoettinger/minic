package typechecker;

import ast.ExpInvoke;
import ast.StmArrayAssign;

/**
 * Created by bernhard on 17.11.17.
 */
public interface ErrorHandler {

    void onTypeError(Type actual, Type expected, String message, ast.Exp e);
    void onTypeError(Type actual, Type expected, String message, ast.Type e);
    void onNameClash(ast.Member m, String message);
    void onNameClash(ast.ClassDeclaration c, String message);
    void onInvalidArgumentCount(int actual, int expected, String message, ExpInvoke e);
    void onUnknownEntity(String message, ast.Exp e);
    void onUnknownEntity(String message, ast.StmAssign s);
    void onNameClash(ast.StmVariableDeclaration s, String s1);
    void onNameClash(ast.Parameter parameter, String s);
    void onUnknownEntity(String s, ast.ClassDeclaration class_);
    void onUnknownEntity(String s, ast.Type type);
    void onUnknownEntity(String s, StmArrayAssign s1);
}
