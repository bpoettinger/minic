package ir;

public interface StmVisitor<A> {

  A visit(StmMove stmMOVE);

  A visit(StmJump stmJUMP);

  A visit(StmCJump stmCJUMP);

  A visit(StmSeq stmSEQ);

  A visit(StmLabel stmLABEL);
}
