package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents {@code new ClassName}.
 */
public class ExpNew extends Exp {

  private final String className;

  public ExpNew(String className, ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
    this.className = className;
  }

  @Override
  public <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T {
    return v.visit(this);
  }

  public String getClassName() {
    return className;
  }
}
