package asm.i386;

import asm.MachineInstruction;
import ir.*;


public class StatementVisitor implements StmVisitor<Void> {
    private final InstructionEmitter emitter;

    public StatementVisitor(InstructionEmitter emitter) {
        this.emitter = emitter;
    }

    private void emit(MachineInstruction instruction) {
        emitter.emit(instruction);
    }

    @Override
    public Void visit(StmMove stmMOVE) {
        final Matcher matcher = new Matcher(emitter);
        Operand src = matcher.match(stmMOVE.getSrc());
        Operand dst = matcher.match(stmMOVE.getDst());

        if (dst instanceof Operand.Mem && src instanceof Operand.Mem) {
            Operand newSrc = new Operand.Reg(new Temp());
            emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, newSrc, src));
            src = newSrc;
        }

        emit(new InstrBinary(InstrBinary.Kind.MOV, dst, src));
        return null;
    }

    @Override
    public Void visit(StmJump stmJUMP) {
        emit(new InstrJump(InstrJump.Kind.JMP, ((ExpName) stmJUMP.getDst()).getLabel()));
        return null;
    }

    @Override
    public Void visit(StmCJump stmCJUMP) {
        InstrJump.Cond cond = null;
        InstrJump.Cond condInv = null;

        switch (stmCJUMP.getRel()) {
            case EQ:
                cond = InstrJump.Cond.E;
                condInv = InstrJump.Cond.E;
                break;
            case NE:
                cond = InstrJump.Cond.NE;
                condInv = InstrJump.Cond.NE;
                break;
            case LT:
                cond = InstrJump.Cond.L;
                condInv = InstrJump.Cond.G;
                break;
            case GT:
                cond = InstrJump.Cond.G;
                condInv = InstrJump.Cond.L;
                break;
            case LE:
                cond = InstrJump.Cond.LE;
                condInv = InstrJump.Cond.GE;
                break;
            case GE:
                cond = InstrJump.Cond.GE;
                condInv = InstrJump.Cond.LE;
                break;
            case ULT:
            case ULE:
            case UGT:
            case UGE:
                assert (false);
                break;
        }

        final Matcher matcher = new Matcher(emitter);
        Operand dst = matcher.match(stmCJUMP.getLeft());
        Operand src = matcher.match(stmCJUMP.getRight());

        if (dst instanceof Operand.Imm && src instanceof Operand.Imm) {
            final int lhs = ((Operand.Imm) dst).imm;
            final int rhs = ((Operand.Imm) src).imm;

            boolean result = false;
            switch (stmCJUMP.getRel()) {
                case EQ:
                    result = lhs == rhs;
                    break;
                case NE:
                    result = lhs != rhs;
                    break;
                case LT:
                    result = lhs < rhs;
                    break;
                case GT:
                    result = lhs > rhs;
                    break;
                case LE:
                    result = lhs <= rhs;
                    break;
                case GE:
                    result = lhs >= rhs;
                    break;
            }

            if (result) {
                emit(new InstrJump(InstrJump.Kind.JMP, stmCJUMP.getLabelTrue()));
            } else {
                emit(new InstrJump(InstrJump.Kind.JMP, stmCJUMP.getLabelFalse()));
            }
        } else {
            if (dst instanceof Operand.Mem && src instanceof Operand.Imm) {
                emit(new InstrBinary(InstrBinary.Kind.CMP, dst, src));
                emit(new InstrJump(cond, stmCJUMP.getLabelTrue()));
                return null;
            } else if (dst instanceof Operand.Imm && src instanceof Operand.Mem) {
                emit(new InstrBinary(InstrBinary.Kind.CMP, src, dst));
                emit(new InstrJump(condInv, stmCJUMP.getLabelTrue()));
                return null;
            } else if (!(dst instanceof Operand.Reg)) {
                Operand t = src;
                src = dst;
                dst = t;
                InstrJump.Cond tt = cond;
                cond = condInv;
                condInv = tt;
            }

            if (!(dst instanceof Operand.Reg)) {
                final Operand.Reg dstValue = new Operand.Reg(new Temp());
                emit(new InstrBinary(InstrBinary.Kind.MOV, dstValue, dst));
                dst = dstValue;
            }
            emit(new InstrBinary(InstrBinary.Kind.CMP, dst, src));
            emit(new InstrJump(cond, stmCJUMP.getLabelTrue()));
        }
        return null;
    }

    @Override
    public Void visit(StmSeq stmSEQ) {
        assert (false);
        return null;
    }

    @Override
    public Void visit(StmLabel stmLABEL) {
        emit(new InstrLabel(stmLABEL.getLabel()));
        return null;
    }
}
