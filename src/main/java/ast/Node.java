package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Created by bernhard on 13.11.17.
 */
public class Node {
    private ComplexSymbolFactory.Location left;
    private ComplexSymbolFactory.Location right;

    public Node(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
        this.left = left;
        this.right = right;
    }

    public ComplexSymbolFactory.Location getLeftLocation() {
        return left;
    }

    public ComplexSymbolFactory.Location getRightLocation() {
        return right;
    }
}

