package asm.i386;

import asm.CodeGenerator;
import asm.allocator.Allocator;
import ir.*;

import java.util.*;
import java.util.stream.Collectors;

public class I386CodeGenerator implements CodeGenerator {

    @Override
    public List<Temp> getAllRegisters() {
        return Arrays.asList(Registers.eax.reg, Registers.ebp.reg, Registers.ebx.reg, Registers.ecx.reg,
                Registers.edi.reg, Registers.edx.reg, Registers.esi.reg, Registers.esp.reg);
    }

    @Override
    public List<Temp> getGeneralPurposeRegisters() {
        return Arrays.asList(Registers.eax.reg, Registers.ebx.reg, Registers.ecx.reg,
                Registers.edi.reg, Registers.edx.reg, Registers.esi.reg);
    }

    @Override
    public I386Prg codeGen(ir.Prg prg) {
        final List<I386Function> translatedFunctions = translateFunctions(prg.getFunctions());
        translatedFunctions.add(makeRaiseDispatch());
        final List<I386Data> vtableData = translateVtables(prg.getVTables());
        return new I386Prg(translatedFunctions, vtableData);
    }

    private List<I386Data> translateVtables(List<VTable> vTables) {
        return vTables.stream().map(this::translateVTable).collect(Collectors.toList());
    }

    private I386Data translateVTable(VTable vTable) {
    	String name = "LvTable_" + vTable.getClassName();
    	if(System.getProperty("os.name").contains("Mac")) {
    		name = "_" + name;
    	}
        final I386Data data = new I386Data(name);
        
        data.appendConst(0);
        vTable.getValues().forEach(value -> data.appendReference(value.getLabel()));
        return data;
    }

    private I386Function makeRaiseDispatch() {
        final InstructionEmitter emitter = new InstructionEmitter();
        emitter.emit(new InstrUnary(InstrUnary.Kind.PUSH, new Operand.Imm(-1)));
        emitter.emit(new InstrJump(InstrJump.Kind.CALL, new ir.Label("_raise")));
        emitter.emit(new InstrBinary(InstrBinary.Kind.ADD, Registers.esp, new Operand.Imm(4)));
        return new I386Function(new ir.Label("_raise_dispatch"), emitter.getMachineInstructions());
    }

    private List<I386Function> translateFunctions(List<ir.Function> functions) {
        return functions.stream().map(this::translateFunction).collect(Collectors.toList());
    }

    private I386Function translateFunction(Function function) {
        final Temp esi = new Temp();
        final Temp edi = new Temp();
        final Temp ebx = new Temp();

        final InstructionEmitter emitter = new InstructionEmitter();

        emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, new Operand.Reg(esi), Registers.esi));
        emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, new Operand.Reg(edi), Registers.edi));
        emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, new Operand.Reg(ebx), Registers.ebx));

        translateStatements(function, emitter);

        emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.ebx, new Operand.Reg(ebx)));
        emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.edi, new Operand.Reg(edi)));
        emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.esi, new Operand.Reg(esi)));

        moveResultTempToResultRegisterAndReturn(function, emitter);

        final I386Function i386Function = new I386Function(function.getName(), emitter.getMachineInstructions());

        final Allocator allocator = new Allocator(i386Function, new HashSet<>(getGeneralPurposeRegisters()), new HashSet<>(getAllRegisters()));
        allocator.allocate();

        i386Function.removeIdempotentMoves();

        return i386Function;
    }

    private void moveResultTempToResultRegisterAndReturn(Function function, InstructionEmitter emitter) {
        emitter.emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.eax, new Operand.Reg(function.getReturnTemp())));
        emitter.emit(new InstrNullary(InstrNullary.Kind.RET));
    }

    private void translateStatements(Iterable<ir.Stm> statements, InstructionEmitter emitter) {
        final StatementVisitor stmVisitor = new StatementVisitor(emitter);
        statements.forEach(statement -> statement.accept(stmVisitor));
    }
}
