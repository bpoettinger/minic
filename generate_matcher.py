from copy import deepcopy


class Formatter:
    def __init__(self):
        self.indent = ''
        self.result = ''

    def inc(self):
        self.indent += '|'

    def dec(self):
        self.indent = self.indent[:-1]

    def print(self, text):
        self.result += self.indent + str(text) + '\n'


class Any:
    
    def __init__(self, *options):
        self.options = options
    
    def expand(self):
        result = []
        for option in self.options:
            result.extend(option.expand())
        return result
    
    def traverse(self, visitor):
        raise Exception('')


class Temp:

    def __init__(self, name=None, action=None):
        self.name = name
        self.type = 'ExpTemp'
        self.action = action
    
    def expand(self):
        return (self,)
    
    def traverse(self, visitor):
        visitor.visit(self)

    def condition(self, name, cache):
        return '{} instanceof ExpTemp'.format(name)

    def accessors(self):
        return ()

    def __hash__(self):
        return 1

    def __eq__(self, other):
        return type(other) is Temp and self.name == other.name

    def __str__(self):
        return 'Temp({})'.format(self.name)


class Const:

    def __init__(self, values=None, name=None, action=None):
        self.name = name
        self.values = values
        self.type = 'ExpConst'
        self.action = action
    
    def expand(self):
        return (self,)

    def traverse(self, visitor):
        visitor.visit(self)

    def condition(self, name, cache):
        return '{} instanceof ExpConst'.format(name) + (' && {}.contains(((ExpConst){}).getValue())'.format(cache.get_from_cache(self.values), name) if self.values else '')

    def accessors(self):
        return ()

    def __hash__(self):
        return 2

    def __eq__(self, other):
        return type(other) is Const and self.values == other.values and self.name == other.name

    def __str__(self):
        return 'Const({})'.format(self.name)


class Param:

    def __init__(self, name=None, action=None):
        self.name = name
        self.type = 'ExpParam'
        self.action = action
    
    def expand(self):
        return (self,)

    def traverse(self, visitor):
        visitor.visit(self)

    def condition(self, name, cache):
        return '{} instanceof ExpParam'.format(name)

    def accessors(self):
        return ()

    def __hash__(self):
        return 3

    def __eq__(self, other):
        return type(other) is Param and self.name == other.name

    def __str__(self):
        return 'Param({})'.format(self.name)


class Name:

    def __init__(self, name=None, action=None):
        self.name = name
        self.type = 'ExpName'
        self.action = action

    def expand(self):
        return (self,)

    def traverse(self, visitor):
        visitor.visit(self)

    def condition(self, name, cache):
        return '{} instanceof ExpName'.format(name)

    def accessors(self):
        return ()

    def __hash__(self):
        return 4

    def __eq__(self, other):
        return type(other) is Name and self.name == other.name

    def __str__(self):
        return 'Name({})'.format(self.name)


class Mem:
    
    def __init__(self, addr, name=None, action=None):
        self.addr = addr
        self.name = name
        self.type = 'ExpMem'
        self.action = action
    
    def expand(self):
        return [Mem(addr, name=self.name, action=self.action) for addr in self.addr.expand()]

    def traverse(self, visitor):
        visitor.visit(self)
        self.addr.traverse(visitor)

    def condition(self, name, cache):
        return '{} instanceof ExpMem'.format(name)

    def accessors(self):
        return ['getAddress']

    def __hash__(self):
        return 5

    def __eq__(self, other):
        return type(other) is Mem and self.name == other.name

    def __str__(self):
        return 'Mem({})'.format(self.name)


class Call:

    def __init__(self, name=None, action=None):
        self.name = name
        self.action = action
        self.type = 'ExpCall'

    def expand(self):
        return (self,)

    def traverse(self, visitor):
        visitor.visit(self)

    def condition(self, name, cache):
        return '{} instanceof ExpCall'.format(name)

    def accessors(self):
        return ()

    def __hash__(self):
        return 8

    def __eq__(self, other):
        return type(other) is Call and self.name == other.name

    def __str__(self):
        return 'Call({})'.format(self.name)

    
class BinOp:
    
    def __init__(self, ops, lhs, rhs, name=None, action=None):
        self.ops = ops
        self.lhs = lhs
        self.rhs = rhs
        self.name = name
        self.type = 'ExpBinOp'
        self.action = action
        
    def expand(self):
        ls = self.lhs.expand()
        rs = self.rhs.expand()
        return [BinOp(self.ops, l, r, name=self.name, action=self.action) for l in ls for r in rs]

    def traverse(self, visitor):
        visitor.visit(self)
        self.lhs.traverse(visitor)
        self.rhs.traverse(visitor)

    def condition(self, name, cache):
        return '{} instanceof ExpBinOp'.format(name) + (' && {}.contains(((ExpBinOp){}).getOp())'.format(cache.get_from_cache(self.ops), name) if self.ops else '')

    def accessors(self):
        return ['getLeft', 'getRight']

    def __hash__(self):
        return 6

    def __eq__(self, other):
        return type(other) is BinOp and self.name == other.name and self.ops == other.ops

    def __str__(self):
        return 'BinOp({},{})'.format(self.ops, self.name)


class Wildcard:

    def __init__(self, name=None, force_reg=True):
        self.name = name
        self.action = None
        self.force_reg = force_reg

    def expand(self):
        return (self,)

    def traverse(self, visitor):
        visitor.visit(self)

    def accessors(self):
        return ()

    def __hash__(self):
        return 7

    def __eq__(self, other):
        return type(other) is Wildcard and self.name == other.name

    def __str__(self):
        return 'Wildcard({})'.format(self.name)


class Node:

    def __init__(self):
        self.children = {}
        self.patterns = []

    def add_child(self, condition):
        if condition not in self.children:
            self.children[condition] = Node()
        return self.children[condition]

    def format(self, formatter):
        for condition, node in self.children.items():
            formatter.print(str(condition) + ': ' + str(node.patterns))
            formatter.inc()
            node.format(formatter)
            formatter.dec()

    def __str__(self):
        formatter = Formatter()
        self.format(formatter)
        return formatter.result


class Builder:

    def __init__(self):
        self.root = Node()
        self.cur_node = None

    def add_pattern(self, pattern):
        self.cur_node = self.root
        pattern.traverse(self)
        assert self.cur_node
        self.cur_node.patterns.append(pattern)

    def visit(self, condition):
        self.cur_node = self.cur_node.add_child(condition)


def build_decision_tree(pattern_root):
    patterns = pattern_root.expand()
    builder = Builder()
    for pattern in patterns:
        builder.add_pattern(pattern)
    return builder.root


class Printer:

    def __init__(self):
        self.indent = ''
        self.values = {}
        self.counter = 1
        self.cache = dict()

    def inc(self):
        self.indent += ' ' * 4

    def dec(self):
        self.indent = self.indent[:-4]

    def print_line(self, text, *args):
        print(self.indent + text.format(*args))

    def get_from_cache(self, data):
        if data not in self.cache:
            index = self.counter
            self.counter += 1
            self.cache[data] = 'T' + str(index)
        return self.cache[data]

    def print_tree(self, node):
        self.print_line('''/*
=====================================
GENERATED CODE, CHANGES WILL BE LOST!
===================================== 
*/

package asm.i386;
        
import ir.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import asm.i386.InstructionEmitter;
import asm.i386.I386Instruction;
''')
        self.print_line('public class Matcher {{')
        self.inc()
        self.print_line('private final InstructionEmitter emitter;')
        self.print_line('public Matcher(InstructionEmitter emitter) {{ this.emitter = emitter; }}')
        self.print_line('private void emit(I386Instruction instruction) {{ emitter.emit(instruction); }}')
        self.print_line('public Operand match(Exp exp) {{')
        self.inc()
        self.print_tree_impl(node, [('t0', ['exp'])], [])
        self.print_line('throw new RuntimeException();')
        self.dec()
        self.print_line('}}')
        self.print_line('''private static Map<ExpBinOp.Op, InstrBinary.Kind> createOpMap() {{
        final Map<ExpBinOp.Op,InstrBinary.Kind> opMap = new HashMap<>();
        opMap.put(ExpBinOp.Op.PLUS, InstrBinary.Kind.ADD);
        opMap.put(ExpBinOp.Op.MINUS, InstrBinary.Kind.SUB);
        opMap.put(ExpBinOp.Op.MUL, InstrBinary.Kind.IMUL);
        opMap.put(ExpBinOp.Op.DIV, InstrBinary.Kind.IMUL);
        return opMap;
    }}
    private static final Map<ExpBinOp.Op, InstrBinary.Kind> opMap = createOpMap();''')
        self.print_cache()
        self.dec()
        self.print_line('}}')

    def gen_name(self):
        index = self.counter
        self.counter += 1
        return 't' + str(index)

    def print_tree_impl(self, node, stack, wildcards):
        if node.children:
            first = True
            wildcard = False

            new_variable = self.gen_name()

            while True:
                variable, accessors = stack[-1]
                if not accessors:
                    stack.pop()
                else:
                    break

            accessor = accessors.pop(0)

            self.print_line('Exp {} = {}.{}();', new_variable, variable, accessor)
            for condition, child in node.children.items():
                if type(condition) is Wildcard:
                    assert not wildcard
                    wildcard = (condition, child)
                    continue

                typed_variable = condition.name or self.gen_name()

                local_stack = deepcopy(stack)
                local_stack.append((typed_variable, condition.accessors()))

                self.print_line('{}if ({}) {{', '' if first else 'else ', condition.condition(new_variable, printer))
                self.inc()
                self.print_line('{} {} = ({}) {};', condition.type, typed_variable, condition.type, new_variable)
                self.print_tree_impl(child, local_stack, wildcards)
                self.dec()
                self.print_line('}}')
                first = False

            if wildcard:
                condition, child = wildcard
                local_wildcards = deepcopy(wildcards)
                local_wildcards.append((new_variable, condition))
                self.print_tree_impl(child, stack, local_wildcards)
        else:
            for variable, condition in wildcards:
                self.print_line('Operand {} = match({});', condition.name, variable)
                if condition.force_reg:
                    self.print_line('if (!({} instanceof Operand.Reg)) {{', condition.name)
                    self.inc()
                    self.print_line('Operand.Reg temp = new Operand.Reg(new Temp(), false);')
                    self.print_line('emit(new InstrBinary(InstrBinary.Kind.MOV, temp, {}));', condition.name)
                    self.print_line('{} = temp;', condition.name)
                    self.dec()
                    self.print_line('}}')
            self.print_line('{};', node.patterns[0].action)

    def print_cache(self):
        for data, name in self.cache.items():
            item_type, typed_data = convert_data_item_to_str(data)
            self.print_line('private static final List<{}> {} = Arrays.asList({});', item_type, name, ', '.join(typed_data))


def convert_data_item_to_str(data):
    if type(data[0]) is int:
        typed_data = map(str, data)
        item_typ = 'Integer'
    else:
        MAP = {
            '+': 'PLUS',
            '-': 'MINUS',
            '*': 'MUL',
            '/': 'DIV',
        }
        typed_data = map(lambda x: 'ExpBinOp.Op.' + MAP[x], data)
        item_typ = 'ExpBinOp.Op'

    return item_typ, typed_data


def gen_mem():
    base = Temp(name='base')
    index = Temp(name='index')
    offset = Const(name='offset')
    _1248 = Const(values=(1, 2, 4, 8), name='scale')

    times_1248 = Any(BinOp(('*',), _1248, index), BinOp(('*',), index, _1248))
    add_times_1248 = Any(BinOp(('+',), base, times_1248), BinOp(('+',), times_1248, base))
    add_offset = Any(BinOp(('+',), base, offset), BinOp(('+',), offset, base))
    offset_and_times_1248 = Any(BinOp(('+',), times_1248, offset), BinOp(('+',), offset, times_1248))
    add_times_1248_and_offset = Any(
        BinOp(('+',), add_offset, times_1248),
        BinOp(('+',), times_1248, add_offset),
        BinOp(('+',), add_times_1248, offset),
        BinOp(('+',), offset, add_times_1248),
        BinOp(('+',), base, offset_and_times_1248),
        BinOp(('+',), offset_and_times_1248, base))

    add_index = BinOp(('+',), base, index)
    add = Any(BinOp(('+',), add_offset, index),
              BinOp(('+',), index, add_offset),
              BinOp(('+',), add_index, offset),
              BinOp(('+',), offset, add_index))

    mem_t = Mem(base, action='return new Operand.Mem(base.getTemp())')
    mem_add_times_1248 = Mem(add_times_1248, action='return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), 0)')
    mem_add_offset = Mem(add_offset, action='return new Operand.Mem(base.getTemp(), null, null, offset.getValue())')
    mem_add_times_1248_and_offset = Mem(add_times_1248_and_offset, action='return new Operand.Mem(base.getTemp(), scale.getValue(), index.getTemp(), offset.getValue())')
    mem_add = Mem(add, action='return new Operand.Mem(base.getTemp(), 1, index.getTemp(), offset.getValue())')

    mem_wc_trivial = Mem(Wildcard(name='wc'), action='return new Operand.Mem(((Operand.Reg)wc).reg)')
    mem_wc_base = Mem(Any(BinOp(('+',), base, Wildcard(name='wc')), BinOp(('+',), Wildcard(name='wc'), base)), action='return new Operand.Mem(base.getTemp(), 1, ((Operand.Reg)wc).reg, 0)')
    mem_wc_offset = Mem(Any(BinOp(('+',), Wildcard(name='wc'), offset), BinOp(('+',), offset, Wildcard(name='wc'))), action='return new Operand.Mem(((Operand.Reg)wc).reg, null, null, offset.getValue())')
    mem_wc = Any(mem_wc_trivial, mem_wc_base, mem_wc_offset)

    return Any(mem_t, mem_add_times_1248, mem_add_offset, mem_add_times_1248_and_offset, mem_wc, mem_add)


def gen_param():
    return Param(name='param', action='return new Operand.Mem(Registers.ebp.reg, null, null, 8+4*param.getNumber())')


def gen_arith():
    w1 = Wildcard(name='w1')
    w2 = Wildcard(name='w2', force_reg=False)

    return Any(
        BinOp(('+', '-', '*',), w1, w2, name='op', action='if (!((Operand.Reg)w1).isReusable) { '
                                                          'Operand.Reg t = new Operand.Reg(new Temp(), false); '
                                                          'emit(new InstrBinary(InstrBinary.Kind.MOV, t, w1)); '
                                                          'emit(new InstrBinary(opMap.get(op.getOp()), t, w2)); return t; } '
                                                          'else { emit(new InstrBinary(opMap.get(op.getOp()), w1, w2)); return w1; }'),
        BinOp(('/',), w1, w2, name='op', action='''Operand.Reg t = new Operand.Reg(new Temp(), false);
                    emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.eax, w1));
                    emit(new InstrBinary(InstrBinary.Kind.MOV, Registers.edx, Registers.eax));
                    emit(new InstrBinary(InstrBinary.Kind.SAR, Registers.edx, new Operand.Imm(31)));
                    emit(new InstrUnary(InstrUnary.Kind.IDIV, w2));
                    emit(new InstrBinary(InstrBinary.Kind.MOV, t, Registers.eax));
                    return t''')
    )


def gen_const():
    return Const(name='c', action='return new Operand.Imm(c.getValue())')


def gen_temp():
    return Temp(name='t', action='return new Operand.Reg(t.getTemp())')


def gen_name():
    return Name(name='n', action='Operand.Reg t = new Operand.Reg(new Temp(), false); emit(new InstrBinary(InstrBinary.Kind.LEA, t, new Operand.Name(n.getLabel()))); return t')


def gen_call():
    return Call(name='call', action='ExpressionVisitor v = new ExpressionVisitor(emitter); return call.accept(v)')


mem = gen_mem()
param = gen_param()
arith = gen_arith()
const = gen_const()
temp = gen_temp()
name = gen_name()
call = gen_call()

root = build_decision_tree(Any(mem, param, arith, const, temp, name, call))

printer = Printer()
printer.print_tree(root)
