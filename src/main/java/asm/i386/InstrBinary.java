package asm.i386;

import ir.Label;
import ir.Temp;
import util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

final class InstrBinary implements I386Instruction {

    enum Kind {
        MOV, ADD, SUB, SHL, SHR, SAL, SAR, AND, OR, XOR, TEST, CMP, LEA, IMUL
    }
    private Operand src;
    private Operand dst;
    private final Kind kind;
    InstrBinary(Kind kind, Operand dst, Operand src) {
        assert (kind != null && src != null && dst != null);
        assert (!((src instanceof Operand.Mem) && (dst instanceof Operand.Mem)));
        assert (kind != Kind.LEA || ((src instanceof Operand.Mem || src instanceof Operand.Name) && (dst instanceof Operand.Reg)));
        this.src = src;
        this.dst = dst;
        this.kind = kind;
    }

    @Override
    public Collection<Temp> use() {
        final List<Temp> result = new ArrayList<>();

        switch (kind) {
	        case MOV:
	        case LEA:
	        	result.addAll(src.use());
	        	if(dst instanceof Operand.Mem) {
	        		result.addAll(dst.use());
	        	}
	        	break;
            case ADD:
            case SUB:
            case SHL:
            case SHR:
            case SAL:
            case SAR:
            case AND:
            case OR:
            case XOR:
            case IMUL:
            case TEST:
            case CMP:
                result.addAll(src.use());
                result.addAll(dst.use());
                break;
            default:
                throw new RuntimeException();
        }

        return result;
    }

    @Override
    public Collection<Temp> def() {
        final List<Temp> result = new ArrayList<>();

        switch (kind) {
            case MOV:
            case ADD:
            case SUB:
            case SHL:
            case SHR:
            case SAL:
            case SAR:
            case AND:
            case OR:
            case XOR:
            case LEA:
            case IMUL:
            	if(dst instanceof Operand.Mem) {
            		break;
            	}
                result.addAll(dst.use());
                break;
            case TEST:
            case CMP:
                break;
            default:
                throw new RuntimeException();
        }

        return result;
    }

    @Override
    public Collection<Label> jumps() {
        return Collections.emptyList();
    }

    @Override
    public boolean isFallThrough() {
        return true;
    }

    @Override
    public Pair<Temp, Temp> isMoveBetweenTemps() {
        switch (kind) {
            case MOV:
                if (src instanceof Operand.Reg && dst instanceof Operand.Reg) {
                    return new Pair<>(((Operand.Reg) dst).reg, ((Operand.Reg) src).reg);
                } else {
                    return null;
                }
            default:
                return null;
        }
    }

    @Override
    public Label isLabel() {
        return null;
    }

    @Override
    public String toString() {
        return "\t" + kind + " " + dst + ", " + src + "\n";
    }

    @Override
    public void rename(Function<Temp, Temp> sigma) {
        src = src.rename(sigma);
        dst = dst.rename(sigma);
    }

    @Override
    public List<Operand.Reg> getSpillableOperands() {
        final List<Operand.Reg> result = new ArrayList<>();
        switch (kind) {
        case ADD:
        case SUB:
        case MOV:
        case CMP:
            if (dst instanceof Operand.Reg && !(src instanceof Operand.Mem)) {
                result.add((Operand.Reg) dst);
                if (src instanceof Operand.Reg) {
                    result.add((Operand.Reg) src);
                }
            }
            break;
        case IMUL:
            if (dst instanceof Operand.Reg && src instanceof Operand.Reg) {
                result.add((Operand.Reg) src);
            }
            break;
        }
        return result;
    }

    @Override
    public void replaceSpillableOperand(Operand original, Operand replacement) {
        if (original == dst) {
            dst = replacement;
        } else if (original == src) {
            src = replacement;
        } else {
            assert false;
        }
    }
}
