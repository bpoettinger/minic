package typechecker;
import java.util.Collections;
import java.util.List;

public class Method extends Entity {

    private Type returnType;
    private List<Type> parameterTypes;
    private boolean throwsIoException;

    public Method(Type returnType, List<Type> parameterTypes, boolean throwsIoException) {
        this.returnType = returnType;
        this.parameterTypes = parameterTypes;
        this.throwsIoException = throwsIoException;
    }

    public Type getReturnType() {
        return returnType;
    }

    public List<Type> getParameterTypes() {
        return Collections.unmodifiableList(parameterTypes);
    }

    public boolean throwsIoException() {
        return throwsIoException;
    }
}
