package asm.i386;

import ir.Label;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class I386Data {

    public static abstract class Expr {

        public abstract String toString();
    }

    private static class Ref extends Expr {
        final String name;

        private Ref(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private static class Const extends Expr {
        final int value;

        private Const(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }

    private final String name;
    private final List<Expr> words = new ArrayList<>();

    public I386Data(String name) {
        this.name = name;
    }

    public int appendConst(int value) {
        words.add(new Const(value));
        return words.size() * 4 - 4;
    }

    public int appendReference(Label label) {
        words.add(new Ref(label.toString()));
        return words.size() * 4 - 4;
    }

    public String getName() {
        return name;
    }

    public List<Expr> getWords() {
        return Collections.unmodifiableList(words);
    }
}
