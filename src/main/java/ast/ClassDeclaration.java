package ast;

import java_cup.runtime.ComplexSymbolFactory;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class ClassDeclaration extends Node {

  private final String className;
  private final String superName;
  private final List<Member> members;
  private Optional<ClassDeclaration> superClassDeclaration = Optional.empty();

  public ClassDeclaration(String className,
                          List<Member> members,
                          ComplexSymbolFactory.Location from,
                          ComplexSymbolFactory.Location to) {
    this(className, null, members, from, to);
  }

  public ClassDeclaration(String className, String superName,
                          List<Member> members,
                          ComplexSymbolFactory.Location from,
                          ComplexSymbolFactory.Location to) {
    super(from, to);
    this.className = className;
    this.superName = superName;
    this.members = members;
  }

  public void setSuperClassDeclaration(ClassDeclaration superClassDeclaration) {
    this.superClassDeclaration = Optional.ofNullable(superClassDeclaration);
  }

  public Optional<ClassDeclaration> getSuperClassDeclaration() {
    return superClassDeclaration;
  }

  public String getClassName() {
    return className;
  }

  public String getSuperName() {
    return superName;
  }

  public List<Member> getMembers() {
    return members;
  }

  public List<FieldDeclaration> getFields() {
    return members.stream()
            .filter(m -> m instanceof FieldDeclaration)
            .map(m -> (FieldDeclaration) m)
            .collect(Collectors.toList());
  }

  public List<MethodDeclaration> getMethods() {
    return members.stream()
            .filter(m -> m instanceof MethodDeclaration)
            .map(m -> (MethodDeclaration) m)
            .collect(Collectors.toList());
  }

  public List<ClassDeclaration> getSuperClasses() {
    final List<ClassDeclaration> hierarchy = new ArrayList<>();
    ClassDeclaration curClassDecl = this;
    hierarchy.add(curClassDecl);

    while (curClassDecl.getSuperClassDeclaration().isPresent()) {
      curClassDecl = curClassDecl.getSuperClassDeclaration().get();
      hierarchy.add(curClassDecl);
    }

    Collections.reverse(hierarchy);
    return hierarchy;
  }
}
