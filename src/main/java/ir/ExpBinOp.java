package ir;

public class ExpBinOp extends Exp {

  public enum Op {
    PLUS, MINUS, MUL, DIV, AND, OR, LSHIFT, RSHIFT, ARSHIFT, XOR
  }

  private final Op op;
  private final Exp left;
  private final Exp right;

  public ExpBinOp(Op op, Exp left, Exp right) {
    if (op == null || left == null || right == null) {
      throw new NullPointerException();
    }
    this.op = op;
    this.left = left;
    this.right = right;
  }

  public Op getOp() {
    return op;
  }

  public Exp getLeft() {
    return left;
  }

  public Exp getRight() {
    return right;
  }


  @Override
  public Type getType() {
    return Type.INTEGER;
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "BINOP(" + getOp() + ", " + getLeft() + ", " + getRight() + ")";
  }

}
