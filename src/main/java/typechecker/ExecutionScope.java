package typechecker;

import java.util.Optional;

/**
 * Created by bernhard on 12.11.17.
 */
public class ExecutionScope extends Scope {

    private Optional<ExecutionScope> parentExecutionScope;

    public ExecutionScope(Scope parentScope) {
        super(parentScope);
        this.parentExecutionScope = Optional.empty();
    }

    public ExecutionScope(ExecutionScope parentScope) {
        super(parentScope);
        this.parentExecutionScope = Optional.of(parentScope);
    }

    @Override
    public void addMethod(String name, Method method) {
        throw new IllegalStateException("Adding methods is not allowed in " + this.getClass().getSimpleName());
    }

    @Override
    public void addClass(String name, Class type) {
        throw new IllegalStateException("Adding types is not allowed in " + this.getClass().getSimpleName());
    }

    @Override
    public void addVariable(String name, Variable variable) {
        if (parentExecutionScope.isPresent() && parentExecutionScope.get().hasVariableInExecutionScope(name)) {
            throw new TypeError(String.format("Variable %s already in scope", name));
        } else {
            super.addVariable(name, variable);
        }
    }

    public boolean hasVariableInExecutionScope(String name) {
        return variables.containsKey(name)
                || parentExecutionScope.isPresent() && parentExecutionScope.get().hasVariableInExecutionScope(name);
    }
}
