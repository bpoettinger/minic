package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents the type {@code boolean}.
 */
public class TypeBoolean extends Type {

  public TypeBoolean(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
  }

  @Override
  public String toString() {
    return "boolean";
  }

  @Override
  public boolean equals(Object obj) {
    return (obj instanceof TypeBoolean);
  }

  @Override
  public <A> A accept(TypeVisitor<A> v) {
    return v.visit(this);
  }

  @Override
  public int hashCode() {
    return 7;
  }
}
