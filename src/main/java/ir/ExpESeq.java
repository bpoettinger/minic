package ir;

public class ExpESeq extends Exp {
  private final Stm stm;
  private final Exp exp;

  public ExpESeq(Stm stm, Exp exp) {
    if (stm == null || exp == null) {
      throw new NullPointerException();
    }
    this.stm = stm;
    this.exp = exp;
  }

  public Stm getStm() {
    return stm;
  }

  public Exp getExp() {
    return exp;
  }

  @Override
  public Type getType() {
    return exp.getType();
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "ESEQ(" + getStm() + ", " + getExp() + ")";
  }


}
