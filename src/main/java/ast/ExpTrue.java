package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents {@code true}.
 */
public class ExpTrue extends Exp {

  public ExpTrue(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
  }

  @Override
  public <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T{
    return v.visit(this);
  }
}
