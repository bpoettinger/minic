package translator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by bernhard on 13.11.17.
 */
public class Env {

	private Optional<Env> parentEnv;
	private Map<String, ir.Exp> variables = new HashMap<>();
	private Map<String, ir.ExpName> labels = new HashMap<>();
	private Map<String, VTableLayout> vTables = new HashMap<>();

	public Env() {
		this.parentEnv = Optional.empty();
	}

	public Env(Env parentEnv) {
		this.parentEnv = Optional.of(parentEnv);
	}

	public void addParameter(String name, int index, ir.Exp.Type type) {
		assert (!variables.containsKey(name));
		variables.put(name, new ir.ExpParam(index, type));
	}

	public void addLocalVariable(String name, ir.Exp.Type type) {
		assert (!variables.containsKey(name));
		variables.put(name, new ir.ExpTemp(new ir.Temp(), type));
	}

	public ir.ExpMem addAttribute(String name, int offset, ir.Exp.Type type) {
		assert (!variables.containsKey(name));
		final ir.ExpMem expMem = new ir.ExpMem(
				new ir.ExpBinOp(ir.ExpBinOp.Op.PLUS, new ir.ExpParam(0, ir.Exp.Type.OBJECT), new ir.ExpConst(offset)), type);
		variables.put(name, expMem);
		return expMem;
	}

	public void addLabel(String name) {
		assert (!labels.containsKey(name));
		labels.put(name, new ir.ExpName(new ir.Label(name)));
	}

	public void addVTableLayout(String className, VTableLayout layout) {
		assert (!vTables.containsKey(className));
		vTables.put(className, layout);
	}

	public ir.Exp getVariable(String name) {
		ir.Exp exp = variables.get(name);
		if (exp == null && parentEnv.isPresent()) {
			exp = parentEnv.get().getVariable(name);
		}

		assert (exp != null);
		return exp;
	}

	public ir.ExpName getLabel(String name) {
		ir.ExpName exp = labels.get(name);
		if (exp == null && parentEnv.isPresent()) {
			exp = parentEnv.get().getLabel(name);
		}

		assert (exp != null);
		return exp;
	}

	public VTableLayout getVTableLayout(String objClass) {
		VTableLayout vTable = vTables.get(objClass);
		if (vTable == null && parentEnv.isPresent()) {
			vTable = parentEnv.get().getVTableLayout(objClass);
		}

		assert (vTable != null);
		return vTable;
	}
	
	public Collection<VTableLayout> getAllVTableLayouts(){
		return vTables.values();
	}
}
