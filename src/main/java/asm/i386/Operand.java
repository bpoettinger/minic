package asm.i386;

import ir.Label;
import ir.Temp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

abstract class Operand {

    final static class Imm extends Operand {

        final int imm;

        Imm(Integer imm) {
            assert (imm != null);
            this.imm = imm;
        }

        @Override
        public String toString() {
            return String.format("%d", imm);
        }

        @Override
        public Operand rename(Function<Temp, Temp> sigma) {
            return this;
        }
    }

    public static class Reg extends Operand {

        public final Temp reg;
        final boolean isReusable;

        Reg(Temp reg) {
            this(reg, false);
        }
        Reg(Temp reg, boolean isReusable) {
            assert (reg != null);
            this.reg = reg;
            this.isReusable = isReusable;
        }

        @Override
          public String toString() {
            if (Registers.names.containsKey(this)) {
                return Registers.names.get(this);
            } else {
                return reg.toString();
            }
        }

        @Override
        List<Temp> use() {
            return Collections.singletonList(reg);
        }

        @Override
        public Operand rename(Function<Temp, Temp> sigma) {
            return new Reg(sigma.apply(reg));
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof Reg && this.reg.equals(((Reg)that).reg);
        }

        @Override
        public int hashCode() {
            return reg.hashCode();
        }
    }

    final static class Mem extends Operand {

        final Temp base;  // maybe null
        final Integer scale; // null or 1, 2, 4 or 8;
        final Temp index;  // maybe null
        final int displacement;

        Mem(Temp base, Integer scale, Temp index, int displacement) {
            assert (scale == null || (scale == 1 || scale == 2 || scale == 4 || scale == 8));
            this.base = base;
            this.scale = scale;
            this.index = index;
            this.displacement = displacement;
        }

        Mem(Temp base) {
            this(base, null, null, 0);
        }

        @Override
        public String toString() {
            final String firstPlus = scale != null && index != null || displacement > 0 ? "+" : "";
            final String secondPlus = displacement > 0 ? "+" : "";
            final String scaleString = scale != null && scale != 1 ? "*" + scale.toString() : "";
            return String.format("DWORD PTR [%s%s%s]",
                    base != null ? new Reg(base).toString() + firstPlus : "",
                    scale != null && index != null ? new Reg(index).toString() + scaleString + secondPlus : "",
                    displacement == 0 ? "" : "" + displacement);
        }

        @Override
        List<Temp> use() {
            List<Temp> use = new ArrayList<>();
            if (base != null && !(base.equals(Registers.ebp.reg) || base.equals(Registers.esp.reg))) {
                use.add(base);
            }
            if (index != null) {
                use.add(index);
            }
            return use;
        }

        @Override
        public Operand rename(Function<Temp, Temp> sigma) {
            return new Mem(base != null ? sigma.apply(base) : null, scale,
                    index != null ? sigma.apply(index) : null, displacement);
        }
    }

    final static class Name extends Operand {

        private final Label label;

        public Name(Label label) {
            this.label = label;
        }

        @Override
        Operand rename(Function<Temp, Temp> sigma) {
            return this;
        }

        @Override
        public String toString() {
            return label.toString();
        }
    }

    List<Temp> use() {
        return Collections.emptyList();
    }

    abstract Operand rename(Function<Temp, Temp> sigma);
}
