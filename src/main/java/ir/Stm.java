package ir;

public abstract class Stm {

  public abstract <A> A accept(StmVisitor<A> visitor);

  public static Stm getNOP() {
    return new StmSeq();
  }
}
