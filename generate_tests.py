from glob import glob
from tempfile import mkdtemp
from subprocess import run, PIPE
from pathlib import Path


RESOURCE_PATH = Path('src/test/resources')


def compile_file(src, dst):
    result = run(['javac', '-d', dst, src])
    if result.returncode != 0:
        print('Failed compiling: {}'.format(src))
    else:
        print('Succeeded compiling: {}'.format(src))


def execute_file(classpath, main):
    result = run(['java', '-cp', classpath, main], stdout=PIPE)
    if result.returncode != 0:
        print('Failed executing: {}'.format(main))
        return ""
    else:
        print('Succeeded executing: {}'.format(main))
        return result.stdout


def write_output_to_destination(output, dst):
    dst.open('wb').write(output)


def generate_test(src, dst):
    main = src.stem
    temp_dir_path = Path(dst, main)
    temp_dir_path.mkdir()

    compile_file(src, temp_dir_path)
    output = execute_file(temp_dir_path, main)

    dst_file = src.with_suffix('.out')
    write_output_to_destination(output, dst_file)


def create_temp_directory():
    return Path(mkdtemp())


def generate_tests(paths):
    temp_dir = create_temp_directory()
    for path in paths:
        if not 'ShouldFail' in path.parts:
            generate_test(path, temp_dir)


def find_test_files():
    return list(RESOURCE_PATH.glob('**/*.java'))


def generate_runner(paths):
    FILE_TEMPLATE = """import org.junit.Test;

public class FunctionTests {{
{}
}}"""
    TEST_TEMPLATE = """    @Test
    public void test{}() {{
        Helpers.{}("{}");
    }}
"""

    tests = ""
    for path in paths:
        rel = path.relative_to(RESOURCE_PATH)
        rel_wo_suffix = str(rel).replace('.java', '')
        name = str(rel).replace('/', '').replace('.java', '')
        if 'RuntimeErrors' in path.parts:
            test_name = 'compileAndExecuteFail'
        elif 'ShouldFail' in path.parts:
            test_name = 'compileFileFails'
        else:
            test_name = 'compileAndExecute'
        tests += TEST_TEMPLATE.format(name, test_name, rel_wo_suffix)

    return FILE_TEMPLATE.format(tests)


def main():
    paths = find_test_files()
    generate_tests(paths)

    runner = generate_runner(paths)
    Path('src/test/java/FunctionTests.java').open('w').write(runner)


if __name__ == '__main__':
    main()
