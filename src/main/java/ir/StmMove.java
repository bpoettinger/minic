package ir;

public class StmMove extends Stm {

  private final Exp dst;
  private final Exp src;

  public StmMove(Exp dst, Exp src) {
    if (dst == null || src == null) {
      throw new NullPointerException();
    }
    this.dst = dst;
    this.src = src;
  }

  public Exp getDst() {
    return dst;
  }

  public Exp getSrc() {
    return src;
  }

  @Override
  public <A> A accept(StmVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "MOVE(" + getDst() + ", " + getSrc() + ")";
  }
}
