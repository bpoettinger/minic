import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import cli.Source;
import cli.StringSource;
import ir.Prg;
import java_cup.runtime.ComplexSymbolFactory;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import org.apache.commons.io.IOUtils;
import prettyprinter.PrettyPrinter;
import ast.Program;
import java_cup.runtime.Symbol;
import translator.Translator;
import typechecker.*;

public class Main {
	public static void main(String[] args) {
		ArgParser argParser = new ArgParser();
		CommandLine command = null;
		try {
			command = argParser.parse(args);
		} catch (ParseException e) {
			argParser.printHelp();
			System.exit(-1);
		}

		try {
			if (command.hasOption("p")) {
				prettify(command.getArgs()[0]);
			} else if (command.hasOption("i")) {
				final String inputPath = command.getArgs()[0];
				irTree(inputPath);
			} else if (command.hasOption("j")) {
				final String inputPath = command.getArgs()[0];
				irCode(inputPath);
			} else if (command.hasOption("k")) {
				final String inputPath = command.getArgs()[0];
				irCodeNormalized(inputPath);
			} else if (command.hasOption("h")) {
				argParser.printHelp();
			} else if (command.hasOption("c")) {
				final String inputPath = command.getArgs()[0];
				compile(inputPath, command.getOptionValue("o", FilenameUtils.getBaseName(inputPath) + ".out"));
			} else { // hasOption("l"), default case
				final String inputPath = command.getArgs()[0];
				String outputPath = command.getOptionValue("o", FilenameUtils.getBaseName(inputPath) + ".s");
				asmCodeToFile(inputPath, outputPath);
			}
		}
		catch (Error err) {
			System.out.println("An error occured, aborting...");
			System.exit(-1);
		}
	}

	static void compile(String fileName, String destPath) {
		final Source source = readSource(fileName);
		final Program program = parseSource(fileName, source);
		typecheckAst(fileName, program, source);
		final Prg programIrTree = createNormalizedIrTree(program);
		final String code = createAsmCode(programIrTree);
		compileAsmCodeToDestFile(destPath, code);
	}

	static void irTree(String fileName) {
		final Source source = readSource(fileName);
		final Program program = parseSource(fileName, source);
		typecheckAst(fileName, program, source);
		final Prg programIrTree = createIrTree(program);
		System.out.println(programIrTree.toString());
	}

	static void irCode(String fileName) {
		final Source source = readSource(fileName);
		final Program program = parseSource(fileName, source);
		typecheckAst(fileName, program, source);
		final Prg irTree = createIrTree(program);
		final String code = createIrCode(irTree);
		System.out.println(code);
	}

	static void irCodeNormalized(String fileName) {
		final Source source = readSource(fileName);
		final Program program = parseSource(fileName, source);
		typecheckAst(fileName, program, source);
		final Prg irTree = createNormalizedIrTree(program);
		final String code = createIrCode(irTree);
		System.out.println(code);
	}

	static void asmCodeToFile(String fileName, String destFileName) {
		final Source source = readSource(fileName);
		final Program program = parseSource(fileName, source);
		typecheckAst(fileName, program, source);
		final Prg irTree = createNormalizedIrTree(program);
		final String code = createAsmCode(irTree);
		FileWriter writer;
		try {
			writer = new FileWriter(destFileName);
			writer.write(code);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void asmCode(String fileName) {
		final Source source = readSource(fileName);
		final Program program = parseSource(fileName, source);
		typecheckAst(fileName, program, source);
		final Prg irTree = createNormalizedIrTree(program);
		final String code = createAsmCode(irTree);
		System.out.println(code);
	}

	static void prettify(String fileName) {
		final Source source = readSource(fileName);
		final Program program = parseSource(fileName, source);
		System.out.println(PrettyPrinter.prettyPrint(program));
	}

	private static String createAsmCode(Prg programIr) {
		final asm.CodeGenerator generator = new asm.i386.I386CodeGenerator();
		final asm.MachinePrg machinePrg = generator.codeGen(programIr);
		return machinePrg.renderAssembly();
	}

	private static String createIrCode(Prg programIr) {
		final ir.CmmPrinter printer = new ir.CmmPrinter();
		return printer.prgToCmm(programIr);
	}
	
	private static Prg createIrTree(Program program) {
		final Translator translator = new Translator();
		ir.Prg programIr = translator.translate(program);
		return programIr;
	}

	private static Prg createNormalizedIrTree(Program program) {
		ir.Prg prg = createIrTree(program);
		prg = ir.eval.Eval.eval(prg);
		return ir.Normalizer.normalize(prg);
	}

	private static Source readSource(String fileName) {
		try {
			final byte[] data = Files.readAllBytes(Paths.get(fileName));
			final String text = new String(data);
			return StringSource.create(text, fileName);
		} catch (IOException e) {
			System.out.println("File not found: " + fileName);
			throw new Error("File not found: " + fileName);
		}
	}

	private static Program parseSource(String fileName, Source source) {
		ComplexSymbolFactory symbolFactory = new ComplexSymbolFactory();
		Lexer lexer = new Lexer(new StringReader(source.getText()), symbolFactory);
		Parser parser = new Parser(lexer, new ComplexSymbolFactory());

		try {
			Symbol root = parser.parse();
			return (Program) root.value;
		} catch (Exception | Error ex) {
			ex.printStackTrace();
			System.out.println("Rejected program: " + fileName);
			throw new Error("Parse error: " + ex.getMessage());
		}
	}

	private static void typecheckAst(String fileName, Program program, Source source) {
		final cli.ErrorLogger errorLogger = new cli.ErrorLogger(source);
		final ErrorCounter errorCounter = new ErrorCounter(errorLogger);
		final ErrorHandler errorHandler = errorCounter;
		final RootScopeBuilder rootScopeBuilder = new RootScopeBuilder(errorHandler);
		final Scope rootScope = rootScopeBuilder.buildRootScope(program);
		final TypeChecker checker = new TypeChecker(errorHandler);

		try {
			checker.check(program, rootScope);
		} catch (TypeError err) {
			err.printStackTrace();
			System.out.println("Rejected program: " + fileName);
			throw new Error("Parse error");
		}

		if (errorCounter.hasErrors()) {
			System.out.println("Rejected program: " + fileName);
			throw new Error("Parse error");
		}
	}

	private static void compileAsmCodeToDestFile(String destPath, String code) {
		final Process compile;
		try {
			final File tempFile = writeAsmCodeToTemporaryFile(code);
			final File runtimeFile = writeRuntimeToTemporaryFile();
			if(System.getProperty("os.name").contains("Mac")) {
				compile = new ProcessBuilder("gcc", "-m32", "-Wl,-no_pie", "-o", destPath,
						tempFile.getAbsolutePath(), runtimeFile.getAbsolutePath()).start();
			} else {
				compile = new ProcessBuilder("gcc", "-m32", "-o", destPath,
						tempFile.getAbsolutePath(), runtimeFile.getAbsolutePath()).start();
			}
		} catch (IOException err) {
			throw new Error("Failed creating the executable");
		}

		try {
			compile.waitFor();
		} catch (InterruptedException e) {
			throw new Error("Process interrupted while waiting for compiliation");
		}
		if (compile.exitValue() != 0) {
			final String error = new BufferedReader(new InputStreamReader(compile.getErrorStream())).lines().collect(Collectors.joining("\n"));
			throw new Error("Failed compiling the intermediate representation using gcc: \n" + error);
		}
	}

	private static File writeRuntimeToTemporaryFile() throws IOException {
		final URL runtimePath = Main.class.getResource("runtime.c");
		final File tempFile = File.createTempFile("minic-rc-", ".c");
		final Writer writer = new BufferedWriter(new FileWriter(tempFile));
		IOUtils.copy(runtimePath.openStream(), writer);
		tempFile.deleteOnExit();
		writer.flush();
		return tempFile;
	}

	private static File writeAsmCodeToTemporaryFile(String code) throws IOException {
		final File tempFile = File.createTempFile("minic-ir-", ".s");
		tempFile.deleteOnExit();
		final FileWriter writer = new FileWriter(tempFile);
		writer.write(code);
		writer.flush();
		writer.close();
		return tempFile;
	}
}
