package typechecker;

import ast.ExpArrayGet;
import ast.ExpArrayLength;
import ast.ExpBinOp;
import ast.ExpBinOp.Op;
import ast.ExpFalse;
import ast.ExpId;
import ast.ExpIntConst;
import ast.ExpInvoke;
import ast.ExpNeg;
import ast.ExpNew;
import ast.ExpNewIntArray;
import ast.ExpRead;
import ast.ExpThis;
import ast.ExpTrue;
import ast.ExpVisitor;
import ast.TypeArray;
import ast.TypeBoolean;
import ast.TypeClass;
import ast.TypeInt;

public class ExpressionVisitor implements ExpVisitor<Type, TypeError> {

	private final ExecutionScope scope;
	private final ErrorHandler errorHandler;

	private boolean throwsException = false;

	public ExpressionVisitor(ExecutionScope scope, ErrorHandler errorHandler) {
		this.scope = scope;
		this.errorHandler = errorHandler;
	}

	private boolean checkSubExpression(ast.Exp e, Type expectedType) {
		final Type actualType = e.accept(this);
		if (!actualType.equals(expectedType)) {
			errorHandler.onTypeError(actualType, expectedType, "Invalid type", e);
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Type visit(ExpTrue e) throws TypeError {
		return Type.BOOLEAN;
	}

	@Override
	public Type visit(ExpFalse e) throws TypeError {
		return Type.BOOLEAN;
	}

	@Override
	public Type visit(ExpThis e) throws TypeError {
		if (scope.hasVariableGlobally("this")) {
			return scope.getVariable("this").getType();
		} else {
			errorHandler.onUnknownEntity("Unkown variable", e);
			return Type.VOID;
		}
	}

	@Override
	public Type visit(ExpNewIntArray e) throws TypeError {
		e.getSize().accept(this);
		return Type.array(Type.INT);
	}

	@Override
	public Type visit(ExpNew e) throws TypeError {
		if (scope.hasClassGlobally(e.getClassName())) {
			return Type.class_(scope.getClass(e.getClassName()));
		} else {
			errorHandler.onUnknownEntity("Unknown class", e);
			return Type.VOID;
		}
	}

	@Override
	public Type visit(ExpNeg e) throws TypeError {
		Type astType = e.getExp().accept(this);
		if (!astType.equals(Type.BOOLEAN)) {
			errorHandler.onTypeError(astType, Type.BOOLEAN, "Invalid type", e);
		}
		return astType;
	}

	@Override
	public Type visit(ExpBinOp e) throws TypeError {
		Op op = e.getOp();

		Type expectedLeft;
		Type expectedRight;
		Type result;

		switch (op) {
            case AND:
                expectedLeft = expectedRight = Type.BOOLEAN;
                result = Type.BOOLEAN;
                break;
            case LT:
				expectedLeft = expectedRight = Type.INT;
				result = Type.BOOLEAN;
				break;
            case PLUS:
            case MINUS:
            case TIMES:
            case DIV:
                expectedLeft = expectedRight = Type.INT;
                result = Type.INT;
                break;
            default:
                throw new RuntimeException("Incomplete switch statement, operator not handled!");
        }

        checkSubExpression(e.getLeft(), expectedLeft);
		checkSubExpression(e.getRight(), expectedRight);

		return result;
	}

	@Override
	public Type visit(ExpArrayGet e) throws TypeError {
		final boolean properArray = checkSubExpression(e.getArray(), Type.array(Type.INT));
		final boolean properIndex = checkSubExpression(e.getIndex(), Type.INT);

		if (properArray && properIndex) {
			final Type.ArrayType arrayType = (Type.ArrayType) e.getArray().accept(this);
			return arrayType.getInnerType();
		} else {
			return Type.VOID;
		}
	}

	@Override
	public Type visit(ExpArrayLength e) throws TypeError {
		checkSubExpression(e.getArray(), Type.array(Type.INT));

		return Type.INT;
	}

	@Override
	public Type visit(ExpInvoke e) throws TypeError {
		final Type objectType = e.getObj().accept(this);
		e.setObjClass(objectType.toString());

		if (!scope.hasClassGlobally(objectType.toString())) {
			errorHandler.onUnknownEntity("Unknown class", e);
			return Type.VOID;
		}
		else {
			final Class objectScope = scope.getClass(objectType.toString());
			if (!objectScope.hasMethodGlobally(e.getMethod())) {
				errorHandler.onUnknownEntity("Unknown method", e);
				return Type.VOID;
			} else {
				final Method method = objectScope.getMethod(e.getMethod());
				checkArgumentCount(e, method);
				checkArgumentTypes(e, objectScope, method);
				return method.getReturnType();
			}
		}
	}

	private void checkArgumentCount(ExpInvoke e, Method method) {
		if (method.getParameterTypes().size() != e.getArgs().size()) {
			errorHandler.onInvalidArgumentCount(e.getArgs().size(), method.getParameterTypes().size(), "Invalid argument count", e);
        }
	}

	private void checkArgumentTypes(ExpInvoke e, Class objectScope, Method method) {
		int parameterIndex = 0;
		for (Type expectedParameterAstType : method.getParameterTypes()) {
			checkArgument(e.getArgs().get(parameterIndex), objectScope, expectedParameterAstType);

			parameterIndex += 1;
		}
	}

	private void checkArgument(ast.Exp arg, Class objectScope, Type expectedParameterType) {
		final Type actualParameterType = arg.accept(this);

		if (!actualParameterType.isSubTypeOf(expectedParameterType)) {
			errorHandler.onTypeError(actualParameterType, expectedParameterType, "Invalid type", arg);
        }
	}

	@Override
	public Type visit(ExpRead e) throws TypeError {
		throwsException = true;
		return Type.INT;
	}

	@Override
	public Type visit(ExpIntConst e) throws TypeError {
		return Type.INT;
	}

	@Override
	public Type visit(ExpId e) throws TypeError {
		if (scope.hasVariableGlobally(e.getId())) {
			return scope.getVariable(e.getId()).getType();
		} else {
			errorHandler.onUnknownEntity("Unknown variable", e);
			return Type.VOID;
		}
	}

	public boolean throwsException() {
		return throwsException;
	}
}
