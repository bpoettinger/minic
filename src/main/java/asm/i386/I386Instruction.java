package asm.i386;

import asm.MachineInstruction;
import java.util.List;

public interface I386Instruction extends MachineInstruction {

    List<Operand.Reg> getSpillableOperands();
    void replaceSpillableOperand(Operand original, Operand replacement);
}
