package ir.canon;

import java.util.ArrayList;
import java.util.List;

/**
 * Main class for canonization. Use {@code canonPrg} to canonize a tree program.
 */
public class Canonizer {

  public ir.Prg canonPrg(ir.Prg prg) {
    List<ir.Function> canonizedFunctions = new ArrayList<>();
    for (ir.Function m : prg) {
      canonizedFunctions.add(canonFunction(m));
    }
    return new ir.Prg(canonizedFunctions, prg.getVTables());
  }

  public ir.Function canonFunction(ir.Function function) {
    List<ir.Stm> canonBody = new ArrayList<>();
    for (ir.Stm s : function) {
      canonBody.addAll(s.accept(new CanonStm()));
    }
    return new ir.Function(function.getName(), function.getNumberOfParameters(), canonBody, function.getReturnTemp());
  }
}
