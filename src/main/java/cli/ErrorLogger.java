package cli;
import ast.*;
import java_cup.runtime.ComplexSymbolFactory;
import typechecker.ErrorHandler;
import typechecker.Type;


class Colors {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
}

/**
 * Created by bernhard on 17.11.17.
 */
public class ErrorLogger implements ErrorHandler {

    private final Source source;

    public ErrorLogger(Source source) {
        this.source = source;
    }

    private void onTypeError(String actual, String expected, String message, ComplexSymbolFactory.Location from, ComplexSymbolFactory.Location to) {
        printColored(String.format("%s: expected %s, actual %s", message, expected, actual), from, to);
    }

    private void onNameClash(String message, ComplexSymbolFactory.Location from, ComplexSymbolFactory.Location to) {
        printColored(message, from, to);
    }

    private void onUnknownEntity(String message, ComplexSymbolFactory.Location from, ComplexSymbolFactory.Location to) {
        printColored(message, from, to);
    }

    private void printColored(String message, ComplexSymbolFactory.Location from, ComplexSymbolFactory.Location to) {
        final int lineFrom = from.getLine() - 1;
        final int lineTo = to.getLine() - 1;
        assert(lineFrom == lineTo);
        final int columnFrom = from.getColumn() - 1;
        final int columnTo = to.getColumn() - 1;

        final String excerpt = source.get(from, to);
        final String coloredExcerpt = String.format("%s%s%s%s%s",
                excerpt.substring(0, columnFrom),
                Colors.ANSI_BLACK + Colors.ANSI_RED_BACKGROUND,
                excerpt.substring(columnFrom, columnTo+1),
                Colors.ANSI_RESET,
                excerpt.substring(columnTo+1));

        System.out.println(String.format("%s (%s:%d:%d)", message, source.getPath(), lineFrom+1, columnFrom+1));
        System.out.print(String.format(">>> %s", coloredExcerpt));
        System.out.println();
    }

    @Override
    public void onTypeError(Type actual, Type expected, String message, Exp e) {
        onTypeError(actual.toString(), expected.toString(), message, e.getLeftLocation(), e.getRightLocation());
    }

    @Override
    public void onTypeError(Type actual, Type expected, String message, ast.Type e) {
        onTypeError(actual.toString(), expected.toString(), message, e.getLeftLocation(), e.getRightLocation());
    }

    @Override
    public void onNameClash(Member m, String message) {
        onNameClash(message, m.getLeftLocation(), m.getRightLocation());
    }

    @Override
    public void onNameClash(ClassDeclaration c, String message) {
        onNameClash(message, c.getLeftLocation(), c.getRightLocation());
    }

    @Override
    public void onInvalidArgumentCount(int actual, int expected, String message, ExpInvoke e) {
        printColored(String.format("%s, expected %d, actual %d", message, expected, actual), e.getLeftLocation(), e.getRightLocation());
    }

    @Override
    public void onUnknownEntity(String message, Exp e) {
        onUnknownEntity(message, e.getLeftLocation(), e.getRightLocation());
    }

    @Override
    public void onUnknownEntity(String message, StmAssign s) {
        onUnknownEntity(message, s.getLeftNameLocation(), s.getRightNameLocation());
    }

    @Override
    public void onNameClash(StmVariableDeclaration s, String s1) {
        onNameClash(s1, s.getLeftNameLocation(), s.getRightNameLocation());
    }

    @Override
    public void onNameClash(Parameter parameter, String s) {
        onNameClash(s, parameter.getLeftLocation(), parameter.getRightLocation());

    }

    @Override
    public void onUnknownEntity(String s, ClassDeclaration class_) {
        onUnknownEntity(s, class_.getLeftLocation(), class_.getRightLocation());
    }

    @Override
    public void onUnknownEntity(String s, ast.Type type) {
        onUnknownEntity(s, type.getLeftLocation(), type.getRightLocation());
    }

    @Override
    public void onUnknownEntity(String s, StmArrayAssign s1) {
        onUnknownEntity(s, s1.getLeftNameLocation(), s1.getRightNameLocation());
    }
}
