package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents a variable {@code id}.
 */
public class ExpId extends Exp {

  private final String id;

  public ExpId(String id, ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
    this.id = id;
  }

  public String getId() {
    return id;
  }

  @Override
  public <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T{
    return v.visit(this);
  }
}
