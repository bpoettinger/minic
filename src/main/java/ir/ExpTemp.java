package ir;

public class ExpTemp extends Exp {
  private final Temp temp;
  private final Type type;

  public ExpTemp(Temp temp, Type type) {
    if (temp == null) {
      throw new NullPointerException();
    }
    this.temp = temp;
    this.type = type;
  }

  public Temp getTemp() {
    return temp;
  }

  @Override
  public Type getType() {
    return type;
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "TEMP(" + getTemp() + ")";
  }

}
