package ir;

public abstract class Exp {

  public enum Type {
    OBJECT,
    INTEGER
  }

  public abstract Type getType();

  public abstract <A> A accept(ExpVisitor<A> visitor);
}
