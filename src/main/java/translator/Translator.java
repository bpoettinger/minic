package translator;

import ir.Exp;
import translator.VTableLayout.Entry;

import java.util.*;
import java.util.stream.Collectors;

import ast.ClassDeclaration;

public class Translator {

	public ir.Prg translate(ast.Program program) {

		Env programEnv = createProgramEnv(program);

		List<ir.VTable> vTables = createIrVTables(programEnv);

		final List<ir.Function> functions = new ArrayList<>();

		final ir.Function intArrayConstructor = createIntArrayConstructor(programEnv);

		functions.add(intArrayConstructor);
		functions.addAll(translateClasses(program.classes, programEnv));
		functions.add(translateMainClass(program.mainClass, programEnv));

		return new ir.Prg(functions, vTables);
	}

	private List<ir.VTable> createIrVTables(Env programEnv) {
		return programEnv.getAllVTableLayouts().stream().map(this::createIrVTable).collect(Collectors.toList());
	}

	private ir.VTable createIrVTable(VTableLayout layout) {
		ir.VTable vTable = new ir.VTable(layout.getClassName());
		for (Entry method : layout.getAllMethods()) {
			vTable.add(method.getOffset(), method.getLabel());
		}
		return vTable;
	}

	private ir.Function createIntArrayConstructor(Env programEnv) {
		programEnv.addLabel("int$__newarray__");

		final ir.Temp returnTemp = new ir.Temp();
		final ir.ExpTemp size = new ir.ExpTemp(new ir.Temp(), ir.Exp.Type.INTEGER);

		final ir.Stm bodyStatement = new ir.StmSeq(
				new ir.StmMove(size, new ir.ExpBinOp(ir.ExpBinOp.Op.MUL, new ir.ExpParam(0, ir.Exp.Type.OBJECT), new ir.ExpConst(4))),
				new ir.StmMove(size, new ir.ExpBinOp(ir.ExpBinOp.Op.PLUS, size, new ir.ExpConst(4))),
				new ir.StmMove(new ir.ExpTemp(returnTemp, ir.Exp.Type.OBJECT),
						new ir.ExpCall(new ir.ExpName(new ir.Label("_halloc")), Collections.singletonList(size), ir.Exp.Type.OBJECT)),
				new ir.StmMove(new ir.ExpMem(new ir.ExpTemp(returnTemp, ir.Exp.Type.OBJECT), ir.Exp.Type.INTEGER), new ir.ExpParam(0, ir.Exp.Type.INTEGER)));

		return new ir.Function(new ir.Label("int$__newarray__"), 1, Collections.singletonList(bodyStatement),
				returnTemp);
	}

	private ir.Function translateMainClass(ast.MainClassDeclaration class_, Env programEnv) {
		final StatementVisitor statementVisitor = new StatementVisitor(programEnv);
		final ir.Stm bodyStatement = class_.getMainBody().accept(statementVisitor);

		final ir.Temp returnTemp = new ir.Temp();
		final ir.StmMove returnStatement = new ir.StmMove(new ir.ExpTemp(returnTemp, ir.Exp.Type.INTEGER), new ir.ExpConst(0));

		final List<ir.Stm> statements = new ArrayList<>();
		Collections.addAll(statements, bodyStatement, returnStatement);

		return new ir.Function(new ir.Label("main"), 1, statements, returnTemp);
	}

	private Env createProgramEnv(ast.Program program) {
		final Set<ast.ClassDeclaration> baseClasses = new HashSet<>();
		program.classes.forEach(class_ -> class_.getSuperClassDeclaration().ifPresent(baseClasses::add));

		final Env programEnv = new Env();
		program.classes.stream().forEach(class_ -> createMethodLabels(class_, programEnv, baseClasses.contains(class_)));

		return programEnv;
	}

	private void createMethodLabels(ast.ClassDeclaration class_, Env programEnv, boolean isBaseClass) {
		programEnv.addLabel(class_.getClassName() + "$__new__");
		class_.getMethods().stream().forEach(method -> programEnv.addLabel(createMethodLabel(class_, method)));

		createVTableLayout(class_, programEnv, isBaseClass);
	}

	private void createVTableLayout(ClassDeclaration class_, Env programEnv, boolean isBaseClass) {
		final LayoutBuilder layoutBuilder = new LayoutBuilder(class_.getClassName(), isBaseClass);

		for (ClassDeclaration classDeclaration : class_.getSuperClasses()) {
			for (ast.MethodDeclaration methodDeclaration : classDeclaration.getMethods()) {
				final Exp.Type returnType = methodDeclaration.getReturnType() instanceof ast.TypeArray || methodDeclaration.getReturnType() instanceof ast.TypeClass
						? Exp.Type.OBJECT : Exp.Type.INTEGER;
				layoutBuilder.addMethod(classDeclaration.getClassName(), methodDeclaration.getMethodName(), returnType);
			}
		}

		programEnv.addVTableLayout(class_.getClassName(), layoutBuilder.getLayout());
	}

	private String createMethodLabel(ast.ClassDeclaration class_, ast.MethodDeclaration method) {
		return class_.getClassName() + "$" + method.getMethodName();
	}

	private List<ir.Function> translateClasses(List<ast.ClassDeclaration> classes, Env programEnv) {
		return classes.stream().flatMap(class_ -> translateClass(class_, programEnv).stream())
				.collect(Collectors.toList());
	}

	private List<ir.Function> translateClass(ast.ClassDeclaration class_, Env programEnv) {
		final ir.Function constructor = createConstructor(class_);

		final List<ir.Function> functions = translateMethods(class_, programEnv);
		functions.add(constructor);
		return functions;
	}

	private ir.Function createConstructor(ast.ClassDeclaration class_) {
		final int size = (class_.getFields().size() + 1) * 4;

		final ir.Temp returnTemp = new ir.Temp();
		final ir.Stm hallocStmt = new ir.StmMove(
				new ir.ExpTemp(returnTemp, ir.Exp.Type.OBJECT),
				new ir.ExpCall(
						new ir.ExpName(new ir.Label("_halloc")),
						Collections.singletonList(new ir.ExpConst(size)), ir.Exp.Type.OBJECT));
		
		final ir.Stm setVTableStmt = new ir.StmMove(
				new ir.ExpMem(new ir.ExpTemp(returnTemp, ir.Exp.Type.OBJECT), ir.Exp.Type.INTEGER),
				new ir.ExpName(new ir.Label("vTable_" + class_.getClassName())));
		
		final ir.StmSeq bodyStatement = new ir.StmSeq(hallocStmt, setVTableStmt);
		
		return new ir.Function(new ir.Label(class_.getClassName() + "$" + "__new__"), 0,
				Collections.singletonList(bodyStatement), returnTemp);
	}

	private List<ir.Function> translateMethods(ast.ClassDeclaration class_, Env programEnv) {
		final Env classEnv = createClassEnv(class_, programEnv);

		return class_.getMethods().stream().map(method -> translateMethod(class_, method, classEnv))
				.collect(Collectors.toList());
	}

	private Env createClassEnv(ast.ClassDeclaration class_, Env programEnv) {
		Env curEnv = new Env(programEnv);

		int offset = 4;
		for (ast.ClassDeclaration classDeclaration : class_.getSuperClasses()) {
			curEnv = new Env(curEnv);
			for (ast.FieldDeclaration field : classDeclaration.getFields()) {
				curEnv.addAttribute(field.getName(), offset,
						field.getType() instanceof ast.TypeArray || field.getType() instanceof ast.TypeClass
							? ir.Exp.Type.OBJECT : ir.Exp.Type.INTEGER);
				offset += 4;
			}
		}

		return curEnv;
	}

	private ir.Function translateMethod(ast.ClassDeclaration class_, ast.MethodDeclaration method, Env classEnv) {
		final Env methodEnv = createMethodEnv(method, classEnv);

		final StatementVisitor statementVisitor = new StatementVisitor(methodEnv);
		final ir.Stm bodyStatement = method.getBody().accept(statementVisitor);

		final ExpressionVisitor expressionVisitor = new ExpressionVisitor(methodEnv);
		final ir.Exp returnExpression = method.getReturnExp().accept(expressionVisitor);

		final ir.Temp returnTemp = new ir.Temp();
		final ir.Exp.Type returnType = method.getReturnType() instanceof ast.TypeArray || method.getReturnType() instanceof ast.TypeClass
				? ir.Exp.Type.OBJECT : ir.Exp.Type.INTEGER;
		final ir.StmMove returnStatement = new ir.StmMove(new ir.ExpTemp(returnTemp, returnType), returnExpression);

		final List<ir.Stm> statements = new ArrayList<>();
		Collections.addAll(statements, bodyStatement, returnStatement);

		return new ir.Function(new ir.Label(createMethodLabel(class_, method)), method.getParameters().size() + 1,
				statements, returnTemp);
	}

	private Env createMethodEnv(ast.MethodDeclaration method, Env classEnv) {
		final Env env = new Env(classEnv);

		int parameterIndex = 1;
		for (ast.Parameter parameter : method.getParameters()) {
			final ir.Exp.Type parameterType = parameter.getType() instanceof ast.TypeArray || parameter.getType() instanceof ast.TypeClass
					? ir.Exp.Type.OBJECT : ir.Exp.Type.INTEGER;
			env.addParameter(parameter.getId(), parameterIndex, parameterType);
			parameterIndex += 1;
		}

		return env;
	}
}

class LayoutBuilder {
	private int nextMethodOffset = 4;
	private VTableLayout layout;

	public LayoutBuilder(String className, boolean isBaseClass) {
		this.layout = new VTableLayout(className, isBaseClass);
	}

	public void addMethod(String className, String methodName, Exp.Type returnType) {
		final int actualMethodOffset = getMethodOffset(methodName);

		ir.ExpName methodLabel = new ir.ExpName(
				new ir.Label(className + "$" + methodName));
		layout.addMethod(methodName, actualMethodOffset, methodLabel, returnType);
	}

	private int getMethodOffset(String methodName) {
		int actualMethodOffset;
		if (layout.containsMethod(methodName)) {
			actualMethodOffset = layout.getMethod(methodName).getOffset();
		} else {
			actualMethodOffset = nextMethodOffset;
			nextMethodOffset += 4;
		}
		return actualMethodOffset;
	}

	public VTableLayout getLayout() {
		return layout;
	}
}
