package asm.i386;

import asm.MachineInstruction;
import asm.MachineFunction;
import ir.Label;
import ir.Temp;
import util.Pair;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Frame {

    private Map<Temp, Integer> allocation = new HashMap<>();
    private int size = 0;

    public void alloc(Iterable<Temp> temps) {
        temps.forEach(this::alloc);
    }

    public void alloc(Temp temp) {
        if (!allocation.containsKey(temp)) {
            final int offset = size;
            allocation.put(temp, offset);
            size += 4;
        }
    }

    public int getOffset(Temp temp) {
        return allocation.get(temp);
    }

    public int getSize() {
        return size;
    }
}

class Body {

    private List<MachineInstruction> instructions;

    public Body(List<MachineInstruction> instructions) {
        this.instructions = instructions;
    }

    public List<MachineInstruction> getInstructions() {
        return instructions;
    }

    public void renameRegisters(Function<Temp, Temp> sigma) {
        instructions.forEach(instruction -> instruction.rename(sigma));
    }

    public void spill(List<Temp> tempsToSpill, Frame frame) {
        final List<MachineInstruction> newInstructions = new ArrayList<>();

        for (MachineInstruction instruction : instructions) {
            final Map<Temp, Temp> temps = new HashMap<>();
            tempsToSpill.forEach(x -> temps.put(x, new Temp()));

            if (instruction instanceof InstrBinary) {
                injectSpillIfPossible(tempsToSpill, frame, (InstrBinary) instruction);
            }

            // If a spill was injected into the instruction then the replaced temp will not occur in the lists
            // of used or defd temps and therefore not be replaced.

            final Collection<Temp> usedTemps = instruction.use();
            final Collection<Temp> defdTemps = instruction.def();

            for (Temp temp : tempsToSpill) {
                if (usedTemps.contains(temp)) {
                    newInstructions.add(
                            new InstrBinary(InstrBinary.Kind.MOV,
                                    new Operand.Reg(temps.get(temp)),
                                    new Operand.Mem(Registers.ebp.reg, null, null, -4-frame.getOffset(temp))));
                }
            }

            instruction.rename(x -> temps.getOrDefault(x, x));
            newInstructions.add(instruction);

            final List<Temp> reversedTempsToSpill = new ArrayList<>(tempsToSpill);
            Collections.reverse(reversedTempsToSpill);
            for (Temp temp : reversedTempsToSpill) {
                if (defdTemps.contains(temp)) {
                    newInstructions.add(
                            new InstrBinary(InstrBinary.Kind.MOV,
                                    new Operand.Mem(Registers.ebp.reg, null, null, -4-frame.getOffset(temp)),
                                    new Operand.Reg(temps.get(temp))));
                }
            }
        }

        instructions = newInstructions;
    }

    private void injectSpillIfPossible(List<Temp> tempsToSpill, Frame frame, InstrBinary instruction) {
        final List<Operand.Reg> spillableOperands = instruction.getSpillableOperands();
        for (Operand.Reg spillableOperand : spillableOperands) {
            if (tempsToSpill.contains(spillableOperand.reg)) {
                final Operand.Mem replacementOperand =
                        new Operand.Mem(Registers.ebp.reg, null, null, -4-frame.getOffset(spillableOperand.reg));
                instruction.replaceSpillableOperand(spillableOperand, replacementOperand);
                break;
            }
        }
    }

    public void removeInstructions(List<MachineInstruction> instructionsToRemove) {
        this.instructions.removeAll(instructionsToRemove);
    }
}

final public class I386Function implements MachineFunction {

    private final Label name;
    private Body body;
    private Frame frame = new Frame();

    I386Function(Label name, List<MachineInstruction> instructions) {
        this.name = name;
        this.body = new Body(instructions);
    }

    public Label getName() {
        return name;
    }

    @Override
    public Iterator<MachineInstruction> iterator() {
        return body.getInstructions().iterator();
    }

    @Override
    public void rename(Function<Temp, Temp> sigma) {
        body.renameRegisters(sigma);
    }

    @Override
    public void spill(List<Temp> tempsToSpill) {
        frame.alloc(tempsToSpill);
        body.spill(tempsToSpill, frame);
    }

    @Override
    public void removeInstructions(List<MachineInstruction> instructionsToRemove) {
        body.removeInstructions(instructionsToRemove);
    }

    @Override
    public int getInstructionCount() {
        return body.getInstructions().size();
    }

    public Frame getFrame() {
        return frame;
    }

    public void removeIdempotentMoves() {
        body = new Body(body.getInstructions().stream().filter(instruction -> {
            final Pair<Temp, Temp> fromTo = instruction.isMoveBetweenTemps();
            return !(fromTo != null && fromTo.fst.equals(fromTo.snd));
        }).collect(Collectors.toList()));
    }
}
