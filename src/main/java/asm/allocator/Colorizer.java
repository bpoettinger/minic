package asm.allocator;

import asm.MachineInstruction;
import asm.i386.Registers;
import ir.Temp;
import util.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class Colorizer {

    private final Set<Temp> registers;
    private final int numRegisters;
    private final Collection<MachineInstruction> moveInstructions;
    private final InterferenceGraph interference;

    private final Stack<InterferenceGraph.Node> removedTemps = new Stack<>();

    private boolean remainingUncoloredNodes = false;
    private final Set<Temp> spilledTemps = new HashSet<>();
    private final Map<Temp, Temp> renamedTemps = new HashMap<>();
    private final List<MachineInstruction> moveInstructionsToRemove = new ArrayList<>();
    private Map<Temp, Temp> mergedTemps = new HashMap<>();

    public Colorizer(Set<Temp> registers, InterferenceGraph interference, Collection<MachineInstruction> moveInstructions) {
        this.interference = interference;
        this.registers = registers;
        this.numRegisters = registers.size();
        this.moveInstructions = moveInstructions;
    }

    public void colorize() {
        while (true) {
            while (true) {
                while (true) {
                    simplify();
                    final boolean coalesced = coalesce();
                    if (!coalesced) {
                        break;
                    }
                }
                final boolean freezed = freeze();
                if (!freezed) {
                    break;
                }
            }
            final boolean spilled = spill();
            if (!spilled) {
                // all remaining nodes in the  are colored
                break;
            }
        }
        select();
    }

    private void simplify() {
        while (true) {
            final Set<InterferenceGraph.Node> nodesToBeRemoved = findRemovableNodes();

            if (nodesToBeRemoved.isEmpty()) {
                break;
            }

            pushNodes(nodesToBeRemoved);
        }
    }

    private Set<InterferenceGraph.Node> findRemovableNodes() {
        final Set<InterferenceGraph.Node> removableNodes = new HashSet<>();
        for (InterferenceGraph.Node node : interference.getNonMoveRelatedNodes()) {
            final int numAdjacentNodes = node.getAdjacentNodes().size();
            if (numAdjacentNodes < numRegisters && node.getRegister() == null) {
                removableNodes.add(node);
            }
        }
        return removableNodes;
    }

    private void pushNodes(Set<InterferenceGraph.Node> nodes) {
        for (InterferenceGraph.Node nodeToBeRemoved : nodes) {
            interference.removeNode(nodeToBeRemoved.getTemporaries());
            removedTemps.add(nodeToBeRemoved);
        }
    }

    private boolean coalesce() {
        for (MachineInstruction move : moveInstructions) {
            final Pair<Temp, Temp> x = move.isMoveBetweenTemps();

            final InterferenceGraph.Node node1 = interference.getNode(x.fst);
            final InterferenceGraph.Node node2 = interference.getNode(x.snd);

            if (node1 == null || node2 == null) {
                // node1 or node2 already pushed on stack
                continue;
            }

            if (node1.interferes(node2)) {
                // nodes interfere
                continue;
            }

            final Set<InterferenceGraph.Node> kNeighbors1 = node1.getAdjacentKNodes(numRegisters);
            final Set<InterferenceGraph.Node> kNeighbors2 = node2.getAdjacentKNodes(numRegisters);

            final Set<InterferenceGraph.Node> allKNeighbors = new HashSet<>();
            allKNeighbors.addAll(kNeighbors1);
            allKNeighbors.addAll(kNeighbors2);

            if (allKNeighbors.size() < numRegisters
                    || node1.getAdjacentNodes().containsAll(kNeighbors2)
                    || node2.getAdjacentNodes().containsAll(kNeighbors1)) {
                moveInstructions.remove(move);
                interference.merge(node1, node2);

                final Temp newTemp = new Temp();
                node1.getTemporaries().forEach(t -> mergedTemps.put(t, newTemp));
                node2.getTemporaries().forEach(t -> mergedTemps.put(t, newTemp));

                moveInstructionsToRemove.add(move);
                return true;
            }
        }

        return false;
    }

    private boolean freeze() {
        InterferenceGraph.Node nodeToFreeze = null;
        for (InterferenceGraph.Node node : interference.getNodes()) {
            if (node.getAdjacentNodes().size() < numRegisters && node.isMoveRelated() && node.getRegister() == null) {
                nodeToFreeze = node;
                break;
            }
        }

        if (nodeToFreeze != null) {
            nodeToFreeze.setMoveRelated(false);
            return true;
        } else {
            return false;
        }
    }

    private boolean spill() {
        InterferenceGraph.Node maxNumAdjacentNodesNode = findNodeToSpill();

        if (maxNumAdjacentNodesNode != null) {
            interference.removeNode(maxNumAdjacentNodesNode.getTemporaries());
            removedTemps.add(maxNumAdjacentNodesNode);
            return true;
        } else {
            return false;
        }
    }

    private InterferenceGraph.Node findNodeToSpill() {
        int maxNumAdjacentNodes = numRegisters - 1;
        InterferenceGraph.Node maxNumAdjacentNodesNode = null;

        for (InterferenceGraph.Node node : interference.getNodes()) {
            final int numAdjacentNodes = node.getAdjacentNodes().size();

            if (numAdjacentNodes > maxNumAdjacentNodes && node.getRegister() == null) {
                maxNumAdjacentNodes = numAdjacentNodes;
                maxNumAdjacentNodesNode = node;
            }
        }
        return maxNumAdjacentNodesNode;
    }

    private void select() {
        while (!removedTemps.empty()) {
            selectNode(removedTemps.pop());
        }
    }

    private void selectNode(InterferenceGraph.Node node) {
        final List<Temp> allowedColors = findAllowedColors(node);

        if (!allowedColors.isEmpty()) {
            final Temp chosenColor = allowedColors.contains(Registers.eax.reg) ? Registers.eax.reg : allowedColors.get(0);
            colorizeNodeAndNoticeForRenaming(node, chosenColor);
        } else {
            noticeNodeForActualSpilling(node);
        }

        interference.insertNode(node);
    }

    private List<Temp> findAllowedColors(InterferenceGraph.Node node) {
        final List<Temp> usedColors = node.getAdjacentRegisters();

        final List<Temp> possibleRegisters = new ArrayList<>(registers);
        possibleRegisters.removeAll(usedColors);
        return possibleRegisters;
    }

    private void colorizeNodeAndNoticeForRenaming(InterferenceGraph.Node node, Temp color) {
        node.getTemporaries().forEach(t -> renamedTemps.put(mergedTemps.getOrDefault(t, t), color));
        node.setRegister(color);
    }

    private void noticeNodeForActualSpilling(InterferenceGraph.Node node) {
        final Set<Temp> ts = node.getTemporaries().stream().map(t -> mergedTemps.getOrDefault(t, t)).collect(Collectors.toSet());
        assert (ts.size() == 1);

        spilledTemps.addAll(ts);
        remainingUncoloredNodes = true;
    }

    public boolean hasRemainingUncoloredNodes() {
        return remainingUncoloredNodes;
    }

    public Set<Temp> getSpilledTemps() {
        return Collections.unmodifiableSet(spilledTemps);
    }

    public Map<Temp, Temp> getRenamedTemps() {
        return Collections.unmodifiableMap(renamedTemps);
    }

    public List<MachineInstruction> getMoveInstructionsToRemove() {
        return Collections.unmodifiableList(moveInstructionsToRemove);
    }

    public Map<Temp, Temp> getMergedTemps() {
        return mergedTemps;
    }
}
