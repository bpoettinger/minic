package ast;

import java_cup.runtime.ComplexSymbolFactory;

public abstract class Type extends Node {

  public Type(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
  }

  @Override
  public abstract String toString();

  public abstract <A> A accept(TypeVisitor<A> v);
}




