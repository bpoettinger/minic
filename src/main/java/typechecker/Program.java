package typechecker;

public class Program extends Scope {

	public Program() {
		super(null);
	}

	@Override
	public void addMethod(String name, Method method) {
		throw new IllegalStateException("Adding methods is not allowed in " + this.getClass().getSimpleName());
	}

	@Override
	public void addVariable(String name, Variable variable) {
		throw new IllegalStateException("Adding variables is not allowed in " + this.getClass().getSimpleName());
	}
}
