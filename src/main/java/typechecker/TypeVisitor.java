package typechecker;

import ast.*;

/**
 * Created by bernhard on 13.11.17.
 */
public class TypeVisitor implements ast.TypeVisitor<Type> {

    private Scope scope;
    private final ErrorHandler errorHandler;

    public TypeVisitor(Scope scope, ErrorHandler errorHandler) {
        this.scope = scope;
        this.errorHandler = errorHandler;
    }

    @Override
    public Type visit(TypeVoid t) {
        return Type.VOID;
    }

    @Override
    public Type visit(TypeBoolean t) {
        return Type.BOOLEAN;
    }

    @Override
    public Type visit(TypeInt t) {
        return Type.INT;
    }

    @Override
    public Type visit(TypeClass t) {
        if (scope.hasClassGlobally(t.getName())) {
            return Type.class_(scope.getClass(t.getName()));
        } else {
            errorHandler.onUnknownEntity("Unknown class", t);
            return Type.VOID;
        }
    }

    @Override
    public Type visit(TypeArray t) {
        final Type innerType = t.getEntryType().accept(this);
        return Type.array(innerType);
    }
}
