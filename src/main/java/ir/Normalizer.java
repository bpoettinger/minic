package ir;

import ir.block.Block;
import ir.block.Blocker;
import ir.block.Blocks;
import ir.block.Tracer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


class Concatenator {

    private final List<Block> blocks;
    private final List<ir.Stm> stms = new ArrayList<>();
    private Block prevBlock = null;

    private Concatenator(List<Block> blocks) {
        this.blocks = blocks;
    }

    public static List<ir.Stm> concat(List<Block> blocks) {
        final Concatenator concatenator = new Concatenator(blocks);
        concatenator.concatBlocks();
        return concatenator.getStms();
    }

    public List<ir.Stm> getStms() {
        return stms;
    }

    public void concatBlocks() {
        for (Block block : blocks) {
            appendBlock(block);
            prevBlock = block;
        }
    }

    private void appendBlock(Block block) {
        if (isTrivialJump(block)) {
            stms.remove(stms.size() - 1);
            stms.addAll(block.getStatements());
        } else {
            stms.addAll(block.getStatements());
        }
    }

    private boolean isTrivialJump(Block block) {
        if (prevBlock == null) {
            return false;
        }

        final ir.Stm jump = prevBlock.getJump();
        final ir.Label label = block.getLabel();
        if (jump instanceof ir.StmJump) {
            final ir.Exp dest = ((StmJump) jump).getDst();
            if (dest instanceof ir.ExpName) {
                final ir.Label destLabel = ((ExpName) dest).getLabel();
                if (label.equals(destLabel)) {
                    return true;
                }
            }
        }
        return false;
    }
}


public class Normalizer {

    public static ir.Prg normalize(ir.Prg program) {
        return new ir.Prg(normalizeFunctions(program.getFunctions()), program.getVTables());
    }

    public static List<ir.Function> normalizeFunctions(List<ir.Function> functions) {
        return functions.stream()
                .map(Normalizer::normalizeFunction)
                .collect(Collectors.toList());
    }

    public static ir.Function normalizeFunction(ir.Function function) {
        final Function canonizedFunction = new ir.canon.Canonizer().canonFunction(function);
        final Blocks blocks = Blocker.blockFunction(canonizedFunction);
        final List<Block> tracedBlocks = Tracer.trace(blocks.getBlocks());
        final List<Stm> stms = Concatenator.concat(tracedBlocks);

        if (blocks.getEndLabel().isPresent()) {
            stms.add(new ir.StmLabel(blocks.getEndLabel().get()));
        }

        return new ir.Function(function.getName(), function.getNumberOfParameters(), stms, function.getReturnTemp());
    }
}
