package ast;

import java_cup.runtime.ComplexSymbolFactory;

/**
 * Represents {@code false}.
 */
public class ExpFalse extends Exp {

  public ExpFalse(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
  }

  @Override
  public <A, T extends Throwable> A accept(ExpVisitor<A, T> v) throws T {
    return v.visit(this);
  }
}
