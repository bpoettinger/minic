package asm.i386;

import ir.Label;
import ir.Temp;
import util.Pair;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

final class InstrLabel implements I386Instruction {

    private final Label label;

    InstrLabel(Label label) {
        this.label = label;
    }

    @Override
    public Collection<Temp> use() {
        return Collections.emptyList();
    }

    @Override
    public Collection<Temp> def() {
        return Collections.emptyList();
    }

    @Override
    public Collection<Label> jumps() {
        return Collections.emptyList();
    }

    @Override
    public boolean isFallThrough() {
        return true;
    }

    @Override
    public Pair<Temp, Temp> isMoveBetweenTemps() {
        return null;
    }

    @Override
    public Label isLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label + ":\n";
    }

    @Override
    public void rename(Function<Temp, Temp> sigma) {

    }

    @Override
    public List<Operand.Reg> getSpillableOperands() {
        return Collections.emptyList();
    }

    @Override
    public void replaceSpillableOperand(Operand original, Operand replacement) {

    }
}
