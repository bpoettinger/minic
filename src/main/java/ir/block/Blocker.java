package ir.block;
import java.util.List;

public class Blocker {

    public static Blocks blockFunction(ir.Function function) {
        final BlockAutomaton blockAutomaton = new BlockAutomaton();
        final BlockerStmVisitor stmVisitor = new BlockerStmVisitor(blockAutomaton);
        for (ir.Stm stm : function) {
            stm.accept(stmVisitor);
        }

        return blockAutomaton.finish();
    }
}
