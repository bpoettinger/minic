package typechecker;
import ast.TypeClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by bernhard on 06.11.17.
 */
public class RootScopeBuilder {

    private final ErrorHandler errorHandler;

    public RootScopeBuilder(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public Program buildRootScope(ast.Program program) {
        final Program scope = new Program();

        createBuiltinTypes(scope);
        buildMainScope(program.mainClass, scope);
        buildClassesScopes(program.classes, scope);

        return scope;
    }

    public void createBuiltinTypes(Scope scope) {
        scope.addClass("int", new Class("int", null));
        scope.addClass("boolean", new Class("boolean", null));
        scope.addClass("int[]", new Class("int[]", null));
        scope.addClass("Object", new Class("Object", null));
    }

    public void buildMainScope(ast.MainClassDeclaration main, Scope scope) {
        final Class type = new Class(main.getClassName(), scope);
        type.addMethod("main",
                new Method(
                        Type.VOID,
                        new ArrayList<>(),
                        main.throwsIOException()));

        scope.addClass(main.getClassName(), type);
    }

    public void buildClassesScopes(List<ast.ClassDeclaration> classes, Scope scope) {
        for (ast.ClassDeclaration class_ : classes) {
            try {
                scope.addClass(class_.getClassName(), new Class(class_.getClassName(), scope));
            } catch (TypeError e) {
                errorHandler.onNameClash(class_, e.getMessage());
            }
        }

        for (ast.ClassDeclaration class_ : classes) {
            buildClassScope(class_, scope);
        }
    }

    private Type typeFromAst(ast.Type astType, Scope scope) {
        final TypeVisitor visitor = new TypeVisitor(scope, errorHandler);
        return astType.accept(visitor);
    }

    public void buildClassScope(ast.ClassDeclaration class_, Scope scope) {
        if (!scope.hasClassGlobally(class_.getClassName())) {
            errorHandler.onUnknownEntity("Unknown class", class_);
        }
        final Class type = scope.getClass(class_.getClassName());

        assert(!type.hasVariableLocally("this"));
        type.addVariable("this", new Variable(Type.class_(type)));

        for (ast.FieldDeclaration field : class_.getFields()) {
            if (!type.hasVariableLocally(field.getName())) {
                type.addVariable(field.getName(), new Variable(typeFromAst(field.getType(), scope)));
            } else {
                errorHandler.onNameClash(class_, "Attribute already exists");
            }
        }
        for (ast.MethodDeclaration method : class_.getMethods()) {
            final List<ast.Type> parameterTypes =
                    method.getParameters().stream().map(p -> p.getType()).collect(Collectors.toList());
            if (!type.hasMethodLocally(method.getMethodName())) {
                type.addMethod(
                        method.getMethodName(),
                        new Method(
                                typeFromAst(method.getReturnType(), scope),
                                parameterTypes.stream().map(t -> typeFromAst(t, scope)).collect(Collectors.toList()),
                                method.throwsIOException()));
            } else {
                errorHandler.onNameClash(method, "Method already exists");
            }
        }
    }
}
