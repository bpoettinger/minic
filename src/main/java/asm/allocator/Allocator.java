package asm.allocator;

import asm.MachineFunction;
import asm.MachineInstruction;
import ir.Temp;
import util.Pair;

import java.util.*;

public class Allocator {

    private final MachineFunction function;
    private final Set<Temp> registers;
    private final Set<Temp> allRegisters;

    public Allocator(MachineFunction function, Set<Temp> registers, Set<Temp> allRegisters) {
        this.function = function;
        this.registers = registers;
        this.allRegisters = allRegisters;
    }

    public void allocate() {
        while (true) {
            final ControlFlowGraph<MachineInstruction> controlFlowGraph = buildControlFlowGraph();
            final Map<MachineInstruction, ActivitySet> activitySets = calculateActivitySets(controlFlowGraph);
            final InterferenceGraph interferenceGraph = calculateInterferenceGraph(activitySets);
            final List<MachineInstruction> moveInstructions = findMoveInstructions();

            final Colorizer colorizer = new Colorizer(registers, interferenceGraph, moveInstructions);
            colorizer.colorize();

            final List<MachineInstruction> moveInstructionsToRemove = colorizer.getMoveInstructionsToRemove();
            final Map<Temp, Temp> mergedTemps = colorizer.getMergedTemps();
            function.removeInstructions(moveInstructionsToRemove);
            renameRegisters(mergedTemps);

            if (colorizer.hasRemainingUncoloredNodes()) {
                final List<Temp> spilledTemps = new ArrayList<>(colorizer.getSpilledTemps());
                spillTemps(spilledTemps);
            } else {
                final Map<Temp, Temp> renamedTemps = colorizer.getRenamedTemps();
                renameRegisters(renamedTemps);
                break;
            }
        }
    }

    private ControlFlowGraph<MachineInstruction> buildControlFlowGraph() {
        final ControlFlowAnalyzer controlFlowAnalyzer = new ControlFlowAnalyzer(function);
        controlFlowAnalyzer.build();
        return controlFlowAnalyzer.getControlFlowGraph();
    }

    private Map<MachineInstruction, ActivitySet> calculateActivitySets(ControlFlowGraph<MachineInstruction> controlFlowGraph) {
        final LivenessAnalyzer livenessAnalyzer = new LivenessAnalyzer(controlFlowGraph);
        livenessAnalyzer.calculateActivitySets();
        return livenessAnalyzer.getActivitySets();
    }

    private InterferenceGraph calculateInterferenceGraph(Map<MachineInstruction, ActivitySet> activitySets) {
        final InterferenceAnalyzer interferenceAnalyzer = new InterferenceAnalyzer(activitySets, registers, allRegisters);
        interferenceAnalyzer.buildInterferenceGraph();
        return interferenceAnalyzer.getInterferenceGraph();
    }

    private List<MachineInstruction> findMoveInstructions() {
        final List<MachineInstruction> moveInstructions = new ArrayList<>();
        for (MachineInstruction instruction : function) {
            final Pair<Temp, Temp> x = instruction.isMoveBetweenTemps();
            if (x != null && !allRegisters.contains(x.fst) && !allRegisters.contains(x.snd)) {
                moveInstructions.add(instruction);
            }
        }
        return moveInstructions;
    }

    private void spillTemps(List<Temp> temps) {
        function.spill(temps);
    }

    private void renameRegisters(Map<Temp, Temp> allocatedTemps) {
        function.rename(x -> allocatedTemps.getOrDefault(x, x));
    }
}
