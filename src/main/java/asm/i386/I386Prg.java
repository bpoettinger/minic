package asm.i386;

import asm.MachineInstruction;
import asm.MachineFunction;
import asm.MachinePrg;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class I386Prg implements MachinePrg {

    private final List<I386Function> functions;
    private final List<I386Data> data;

    I386Prg(List<I386Function> functions, List<I386Data> data) {
        this.functions = functions;
        this.data = data;
    }

    @Override
    public String renderAssembly() {
        StringBuilder s = new StringBuilder();
        s.append("\t.intel_syntax noprefix\n");
        
        if(System.getProperty("os.name").contains("Mac")) {
        	s.append("\t.global _Lmain\n");
        } else {
        	s.append("\t.global Lmain\n");
        }
        
        s.append(".data\n");
        for (I386Data item : data) {
            renderData(item, s);
        }
        s.append(".text\n");
        for (I386Function m : functions) {
            renderFunction(m, s);
        }
        return s.toString();
    }

    private void renderData(I386Data item, StringBuilder s) {
        s.append(item.getName());
        s.append(": ");
        s.append(".long ");
        s.append(item.getWords().stream().map(Object::toString).collect(Collectors.joining(", ")));
        s.append("\n");
    }

    private void renderFunction(I386Function function, StringBuilder s) {
        s.append(function.getName());
        s.append(":\n");
        addFunctionPrologue(function, s);
        for (MachineInstruction i : function) {
            if (!(i instanceof InstrNullary && ((InstrNullary)i).getKind() == InstrNullary.Kind.RET)) {
                s.append(i);
            }
        }
        addFunctionEpilogue(function, s);
    }

    private void addFunctionPrologue(I386Function function, StringBuilder s) {
        s.append(new InstrUnary(InstrUnary.Kind.PUSH, Registers.ebp));
        s.append(new InstrBinary(InstrBinary.Kind.MOV, Registers.ebp, Registers.esp));

        int size = (15 + function.getFrame().getSize()) & ~0xf;
        if (size != 0) {
            s.append(new InstrBinary(InstrBinary.Kind.SUB, Registers.esp, new Operand.Imm(size)));
        }
    }

    private void addFunctionEpilogue(I386Function function, StringBuilder s) {
    	int size = (15 + function.getFrame().getSize()) & ~0xf;
        if (size != 0) {
            s.append(new InstrBinary(InstrBinary.Kind.ADD, Registers.esp, new Operand.Imm(size)));
        }
        s.append(new InstrUnary(InstrUnary.Kind.POP, Registers.ebp));
        s.append(new InstrNullary(InstrNullary.Kind.RET));
    }

    @Override
    public Iterator<MachineFunction> iterator() {
        return new Iterator<MachineFunction>() {
            final Iterator<I386Function> iterator = functions.iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public MachineFunction next() {
                return iterator.next();
            }
        };
    }
}

