package ir;

public class ExpName extends Exp {

  private final Label label;

  public ExpName(Label label) {
    if (label == null) {
      throw new NullPointerException();
    }
    this.label = label;
  }

  public Label getLabel() {
    return label;
  }

  @Override
  public Type getType() {
    return Type.INTEGER;
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "NAME(" + getLabel() + ")";
  }

}
