package ir.block;

import ir.Label;

import java.util.List;
import java.util.Optional;

public class Blocks {

    private final List<Block> blocks;
    private final Optional<Label> endLabel;

    public Blocks(List<Block> blocks, Optional<Label> endLabel) {
        this.blocks = blocks;
        this.endLabel = endLabel;
    }


    public List<Block> getBlocks() {
        return blocks;
    }

    public Optional<Label> getEndLabel() {
        return endLabel;
    }
}
