package ir.block;

import ir.Label;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


class BlockBuilder {

    private List<Block> blocks = new ArrayList<>();
    private Block currentBlock = new Block();

    public void addStatement(ir.Stm stm) {
        currentBlock.addStatement(stm);
    }

    public void startNewBlock() {
        if (currentBlock.getStatements().size() > 0) {
            blocks.add(currentBlock);
        }
        currentBlock = new Block();
    }

    public List<Block> getBlocks() {
        return blocks;
    }
}


class BlockAutomaton {

    private enum State {
        NIX,
        LABEL,
        JUMP
    }

    private BlockBuilder builder = new BlockBuilder();
    private State currentState = State.JUMP;

    public void onNix(ir.Stm nix) {
        switch (currentState) {
            case NIX:
            case LABEL:
                break;
            case JUMP:
                builder.startNewBlock();
                builder.addStatement(new ir.StmLabel(new ir.Label()));
                break;
        }
        builder.addStatement(nix);
        currentState = State.NIX;
    }

    public void onJump(ir.Stm stm) {
        switch (currentState) {
            case NIX:
            case LABEL:
                break;
            case JUMP:
                builder.startNewBlock();
                builder.addStatement(new ir.StmLabel(new ir.Label()));
                break;
        }

        builder.addStatement(stm);
        currentState = State.JUMP;
    }

    public void onLabel(ir.StmLabel label) {
        switch (currentState) {
            case NIX:
            case LABEL:
                builder.addStatement(new ir.StmJump(label.getLabel()));
                break;
            case JUMP:
                break;
        }

        builder.startNewBlock();
        builder.addStatement(label);
        currentState = State.LABEL;
    }

    public Blocks finish() {
        Optional<Label> endLabel = Optional.empty();

        switch (currentState) {
            case NIX:
            case LABEL: {
                endLabel = Optional.of(new ir.Label());
                builder.addStatement(new ir.StmJump(endLabel.get()));
                break;
            }
            case JUMP:
                break;
        }

        builder.startNewBlock();
        final List<Block> resultBlocks = builder.getBlocks();
        builder = new BlockBuilder();
        currentState = State.JUMP;
        return new Blocks(resultBlocks, endLabel);
    }
}


public class BlockerStmVisitor implements ir.StmVisitor<Void> {
    private final BlockAutomaton automaton;

    public BlockerStmVisitor(BlockAutomaton automaton) {
        this.automaton = automaton;
    }

    @Override
    public Void visit(ir.StmMove stmMOVE) {
        automaton.onNix(stmMOVE);
        return null;
    }

    @Override
    public Void visit(ir.StmJump stmJUMP) {
        automaton.onJump(stmJUMP);
        return null;
    }

    @Override
    public Void visit(ir.StmCJump stmCJUMP) {
        automaton.onJump(stmCJUMP);
        return null;
    }

    @Override
    public Void visit(ir.StmSeq stmSEQ) {
        assert(false);
        return null;
    }

    @Override
    public Void visit(ir.StmLabel stmLABEL) {
        automaton.onLabel(stmLABEL);
        return null;
    }
}
