package ir.eval;

import ir.Function;
import ir.Prg;
import ir.Stm;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Eval {

    public static Prg eval(Prg prg) {
        final List<Function> functions = prg.getFunctions().stream().map(Eval::evalFunction).collect(Collectors.toList());
        return new Prg(functions, prg.getVTables());
    }

    private static Function evalFunction(Function function) {
        final StatementVisitor statementVisitor = new StatementVisitor();
        final List<Stm> body = new ArrayList<>();
        function.iterator().forEachRemaining(s -> body.add(s.accept(statementVisitor)));
        return new Function(function.getName(), function.getNumberOfParameters(), body, function.getReturnTemp());
    }
}
