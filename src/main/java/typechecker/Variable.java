package typechecker;

public class Variable extends Entity {

	private final Type type;

	public Variable(Type type) {
		this.type = type;
	}
	
	public Type getType(){
		return type;
	}
}
