package asm.allocator;

import java.util.*;

import asm.MachineInstruction;
import ir.Temp;

public class LivenessAnalyzer {
	
	private final ControlFlowGraph<MachineInstruction> controlFlowGraph;
	private final List<ActivitySet> activitySets = new ArrayList<>();

	public LivenessAnalyzer(ControlFlowGraph<MachineInstruction> controlFlowGraph) {
		this.controlFlowGraph = controlFlowGraph;
	}

	public void calculateActivitySets() {
		initializeActivitySets();
		iterateUntilFixpoint();
	}

	private void initializeActivitySets() {
		final List<ControlFlowGraph.Node<MachineInstruction>> nodes = controlFlowGraph.getNodes();

		for (int i = nodes.size() - 1; i >= 0; --i) {
			activitySets.add(new ActivitySet());
		}
	}

	private void iterateUntilFixpoint() {
		final List<ControlFlowGraph.Node<MachineInstruction>> nodes = controlFlowGraph.getNodes();

		for (int i = 0; i < nodes.size(); ++i) {
			final ControlFlowGraph.Node<MachineInstruction> node = nodes.get(i);
			final ActivitySet activitySet = activitySets.get(i);

			// in: use[n]
			activitySet.in.addAll(node.node.use());
		}

		for (boolean changed = true; changed; ) {
			changed = false;

			for (int i = nodes.size() - 1; i >= 0; --i) {
				final ControlFlowGraph.Node<MachineInstruction> node = nodes.get(i);
				final ActivitySet activitySet = activitySets.get(i);

				// out: Vereinigungsmenge aller successors s in[s]
				for (int successor : node.edges) {
					final ActivitySet successorActivitySet = activitySets.get(successor);
					final boolean localChanged = activitySet.out.addAll(successorActivitySet.in);

					if (localChanged) {
						// in: (out[n] \ def[n])
						final Collection<Temp> def = node.node.def();
						for (Temp temp : successorActivitySet.in) {
							if (!def.contains(temp)) {
								activitySet.in.add(temp);
							}
						}
						changed = true;
					}
				}
			}
		}
	}

	public Map<MachineInstruction, ActivitySet> getActivitySets() {
		final List<ControlFlowGraph.Node<MachineInstruction>> nodes = controlFlowGraph.getNodes();

		final Map<MachineInstruction, ActivitySet> result = new HashMap<>();
		for (int i = 0; i < activitySets.size(); ++i) {
			result.put(nodes.get(i).node, activitySets.get(i));
		}
		return result;
	}
}
