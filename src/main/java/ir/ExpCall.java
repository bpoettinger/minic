package ir;

import java.util.Arrays;
import java.util.List;

public class ExpCall extends Exp {

  private final Exp func;
  private final List<Exp> args;
  private final Type returnType;

  public ExpCall(Exp func, List<Exp> args, Type returnType) {
    this.returnType = returnType;
    if (func == null || args == null) {
      throw new NullPointerException();
    }
    this.func = func;
    this.args = args;
  }

  public Exp getFunction() {
    return func;
  }

  public List<Exp> getArguments() {
    return args;
  }

  @Override
  public Type getType() {
    return returnType;
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder("CALL(" + getFunction());
    for (Exp arg : getArguments()) {
      s.append(", ").append(arg);
    }
    s.append(")");
    return s.toString();
  }

  public static Exp name(String name, Type returnType, Exp... args) {
    return new ExpCall(new ExpName(new Label(name)), Arrays.asList(args), returnType);
  }
}
