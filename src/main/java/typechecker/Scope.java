package typechecker;

import java.util.HashMap;
import java.util.Map;

public class Scope extends Entity {

	protected final Scope parentScope;

	protected final Map<String, Class> types = new HashMap<>();
	protected final Map<String, Method> methods = new HashMap<>();
	protected final Map<String, Variable> variables = new HashMap<>();

	public Scope() {
		this(null);
	}

	public Scope(Scope parentScope) {
		this.parentScope = parentScope;
	}

	public Scope getParentScope() {
		return parentScope;
	}
	
	public final Class getClass(String name) {
		Class type = types.get(name);
		if(type == null) {
			if (parentScope != null) {
				type = parentScope.getClass(name);
			} else {
				throw new TypeError("Unknown type " + name);
			}
		}
		return type;
	}

	public void addClass(String name, Class type) {
		if (types.containsKey(name)) {
			throw new TypeError("Key '" + name + "' is already mapped");
		}
		types.put(name, type);
	}

	public Method getMethod(String name) {
		Method method = methods.get(name);
		if(method == null) {
			if (parentScope != null) {
				method = parentScope.getMethod(name);
			} else {
				throw new TypeError(String.format("Unkown method %s", name));
			}
		}
		return method;
	}

	public void addMethod(String name, Method method) {
		if (methods.containsKey(name)) {
			throw new TypeError("Key '" + name + "' is already mapped");
		}
		methods.put(name, method);
	}

	public Variable getVariable(String name) {
		Variable variable = variables.get(name);
		if(variable == null) {
			if (parentScope != null) {
				variable = parentScope.getVariable(name);
			} else {
				throw new TypeError(String.format("Unknown variable %s", name));
			}
		}
		return variable;
	}

	public void addVariable(String name, Variable variable) {
		if (variables.containsKey(name)) {
			throw new TypeError("Key '" + name + "' is already mapped");
		}
		variables.put(name, variable);
	}

	public boolean hasVariableLocally(String name) {
		return variables.containsKey(name);
	}

	public boolean hasVariableGlobally(String name) {
		return hasVariableLocally(name) || (parentScope != null && parentScope.hasVariableGlobally(name));
	}

	public boolean hasMethodLocally(String name) {
		return methods.containsKey(name);
	}

	public boolean hasMethodGlobally(String name) {
		return hasMethodLocally(name) || (parentScope != null && parentScope.hasMethodGlobally(name));
	}

	public boolean hasClassLocally(String name) {
		return types.containsKey(name);
	}

	public boolean hasClassGlobally(String name) {
		return hasClassLocally(name) || (parentScope != null && parentScope.hasClassGlobally(name));
	}
}
