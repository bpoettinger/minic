package asm.allocator;

import ir.Temp;
import java.util.*;
import java.util.stream.Collectors;

public class InterferenceGraph {

    public static class Node {

        private final Set<Temp> temporaries;

        private Temp register;
        private final Set<Node> adjacentNodes = new HashSet<>(16);
        private boolean isMoveRelated = false;
        private Node(Set<Temp> temporaries, Temp register) {
            this.temporaries = temporaries;
            this.register = register;
        }

        public Set<Temp> getTemporaries() {
            return temporaries;
        }

        public Temp getRegister() {
            return register;
        }

        public void setRegister(Temp newRegister) {
            register = newRegister;
        }

        public Set<Node> getAdjacentNodes() {
            return Collections.unmodifiableSet(adjacentNodes);
        }

        public Set<Node> getAdjacentKNodes(int k) {
            return adjacentNodes.stream().filter(node -> node.getAdjacentNodes().size() >= k).collect(Collectors.toSet());
        }

        public List<Temp> getAdjacentRegisters() {
            return adjacentNodes.stream()
                    .map(Node::getRegister)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        public void setMoveRelated(boolean isMoveRelated) {
            this.isMoveRelated = isMoveRelated;
        }

        public boolean isMoveRelated() {
            return isMoveRelated;
        }

        public boolean interferes(Node that) {
            return adjacentNodes.contains(that);
        }
    }

    private final Map<Temp, Node> nodesByTemporary;
    private final Set<Node> removedNodes = new HashSet<>();

    public InterferenceGraph(int sizeHint) {
        nodesByTemporary = new HashMap<>(sizeHint);
    }

    public void addNode(Temp temp, Temp register) {
        final Set<Temp> temporaries = new HashSet<>();
        temporaries.add(temp);
        nodesByTemporary.put(temp, new Node(temporaries, register));
    }

    public void addEdge(Temp temp1, Temp temp2) {
        if (temp1 != temp2) {
            final Node node1 = nodesByTemporary.get(temp1);
            final Node node2 = nodesByTemporary.get(temp2);

            node1.adjacentNodes.add(node2);
            node2.adjacentNodes.add(node1);
        }
    }

    public void removeEdge(Temp temp1, Temp temp2) {
        final Node node1 = nodesByTemporary.get(temp1);
        final Node node2 = nodesByTemporary.get(temp2);

        node1.adjacentNodes.remove(node2);
        node2.adjacentNodes.remove(node1);
    }

    public Node removeNode(Temp temp) {
        return removeNode(Collections.singleton(temp));
    }

    public Node removeNode(Set<Temp> temporaries) {
        final Node node = nodesByTemporary.get(temporaries.iterator().next());
        if (!node.getTemporaries().containsAll(temporaries)) {
            throw new RuntimeException();
        }

        removeNode(node);
        removedNodes.add(node);
        return node;
    }

    private void removeNode(Node node) {
        for (Node adjacentNode : node.adjacentNodes) {
            adjacentNode.adjacentNodes.remove(node);
        }

        for (Temp t : node.temporaries) {
            nodesByTemporary.remove(t);
        }

        removedNodes.add(node);
    }

    public void insertNode(Node node) {
        for (Node adjacentNode : node.adjacentNodes) {
            adjacentNode.adjacentNodes.add(node);
        }

        for (Temp temp : node.temporaries) {
            nodesByTemporary.put(temp, node);
        }

        removedNodes.remove(node);
    }

    public Node getNode(Temp temp) {
        return nodesByTemporary.get(temp);
    }

    public void deleteNode(Temp temp) {
        removeNode(temp);
    }

    public Iterable<Node> getNodes() {
        return nodesByTemporary.values();
    }

    public Iterable<Node> getMoveRelatedNodes() {
        return nodesByTemporary.values().stream().filter(Node::isMoveRelated).collect(Collectors.toList());
    }

    public Iterable<Node> getNonMoveRelatedNodes() {
        return nodesByTemporary.values().stream().filter(node -> !node.isMoveRelated()).collect(Collectors.toList());
    }

    public void merge(Node node1, Node node2) {
        node1.temporaries.addAll(node2.temporaries);
        node1.register = node1.register != null ? node1.register : node2.register;
        node1.adjacentNodes.addAll(node2.adjacentNodes);
        node1.isMoveRelated = false;

        for (Node adjacentNode : node2.adjacentNodes) {
            adjacentNode.adjacentNodes.remove(node2);
            adjacentNode.adjacentNodes.add(node1);
        }

        removedNodes.forEach(removed -> {
            if (removed.adjacentNodes.contains(node2)) {
                removed.adjacentNodes.remove(node2);
                removed.adjacentNodes.add(node1);
            }
        });

        for (Temp temp : node2.temporaries) {
            nodesByTemporary.put(temp, node1);
        }
    }
}
