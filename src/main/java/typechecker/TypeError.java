package typechecker;

/**
 * Created by bernhard on 06.11.17.
 */
public class TypeError extends RuntimeException {

    public TypeError(String message) {
        super(message);
    }
}
