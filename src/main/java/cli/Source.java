package cli;
import java_cup.runtime.ComplexSymbolFactory;

public interface Source {

    String get(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right);
    String getText();
    String getPath();
}