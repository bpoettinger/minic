package ir;

public class StmLabel extends Stm {

  private final Label label;

  public StmLabel(Label label) {
    if (label == null) {
      throw new NullPointerException();
    }
    this.label = label;
  }

  public Label getLabel() {
    return label;
  }

  @Override
  public <A> A accept(StmVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "LABEL(" + getLabel() + ")";
  }
}
