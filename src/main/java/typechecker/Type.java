package typechecker;

/**
 * Created by bernhard on 13.11.17.
 */
public abstract class Type {

    public static Type INT = new SimpleType("int");
    public static Type BOOLEAN = new SimpleType("boolean");
    public static Type VOID = new SimpleType("void");
    public static Type STRING = new SimpleType("String");

    public static Type array(Type innerType) {
        return new ArrayType(innerType);
    }

    public static Type class_(Class class_) {
        return new ClassType(class_);
    }

    public static class SimpleType extends Type {

        private String name;

        private SimpleType(String name) {
            this.name = name;
        }

        public String toString() {
            return name;
        }

        @Override
        public boolean isSubTypeOf(Type that) {
            return that instanceof SimpleType && this.name.equals(((SimpleType) that).name);
        }
    }

    public static class ArrayType extends Type {

        private Type innerType;

        private ArrayType(Type innerType) {
            this.innerType = innerType;
        }

        public Type getInnerType() {
            return innerType;
        }

        public boolean equals(Object that) {
            return that instanceof ArrayType && innerType.equals(((ArrayType) that).innerType);
        }

        public String toString() {
            return innerType.toString() + "[]";
        }

        @Override
        public boolean isSubTypeOf(Type that) {
            return that instanceof ArrayType && this.innerType.equals(((ArrayType) that).getInnerType());
        }
    }

    public static class ClassType extends Type {

        private Class class_;

        private ClassType(Class class_) {
            this.class_ = class_;
        }

        public Class getClass_() {
            return class_;
        }

        public boolean equals(Object that) {
            return that instanceof ClassType && class_.equals(((ClassType) that).class_);
        }

        public String toString() {
            return class_.getName();
        }

        @Override
        public boolean isSubTypeOf(Type that) {
            return that instanceof ClassType && class_.isDerivedFrom(((ClassType) that).class_);
        }
    }

    public abstract boolean isSubTypeOf(Type that);
}
