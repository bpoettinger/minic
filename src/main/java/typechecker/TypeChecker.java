package typechecker;

import ast.ClassDeclaration;
import ast.MethodDeclaration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by bernhard on 06.11.17.
 */
public class TypeChecker {
    
    private final ErrorHandler errorHandler;
    
    public TypeChecker(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void check(ast.Program program, Scope rootScope) {
        resolveAndCheckSuperClasses(program.classes, rootScope);
        checkMain(program.mainClass, rootScope);
        checkClasses(program.classes, rootScope);
    }

    public void resolveAndCheckSuperClasses(List<ClassDeclaration> classes, Scope rootScope) {
        final Map<String, ClassDeclaration> classDeclarations = new HashMap<>();
        classes.forEach(decl -> classDeclarations.put(decl.getClassName(), decl));

        for (ast.ClassDeclaration class_ : classes) {
            resolveAndCheckSuperClass(class_, classDeclarations, rootScope);
        }
    }

    public void resolveAndCheckSuperClass(ClassDeclaration class_, Map<String, ClassDeclaration> classDeclarations, Scope rootScope) {
        String superName = class_.getSuperName();
        if (superName == null) {
            superName = "Object";
        }

        if (rootScope.hasClassGlobally(superName)) {
            final Class superScope = rootScope.getClass(superName);
            final Class classScope = rootScope.getClass(class_.getClassName());

            classScope.setSuperClass(Optional.of(superScope));

            if (!superName.equals("Object")) {
                class_.setSuperClassDeclaration(classDeclarations.get(superName));
            }
        } else {
            errorHandler.onUnknownEntity("Unknown base class", class_);
        }
    }

    public void checkMain(ast.MainClassDeclaration main, Scope scope) {
        final ExecutionScope methodScope = new ExecutionScope(scope);
        final StatementVisitor bodyVisitor = new StatementVisitor(methodScope, errorHandler);
        main.getMainBody().accept(bodyVisitor);

        //if (bodyVisitor.throwsException() && !main.throwsIOException()) {
        //    throw new TypeError("");
        //}
    }

    public void checkClasses(List<ast.ClassDeclaration> classes, Scope scope) {
        for (ast.ClassDeclaration class_ : classes) {
            checkClass(class_, scope);
        }
    }

    public void checkClass(ast.ClassDeclaration class_, Scope scope) {
        final Class classScope = scope.getClass(class_.getClassName());
        checkMethods(class_.getMethods(), classScope);
        checkFields(class_.getFields(), classScope);
    }

    public void checkFields(List<ast.FieldDeclaration> fields, Class scope) {
        for (ast.FieldDeclaration field : fields) {
            checkField(field, scope);
        }
    }

    public void checkField(ast.FieldDeclaration field, Class scope) {
        if (!scope.hasClassGlobally(field.getType().toString())) {
            errorHandler.onUnknownEntity("Unknown class", field.getType());
        }
    }

    public void checkMethods(List<ast.MethodDeclaration> methods, Class scope) {
        for (ast.MethodDeclaration method : methods) {
            checkMethod(method, scope);
        }
    }

    public void checkMethod(ast.MethodDeclaration method, Class scope) {
        final ExecutionScope methodScope = createMethodScope(method, scope);

        final StatementVisitor bodyVisitor = new StatementVisitor(methodScope, errorHandler);
        method.getBody().accept(bodyVisitor);

        final ExpressionVisitor returnExpressionVisitor = new ExpressionVisitor(methodScope, errorHandler);
        final Type returnType = method.getReturnExp().accept(returnExpressionVisitor);

        final TypeVisitor typeVisitor = new TypeVisitor(scope, errorHandler);
        final Type expectedReturnType = method.getReturnType().accept(typeVisitor);
        if (!returnType.isSubTypeOf(expectedReturnType)) {
            errorHandler.onTypeError(returnType, expectedReturnType, "Incompatible return type", method.getReturnExp());
        }

        final boolean throwsException =
                returnExpressionVisitor.throwsException() | bodyVisitor.throwsException();
        //if (throwsException && !method.throwsIOException()) {
        //    throw new TypeError("");
        //}

        checkMethodOverride(method, scope, returnType);
    }

    private ExecutionScope createMethodScope(MethodDeclaration method, Class scope) {
        final ExecutionScope methodScope = new ExecutionScope(scope);
        for (ast.Parameter parameter : method.getParameters()) {
            final TypeVisitor typeVisitor = new TypeVisitor(scope, errorHandler);
            final Type parameterType = parameter.getType().accept(typeVisitor);
            assert(!methodScope.hasVariableInExecutionScope(parameter.getId()));
            methodScope.addVariable(parameter.getId(), new Variable(parameterType));
        }
        return methodScope;
    }

    private void checkMethodOverride(MethodDeclaration method, Class scope, Type returnType) {
        final Optional<Method> methodToOverride = scope.getMethodFromSuperClasses(method.getMethodName());
        if (methodToOverride.isPresent()) {
            final List<Type> overridenParameters = methodToOverride.get().getParameterTypes();
            if (overridenParameters.size() != method.getParameters().size()) {
                errorHandler.onNameClash(method, "Trying to overload method");
            }

            int index = 0;
            for (Type overridenParameterType : overridenParameters) {
                final ast.Parameter newAstParameter = method.getParameters().get(index);

                final TypeVisitor typeVisitor = new TypeVisitor(scope, errorHandler);
                final Type newParameterType = newAstParameter.getType().accept(typeVisitor);

                if (!overridenParameterType.isSubTypeOf(newParameterType)) {
                    errorHandler.onTypeError(newParameterType, overridenParameterType, "Incompatible type when overriding method", newAstParameter.getType());
                }

                index += 1;
            }

            if (!returnType.isSubTypeOf(methodToOverride.get().getReturnType())) {
                errorHandler.onTypeError(returnType, methodToOverride.get().getReturnType(), "Incompatible return type when overriding method", method.getReturnType());
            }
        }
    }

}
