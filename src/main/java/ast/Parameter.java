package ast;

import java_cup.runtime.ComplexSymbolFactory;

public class Parameter extends Node {

  private final String id;
  private final Type type;

  public Parameter(String id, Type type, ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
    super(left, right);
    this.id = id;
    this.type = type;
  }

  public String getId() {
    return id;
  }

  public Type getType() {
    return type;
  }
}
