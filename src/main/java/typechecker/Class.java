package typechecker;

import java.util.Optional;

public class Class extends Scope {

	private Optional<Class> superClass = Optional.empty();
	private String name;

	public Class(String name, Scope parentScope) {
		super(parentScope);
		this.name = name;
	}

	public void setSuperClass(Optional<Class> superClass) {
		this.superClass = superClass;
	}

	@Override
	public void addClass(String name, Class type) {
		throw new IllegalStateException("Adding types is not allowed in " + this.getClass().getSimpleName());
	}

	@Override
	public Method getMethod(String name) {
		Method method = methods.get(name);
		if(method == null) {
			if (superClass.isPresent()) {
				method = superClass.get().getMethod(name);
			} else if (parentScope != null) {
				method = parentScope.getMethod(name);
			} else {
				throw new TypeError("Unkown method {}".format(name));
			}
		}
		return method;
	}

	@Override
	public Variable getVariable(String name) {
		Variable variable = variables.get(name);
		if(variable == null) {
			if (superClass.isPresent()) {
				variable = superClass.get().getVariable(name);
			} else if (parentScope != null) {
				variable = parentScope.getVariable(name);
			} else {
				throw new TypeError("Unknown variable %s".format(name));
			}
		}
		return variable;
	}

	private Optional<Method> getMethodFromClasses(String name) {
		if (methods.containsKey(name)) {
			return Optional.of(methods.get(name));
		} else if (superClass.isPresent()) {
			return superClass.get().getMethodFromClasses(name);
		} else {
			return Optional.empty();
		}
	}

	public Optional<Method> getMethodFromSuperClasses(String name) {
		if (superClass.isPresent()) {
			return superClass.get().getMethodFromClasses(name);
		}
		return Optional.empty();
	}

	private boolean hasVariableInTypeScope(String name) {
		if (variables.containsKey(name)) {
			return true;
		} else {
			return hasVariableInSuperClasses(name);
		}
	}

	public boolean hasVariableInSuperClasses(String name) {
		if (superClass.isPresent()) {
			return superClass.get().hasVariableInTypeScope(name);
		}
		return false;
	}

	public String getName() {
		return name;
	}

	public boolean isDerivedFrom(Class that) {
		return (this == that)
				|| (this.superClass.isPresent() && this.superClass.get().isDerivedFrom(that));
	}

	public boolean hasVariableGlobally(String name) {
		return hasVariableInTypeScope(name)
				|| parentScope != null && parentScope.hasVariableGlobally(name);
	}

	public boolean hasMethodGlobally(String name) {
		return hasMethodLocally(name)
				|| superClass.isPresent() && superClass.get().hasMethodLocally(name)
				|| parentScope != null && parentScope.hasVariableGlobally(name);
	}
}
