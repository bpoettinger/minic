package cli;

import java_cup.runtime.ComplexSymbolFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by bernhard on 17.11.17.
 */
public class StringSource implements Source {

    private final String text;
    private final List<String> lines;
    private final String path;

    private StringSource(String text, List<String> lines, String path) {
        this.text = text;
        this.lines = lines;
        this.path = path;
    }

    public static Source create(String text, String path) {
        final String[] lines = text.split("(?<=(?:\n|\n\r|\r\n))");
        return new StringSource(text, Arrays.asList(lines), path);
    }

    @Override
    public String get(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
        final int lineFrom = left.getLine() - 1;
        final int lineTo = right.getLine() - 1;

        return String.join("", lines.subList(lineFrom, lineTo+1));
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getPath() {
        return path;
    }
}
