package ir;

public interface ExpVisitor<A> {

  A visit(ExpConst expCONST);

  A visit(ExpName expNAME);

  A visit(ExpTemp expTEMP);

  A visit(ExpParam expPARAM);

  A visit(ExpMem expMEM);

  A visit(ExpBinOp expOP);

  A visit(ExpCall expCALL);

  A visit(ExpESeq expESEQ);
}
