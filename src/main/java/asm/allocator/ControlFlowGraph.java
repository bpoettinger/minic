package asm.allocator;

import java.util.*;

public class ControlFlowGraph<T> {
	public static class Node<T> {
		public final T node;
		public final Set<Integer> edges = new HashSet<>(2);

		public Node(T node) {
			this.node = node;
		}
	}

	private final List<Node<T>> nodes;
	private final Map<T, Integer> indices;

	public ControlFlowGraph(int sizeHint) {
		nodes = new ArrayList<>(sizeHint);
		indices = new HashMap<>(sizeHint);
	}
	
	public void addNode(T node) {
		assert(node != null);
		indices.put(node, nodes.size());
		nodes.add(new Node<>(node));
	}
	
	public void addSuccessor(T node, T successor) {
		assert(node != null);
		assert(successor != null);
		nodes.get(indices.get(node)).edges.add(indices.get(successor));
	}

	public List<Node<T>> getNodes() {
		return nodes;
	}
}
