package ir.canon;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Representation of a canonized expression
 * <p>
 * A canonised expression is a pair (s,e), where s is a list of canonized statements and e is an expression.
 * </p>
 */
final class CanonizedExp {

  private final List<ir.Stm> body;
  private final ir.Exp exp;

  CanonizedExp(ir.Exp exp) {
    this.body = Collections.emptyList();
    this.exp = exp;
  }

  CanonizedExp(List<ir.Stm> body, ir.Exp exp) {
    this.body = body;
    this.exp = exp;
  }

  public List<ir.Stm> getBody() {
    return body;
  }

  public ir.Exp getExp() {
    return exp;
  }

  CanonizedExp mapExp(Function<ir.Exp, ir.Exp> f) {
    return new CanonizedExp(getBody(), f.apply(getExp()));
  }

  /**
   * Combine two canonised expressions (s1,e1) and (s2,e2) into one
   * using the function f.
   * <p>
   * One may think of the result as the pair
   * (s1; MOVE(TEMP(t), e1); s2, f(TEMP(t), e2)),
   * for a fresh temporary t.
   * However, in some cases it is not necessary to introduce a new temporary t,
   * e.g. if e1 is CONST(3). Such cases can be optimised here.
   * </p>
   */
  static CanonizedExp combine(CanonizedExp b1, CanonizedExp b2,
                              BiFunction<ir.Exp, ir.Exp, ir.Exp> f) {
    CanonizedExp c = compose(b1, b2.getBody());
    return new CanonizedExp(c.getBody(), f.apply(c.getExp(), b2.getExp()));
  }

  /**
   * Iterated case of {@code combine}.
   */
  static CanonizedExp combine(CanonizedExp b1, List<CanonizedExp> clist,
                              BiFunction<ir.Exp, List<ir.Exp>, ir.Exp> f) {

    List<CanonizedExp> clistRev = new LinkedList<>(clist);
    Collections.reverse(clistRev);

    LinkedList<ir.Exp> joined = new LinkedList<>();
    List<ir.Stm> stms = new LinkedList<>();
    for (CanonizedExp c : clistRev) {
      CanonizedExp ca = compose(c, stms);
      stms = ca.getBody();
      joined.addFirst(ca.getExp());
    }
    CanonizedExp c = compose(b1, stms);
    return new CanonizedExp(c.getBody(), f.apply(c.getExp(), joined));
  }

  List<ir.Stm> toStm(Function<ir.Exp, ir.Stm> f) {
    LinkedList<ir.Stm> res = new LinkedList<>(getBody());
    res.add(f.apply(getExp()));
    return res;
  }

  static List<ir.Stm> toStm(CanonizedExp b1, CanonizedExp b2,
                             BiFunction<ir.Exp, ir.Exp, ir.Stm> f) {
    CanonizedExp c = compose(b1, b2.getBody());
    LinkedList<ir.Stm> res = new LinkedList<>(c.getBody());
    res.add(f.apply(c.getExp(), b2.getExp()));
    return res;
  }

  /**
   * Given inputs (s, e) and t, this function returns a pair (u,e') such that u
   * has the same side effects as s;t and after running u the expression e' has
   * the same value as the expression e has after evaluating s.
   */
  private static CanonizedExp compose(CanonizedExp c, List<ir.Stm> stms) {
    if (stms.isEmpty()) {
      return c;
    }

    List<ir.Stm> newstms = new LinkedList<>();
    newstms.addAll(c.getBody());

    if (commute(stms, c.getExp())) {
      newstms.addAll(stms);
      return new CanonizedExp(newstms, c.getExp());
    } else {
      ir.Exp t = new ir.ExpTemp(new ir.Temp(), c.getExp().getType());
      newstms.add(new ir.StmMove(t, c.getExp()));
      newstms.addAll(stms);
      return new CanonizedExp(newstms, t);
    }
  }

  /**
   * Returns true if s and e commute, i.e. if the execution of s does not change
   * the value of e.
   */
  private static boolean commute(ir.Stm s, ir.Exp e) {
    return (e instanceof ir.ExpName) || (e instanceof ir.ExpConst);
    // could have more cases here
  }

  private static boolean commute(List<ir.Stm> stms, ir.Exp e) {
    return stms.stream().allMatch(s -> commute(s, e));
  }

}
