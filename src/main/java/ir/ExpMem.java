package ir;

public class ExpMem extends Exp {
  private final Exp address;
  private final Type type;

  public ExpMem(Exp address, Type type) {
    this.type = type;
    if (address == null) {
      throw new NullPointerException();
    }
    this.address = address;
  }

  public Exp getAddress() {
    return address;
  }

  @Override
  public Type getType() {
    return type;
  }

  @Override
  public <A> A accept(ExpVisitor<A> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "MEM(" + getAddress() + ")";
  }


}
