package asm.allocator;

import ir.Temp;

import java.util.HashSet;
import java.util.Set;


public class ActivitySet {
    final Set<Temp> in = new HashSet<>(16);
    final Set<Temp> out = new HashSet<>(16);
}
