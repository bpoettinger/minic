package ir.block;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tracer {

    private Map<String, Block> remainingBlocksByLabel = new HashMap<>();
    private Block startBlock;
    private List<Block> trace = new ArrayList<>();

    public static List<Block> trace(List<Block> blocks) {
        final Map<String, Block> blocksByLabel = new HashMap<>();
        blocks.forEach(block -> blocksByLabel.put(block.getLabel().toString(), block));

        final Tracer tracer = new Tracer(blocksByLabel, blocks.get(0));
        tracer.computeTrace();
        return tracer.getTrace();
    }

    public Tracer(Map<String, Block> blocksByLabel, Block startBlock) {
        this.remainingBlocksByLabel = blocksByLabel;
        this.startBlock = startBlock;
    }

    public void computeTrace() {
        followTrace(startBlock);
        remainingBlocksByLabel.remove(startBlock.getLabel().toString());

        while (!remainingBlocksByLabel.isEmpty()) {
            final Map.Entry<String, Block> entry = remainingBlocksByLabel.entrySet().iterator().next();
            final Block block = entry.getValue();
            final String key = entry.getKey();

            remainingBlocksByLabel.remove(key);
            followTrace(block);
        }
    }

    public List<Block> getTrace() {
        return trace;
    }

    private void followTrace(Block block) {
        trace.add(block);

        if (block.endsWithJump()) {
            final ir.StmJump jump = (ir.StmJump) block.getJump();
            final ir.Label label = jump.getPossibleTargets().get(0);
            final Block targetedBlock = remainingBlocksByLabel.remove(label.toString());
            if (targetedBlock != null) {
                followTrace(targetedBlock);
            }
        } else {
            final ir.StmCJump jump = (ir.StmCJump) block.getJump();
            final ir.Label trueLabel = jump.getLabelTrue();
            final ir.Label falseLabel = jump.getLabelFalse();

            final Block falseBlock = remainingBlocksByLabel.remove(falseLabel.toString());
            if (falseBlock != null) {
                followTrace(falseBlock);
            } else {
                final Block trueBlock = remainingBlocksByLabel.remove(trueLabel.toString());
                if (trueBlock != null) {
                    block.negate();
                    followTrace(trueBlock);
                } else {
                    final Block dummyBlock = new Block();
                    final ir.Label dummyLabel = new ir.Label();
                    dummyBlock.addStatement(new ir.StmLabel(dummyLabel));
                    dummyBlock.addStatement(new ir.StmJump(jump.getLabelFalse()));
                    followTrace(dummyBlock); // ends recursion in block case with no target block

                    block.redirect(new ir.StmCJump(jump.getRel(), jump.getLeft(), jump.getRight(), jump.getLabelTrue(), dummyLabel));
                }
            }
        }
    }
}
